Date: Sat, 24 Jan 2009 11:18:24 +0100
Mime-Version: 1.0 (Produced by PhpWiki 1.3.14-20080124)
X-Rcs-Id: $Id: Help%2FWikicreole 6437 2009-01-24 11:06:22Z vargenau $
Content-Type: application/x-phpwiki;
  pagename=Help%2FWikicreole;
  flags=PAGE_LOCKED;
  markup=2;
  charset=iso-8859-1
Content-Transfer-Encoding: binary

<<CreateToc jshide||=0 with_toclink||=1 position=right with_counter=1 >>

Phpwiki allows you to use the Wikicreole syntax to edit your wiki pages.

We have implemented most of Wikicreole 1.0 and Wikicreole 1.0 additions. What is implemented is described below.

Complete information about Wikicreole can be found at: http://www.wikicreole.org/

== Cheat Sheet from Wikicreole web site ==

[[http://www.wikicreole.org/attach/CheatSheet/creole_cheat_sheet.png]]

== Text formatting ==

=== Bold ===

Bold text is made using two stars:

<verbatim>
This sentence contains **words in bold**.
</verbatim>

This sentence contains **words in bold**.

=== Italics ===

Italics text is made using two slashes:

<verbatim>
This sentence contains //words in italics//.
</verbatim>

This sentence contains //words in italics//.

=== Underline ===

Underline text is made using two hashes:

<verbatim>
This sentence contains __underlined text__.
</verbatim>

This sentence contains __underlined text__.

=== Monospace ===

Monospace text is made using two hashes:

<verbatim>
This sentence contains ##monospace text##.
</verbatim>

This sentence contains ##monospace text##.

=== Superscript ===

Superscript text is made using two carets:

<verbatim>
The XX^^th^^ century.
</verbatim>

The XX^^th^^ century.

=== Subscript ===

Subscript text is made using two commas:

<verbatim>
Water is H,,2,,O.
</verbatim>

Water is H,,2,,O.

== Headers ==

Headers start on a new line with two or more equal signs (up to six) followed the header text.

Optional equal signs can be put at the end of the line (to be compatible with Mediawiki syntax). They are ignored.

The content of the headers is parsed, allowing for instance to use subscript and superscript text. (The header content parsing is optional in Wikicreole.)

<pre>
~== First level of header (h2 in HTML) ==
~=== Second level of header (h3 in HTML) ===
~==== Third level of header (h4 in HTML)
</pre>

== Links ==

=== Simple link ===

<verbatim>
This is a [[link]].
</verbatim>

This is a [[link]].

=== Link with alternate text ===

<verbatim>
This is a [[link|link with alternate text]].
</verbatim>

This is a [[link|link with alternate text]].

=== External link (URL) ===

<verbatim>
[[http://www.wikicreole.org]]
</verbatim>

[[http://www.wikicreole.org]]

=== Raw external link (URL) ===

<verbatim>
http://www.wikicreole.org
</verbatim>

http://www.wikicreole.org

=== External link with alternate text ===

<verbatim>
[[http://www.wikicreole.org|Visit the Wikicreole website]]
</verbatim>

[[http://www.wikicreole.org|Visit the Wikicreole website]]

== Paragraphs ==

Paragraphs are made of text separated by a blank line.

== Line breaks ==

A line break is made with two backslashes.

<verbatim>
This is the first line,\\and this is the second.
</verbatim>

This is the first line,\\and this is the second.

== Lists ==

=== Unordered lists ===

<verbatim>
* Item 1
* Item 2
** Item 2.1
** Item 2.2
* Item 3
</verbatim>

* Item 1
* Item 2
** Item 2.1
** Item 2.2
* Item 3

=== Ordered lists ===

<verbatim>
# Item 1
# Item 2
## Item 2.1
## Item 2.2
# Item 3
</verbatim>

# Item 1
# Item 2
## Item 2.1
## Item 2.2
# Item 3

== Horizontal rule ==

A horizontal rule is made with four hyphens (or more)

<verbatim>
----
</verbatim>

----

== Images ==

A inline image is created with curly brackets.
An alternate text can be put after a pipe. If there is none, an empty alternate text will be created.

<verbatim>
{{myimage.png}}
</verbatim>

<verbatim>
{{myimage.png|this is my image}}
</verbatim>

== Tables ==

All cells are separated by single pipes. Leading spaces are permitted before the first cell of a row and trailing spaces are permitted at the end of a line. The ending pipe is optional. You can embed links, bold, italics, line breaks, and nowiki in table cells. Equal sign directly following pipe defines a header. Headers can be arranged horizontally or vertically.

<verbatim>
|=Heading Col 1 |=Heading Col 2         |
|Cell 1.1       |Two lines\\in Cell 1.2 |
|Cell 2.1       |Cell 2.2               |
</verbatim>

|=Heading Col 1 |=Heading Col 2         |
|Cell 1.1       |Two lines\\in Cell 1.2 |
|Cell 2.1       |Cell 2.2               |

== Nowiki ==

Between three opening curly brackets and three closing curly brackets, no wiki markup is interpreted.

<verbatim>
{{{
//This// does **not** get [[formatted]]
}}}
</verbatim>

{{{
//This// does **not** get [[formatted]]
}}}

<verbatim>
Some examples of markup are: {{{** <i>this</i> ** }}}
</verbatim>

Some examples of markup are: {{{** <i>this</i> ** }}}

== Escape character ==

The escape character is the tilde.

It escapes the character immediately following it.

It disables the automatic conversion of the URL immediately following it.

It disables camel case in the word following it.

<verbatim>
~http://www.foo.com/
</verbatim>

~http://www.foo.com/

<verbatim>
~CamelCaseLink
</verbatim>

~CamelCaseLink

== Plugins ==

Plugins use double angle brackets.

<verbatim>
<<CurrentTime format="date">>
</verbatim>

<<CurrentTime format="date">>

----
[[PhpWikiDocumentation]]
