Date: Sat, 24 Jan 2009 20:18:24 +0100
Mime-Version: 1.0 (Produced by PhpWiki 1.3.14-20080124)
X-Rcs-Id: $Id: Help%2FRedirectToPlugin 6451 2009-01-24 21:40:21Z vargenau $
Content-Type: application/x-phpwiki;
  pagename=Help%2FRedirectToPlugin;
  flags=PAGE_LOCKED;
  markup=2;
  charset=iso-8859-1
Content-Transfer-Encoding: binary

The *~RedirectTo* [[Help:WikiPlugin|plugin]] can be used to redirect a user to another page.
In other words it makes a page an _alias_ for another page.

== Arguments

Use only one of these arguments at a time.

page |
  The page to redirect to (a wiki page name).
href |
  An external URL to redirect to.  Redirection to external URLs will only work
  on locked pages.  (If the URL contains funny characters, you'll probably have
  to put quotes around the URL.)

=== Caveats

The ~RedirectToPlugin invocation must be the first thing on a page.
For most purposes it makes any other content on the page inaccessible.

== Example

A page may be made a alias for the HomePage by placing this code at the top:
<verbatim>
<?plugin RedirectTo page="HomePage" ?>
</verbatim>

To see the example in action, visit HomePageAlias.

----
[[PhpWikiDocumentation]] [[CategoryWikiPlugin]]
