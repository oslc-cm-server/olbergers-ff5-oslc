<?php
rcs_id('$Id: themeinfo.php 6248 2008-09-07 15:13:56Z vargenau $');

/*
 * This file defines the default appearance ("theme") of PhpWiki.
 */

require_once('lib/WikiTheme.php');

$WikiTheme = new WikiTheme('default');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// (c-file-style: "gnu")
// Local Variables:
// mode: php
// tab-width: 8
// c-basic-offset: 4
// c-hanging-comment-ender-p: nil
// indent-tabs-mode: nil
// End:   
?>
