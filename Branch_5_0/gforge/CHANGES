FusionForge-5.0.2:
* Maintenance release, bugfixes mostly.

FusionForge-5.0.1:
* Maintenance release, bugfixes and translations only.

FusionForge-5.0:
* New projectlabels plugin, to tag projects with snippets of
  user-defined HTML (developed for/sponsored by Adullact)
* New extratabs plugin, to add tabs with links to arbitrary URLs
  (Adullact again)
* New globalsearch, allowing cross-forge searching of projects
* Users can log in with their email address (in addition to login
  name) if sys_require_unique_email is true (developed for/sponsored by AdaCore)
* Site admin can now optionally enforce the acceptance of terms of use
  on account creation (AdaCore again)
* Site admin can now optionnally block anonymous downloads from the
  FRS (AdaCore again)
* New command-line scripts to inject users, groups and file releases
  into the database from text files (AdaCore again)
* Trackers: The list of fields displayed when browsing the list of artifacts
  can now be defined (Alcatel-Lucent)
* Trackers: The description field can now be updated/corrected (Alcatel-Lucent)
* Trackers: It is now possible to force a custom field to be mandatory (Alcatel-Lucent)
* Trackers: The values for 'select box' custom fields can now be reordered (Alcatel-Lucent)
* Trackers: A workflow on the status field (when overwritten) can be set. Allowed
  transition between values can be defined and allowed per roles (Alcatel-Lucent)
* Trackers: A new type of extra field has been added: Relation between artifact. 
  This type allow to create a relation between artifacts. Backwards relation can
  also be visible (Alcatel-Lucent)
* Trackers: Dynamic links added, expressions like [#NNN],[TNNN] are now rendered as
  links for description and comments (Alcatel-Lucent).
* Trackers: Search improved to allow searching in text fields (Alcatel-Lucent).
* Trackers: New system to share a search query. Shared queries can be represented 
  as an URL or bookmarked. It is also possible to define one query as the default
  one (Alcatel-Lucent)
* Trackers: Custom fields are now preserved (if same name and value) when moving an
  artifact (Alcatel-Lucent)
* Trackers: New custom field type: Integer (Alain Peyrat)
* Version control subsystem: completely rewritten.  New plugins add
  support for Arch, Bazaar, Darcs, Git, Mercurial.
* Mediawiki plugin: mostly rewritten.  Now creates independent wikis
  for projects, allowing different sets of permissions.
* Forums: New 'move' option to move a thread from one forum to another (Alcatel-Lucent).
* Tasks: Improved CSV import/export (Alcatel-Lucent)
* Global: rework of the default theme, for better accessibility, maintainability and
  conformance (developed by Open-S for Adullact)
* New contribtracker plugin, to give visibility to major contributors
  to projects (developed for/sponsored by Adullact)

FusionForge-4.8.3:
* Maintenance release, only bugfixes.

FusionForge-4.8.2:
* Maintenance release, security and bugfixes.

FusionForge-4.8.1:
* Maintenance release, only bugfixes.

FusionForge-4.8:
* New classification by tags (aka tag cloud)
* New reporting page on frs to view the downloads per package
* List of all projects added in 'Project List'
* New version of wiki plugin, using lastest svn code
