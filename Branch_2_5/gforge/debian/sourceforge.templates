Template: sourceforge/domain_name
Type: string
Description: Your SF domain or subdomain name
 The domain that will host your Sourceforge installation.  Some services
 will be given their own subdomain in that domain (cvs, lists, etc.).
Description-fr: Votre nom de domaine
 Le nom du domaine qui h�berge votre Sourceforge.  Certains services auront
 leur propre sous-domaine � l'int�rieur de ce domaine (cvs, lists, etc.).

Template: sourceforge/cvs_host
Type: string
Description: Your CVS server
 The hostname of the server that will host your Sourceforge CVS
 repositories.  It should not be the same as your main SF host.
Description-fr: Votre serveur CVS
 Le nom du serveur CVS de votre Sourceforge.
 Ce nom ne devrait pas �tre identique au nom de votre serveur SF principal.

Template: sourceforge/lists_host
Type: string
Description: Your mailing-lists server
 The hostname of the server that will host your Sourceforge mailing-lists. 
 It should not be the same as your main SF host.
Description-fr: Votre serveur de listes de diffusion
 Le nom du serveur qui h�bergera vos listes de diffusion de Sourceforge.
 Ce nom ne devrait pas �tre identique au nom de votre serveur SF principal.

Template: sourceforge/download_host
Type: string
Description: Your download server
 The hostname of the server that will host your Sourceforge packages. 
 It should not be the same as your main SF host.
Description-fr: Votre serveur de telechargement
 Le nom du serveur qui h�bergera vos paquetages Sourceforge.
 Ce nom ne devrait pas �tre identique au nom de votre serveur SF principal.

Template: sourceforge/ip_address
Type: string
Description: Your IP address
 The IP address of the server that will host your Sourceforge installation.
 This is needed for the Apache virtualhosting configuration.
Description-fr: Votre adresse IP
 L'adresse IP du serveur qui h�bergera votre installation de Sourceforge.
 Cette information est requise pour la configuration des h�tes virtuels
 d'Apache.

Template: sourceforge/server_admin
Type: string
Description: The SF admin email address.
 The email address of the Sourceforge administrator of your site.  Needed
 in case a problem occurs.
Description-fr: L'adresse e-mail de l'administrateur SF
 L'adresse e-mail de l'administrateur de votre Sourceforge, requise au cas
 o� un probl�me survendrait.

Template: sourceforge/db_password
Type: password
Description: Password used for the database
 The connecton to the DB system requires a password.  Please choose a
 password here.
Description-fr: Mot de passe utilis� pour la base de donn�es
 La connexion � la base de donn�es doit �tre authentifi�e par un mot de
 passe.  Veuillez le choisir ici.

Template: sourceforge/db_password_confirm
Type: password
Description: Password used for the database - again
 Please re-type the password for confirmation.
Description-fr: Mot de passe pour la base de donn�es - confirmation
 Veuillez saisir le mot de passe de nouveau, pour confirmation.

Template: sourceforge/system_name
Type: string
Default: Sourceforge
Default-fr: Sourceforge
Description: The Sourceforge system name
 This name is used in various places throughout the system.
Description-fr: Nom de votre syst�me Sourceforge
 Ce nom est utilis� � divers endroits au travers du syst�me.

Template: sourceforge/admin_login
Type: string
Default: admin
Default-fr: admin
Description: The Sourceforge administrator login
 This Sourceforge account will have all privileges on the Sourceforge
 system.  It is needed to approve the creation of the projects.
Description-fr: Le nom du compte de l'administrateur
 Ce compte Sourceforge a tous les privil�ges sur le syst�me Sourceforge.
 Il sert entre autres � approuver la cr�ation de projets.

Template: sourceforge/admin_password
Type: password
Description: The Sourceforge administrator password
 This Sourceforge account will have all privileges on the Sourceforge
 system.  It is needed to approve the creation of the projects.  Please
 type a password here.
Description-fr: Mot de passe de l'administrateur
 Le compte de l'administrateur a besoin d'un mot de passe.  Veuillez le
 saisir ici.

Template: sourceforge/admin_password_confirm
Type: password
Description: The Sourceforge administrator password - again
 Please re-type the password for confirmation.
Description-fr: Mot de passe administrateur - confirmation
 Veuillez saisir le mot de passe de nouveau, pour confirmation.

Template: sourceforge/newsadmin_groupid
Type: string
Default: 2
Default-fr: 2
Description: The news admin group id
 Members of the news admin group can approve news for the Sourceforge main
 page.  This group id MUST NOT be 1. You only need to touch this group if
 you upgrade from a previous version and want to keep your data.
Description-fr: Le num�ro du groupe d'administration des nouvelles
 Les membres du groupe d'administration des nouvelles peuvent approuver des
 nouvelles pour les publier sur la page d'accueil de Sourceforge.  Le num�ro
 de ce groupe NE DOIT PAS �tre 1.  Cette information n'est requise que si vous
 mettez � jour une installation pr�c�dente et voulez garder vos donn�es.

Template: sourceforge/skill_list
Type: string
Default: Ada;C;C++;HTML;LISP;Perl;PHP;Python;SQL
Default-fr: Ada;C;C++;HTML;LISP;Perl;PHP;Python;SQL
Description: The initial list of skills
 Sourceforge allows users to define a list of their skills, to be chosen
 amongst those present in the database.  This list is the initial list of
 skills that will enter the database.  Please enter the skill names
 separated by semi-colons `;'.
Description-fr: Liste de comp�tences
 Les utilisateurs de Sourceforge peuvent afficher leurs niveaux de comp�tence
 dans diff�rents domaines.  Cette liste est la liste initiale de ces domaines.
 Veuillez entrer les noms de ces domaines de comp�tence s�par�s par des
 points-virgules `;'.

Template: sourceforge/ldap_base_dn
Type: string
Description: The LDAP base DN
 The DN is used to refer to the LDAP directory unambiguously.  You could use,
 for instance, "dc=sourceforge,dc=example,dc=com"
Description-fr: Le nom du r�pertoire LDAP (DN)
 Le nom DN est utilis� pour identifier l'annuaire LDAP de mani�re unique.
 Par exemple, vous pourriez utiliser le DN "dc=sourceforge,dc=exemple,dc=com".

Template: sourceforge/ldap_host
Type: string
Description: The LDAP host
 The hostname of the LDAP server.
Description-fr: Le serveur LDAP
 Le nom d'h�te de votre serveur LDAP.

Template: sourceforge/noreply_to_bitbucket
Type: boolean
Default: true
Description: Do you want mail to ${noreply} to be deleted?
 Sourceforge sends plenty of e-mail from the "${noreply}" address,
 and maybe even some e-mail to that address too.
 .
 It is advised that you let the package direct e-mail to that address to a
 black hole (/dev/null), unless you have another use for that address.
 .
 Answering "yes" here will perform that redirection.
Description-fr: Supprimer les e-mails pour ${noreply} ?
 Sourceforge envoie fr�quemment des e-mails depuis (et vers) l'adresse
 "${noreply}".
 .
 Il est conseill� de rediriger les e-mails vers cette adresse vers un trou
 noir, sauf si vous avez un autre usage pour cette adresse.
 .
 R�pondre "oui" ici mettra en place cette redirection.

Template: sourceforge/ldap_web_add_password
Type: password
Description: LDAP password used to add users from the web
 In order to add users into the LDAP directory from the web, you need to
 provide a password.
Description-fr: Mot de passe LDAP pour l'ajout d'utilisateurs
 Vous avez besoin d'un mot de passe pour ajouter des utilisateurs �
 l'annuaire LDAP depuis le web.  Veuillez le saisir ici.

Template: sourceforge/ldap_web_add_password_confirm
Type: password
Description: LDAP password - again
 Please re-type the password for confirmation.
Description-fr: Mot de passe LDAP - confirmation
 Veuillez saisir le mot de passe de nouveau, pour confirmation.

Template: sourceforge/mod_ssl_cert
Type: note
Description: Generate an SSL certificate
 You need a valid SSL/TLS certificate to run Sourceforge.
 If you don't already have one, you'll have to run the
 "mod-ssl-makecert" command to generate one, and restart Apache
 afterwards.
 .
 Please note you will not be able to start Apache if you have no
 such certificate.
Description-fr: N'oubliez pas de g�n�rer un certificat SSL
 Sourceforge a besoin d'un certificat de cryptographie SSL/TLS
 pour fonctionner.  Si vous n'en avez pas d�j� un, vous devrez
 vous en fabriquer un � l'aide de la commande "mod-ssl-makecert".
 N'oubliez pas de red�marrer Apache par la suite.
 .
 NB : vous ne pourrez pas d�marrer Apache si vous n'avez pas de
 certificat.

Template: sourceforge/pam_ldap_config
Type: note
Description: Sourceforge requires appropriate PAM-LDAP configuration
 Sourceforge requires the libpam-ldap package to be configured
 appropriately.  You might need to "dpkg-reconfigure libpam-ldap" in
 order to fix incorrect parameters.
 .
 Known good values are: LDAP protocol version=3, make root database
 admin=yes, root login account="cn=admin,dc=<your-dc-here>", crypt to
 use for passwords=crypt.  Make sure that you type the same password
 for the libpam-ldap root password and the Sourceforge LDAP password.
Description-fr: Sourceforge a besoin d'une configuration PAM-LDAP correcte
 Sourceforge repose sur PAM-LDAP, et a donc besoin d'une configuration
 appropri�e de cette biblioth�que.  Il se peut que vous ayez besoin
 d'utiliser "dpkg-reconfigure libpam-ldap" pour corriger des
 param�tres incorrects.
 .
 Un jeu de param�trs r�put�s corrects est le suivant: LDAP protocol
 version="3", make root database admin="yes", root login
 account="cn=admin,dc=<votre-dc>", crypt to use for
 passwords="crypt".  En outre, vous devrez utiliser le m�me mot de
 passe pour le compte administrateur de libpam-ldap et pour le mot de
 passe LDAP de Sourceforge.

Template: sourceforge/do_config
Type: boolean
Default: false
Description: Do you want to let sourceforge install modify some config files?
 For Sourceforge to be fully functional, a lot of config files need to
 be changed.  The debian policy forbids us to fiddle with them automatically,
 so the default answer is "no", leaving you to update them.  If, however, you'd
 like me to update them, just answer "yes" here.  It should be safe, unless
 you have a particularly tricky system.  If you choose to answer "yes" here,
 backups of your previous files will be made, named <file>.sourceforge-old.
 .
 Potentially modified files are: /etc/ldap/slapd.conf,
 /etc/nsswitch.conf, /etc/libnss-ldap.conf, /etc/apache/httpd.conf,
 /etc/apache-ssl/httpd.conf, /etc/php4/apache/php.ini,
 /etc/php4/cgi/php.ini, /etc/aliases, /etc/postgresql/pg_hba.conf,
 /etc/exim/main.cf, /etc/bind/named.conf, /etc/exim/main.cf,
 /etc/proftpd.conf, /etc/bind/named.conf,
 /etc/init.d/ssh.
 .
 We will maybe have a better support for this in the future,
 feel free to send a patch :-)
Description-fr: Modifier automatiquement certains fichiers ?
 Voulez-vous laisser l'intallation de sourceforge modifier certains
 fichiers de configuration ?  La modification est non recommand�e,
 et les r�gles Debian l'emp�chent d'�tre syst�matique, elle est donc
 non activ�e par d�faut.  Si vous r�pondez "oui" ici, des sauvegardes
 des fichiers modifi�s seront faites sous le nom <fichier>.sourceforge-old.
 .
 Les fichiers potentiellement modifi�s sont: /etc/ldap/slapd.conf,
 /etc/nsswitch.conf, /etc/libnss-ldap.conf, /etc/apache/httpd.conf,
 /etc/apache-ssl/httpd.conf, /etc/php4/apache/php.ini,
 /etc/php4/cgi/php.ini, /etc/aliases, /etc/postgresql/pg_hba.conf,
 /etc/exim/main.cf, /etc/bind/named.conf, /etc/exim/main.cf,
 /etc/proftpd.conf, /etc/bind/named.conf, /etc/init.d/ssh.
 .
 Nous supporterons peut-�tre cela de meilleure fa�on, envoyez vos patch :-)
