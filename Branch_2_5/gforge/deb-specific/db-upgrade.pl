#!/usr/bin/perl -w
#
# Debian-specific script to upgrade the database between releases

use DBI ;
use strict ;
use diagnostics ;

use vars qw/$dbh @reqlist $query/ ;

use vars qw/$sys_default_domain $sys_cvs_host $sys_download_host
    $sys_shell_host $sys_users_host $sys_docs_host $sys_lists_host
    $sys_dns1_host $sys_dns2_host $FTPINCOMING_DIR $FTPFILES_DIR
    $sys_urlroot $sf_cache_dir $sys_name $sys_themeroot
    $sys_news_group $sys_dbhost $sys_dbname $sys_dbuser $sys_dbpasswd
    $sys_ldap_base_dn $sys_ldap_host $admin_login $admin_password
    $server_admin $domain_name $newsadmin_groupid $skill_list/ ;

sub is_lesser ( $$ ) ;
sub is_greater ( $$ ) ;
sub debug ( $ ) ;
sub parse_sql_file ( $ ) ;

require ("/usr/lib/sourceforge/lib/include.pl") ; # Include all the predefined functions 

debug "You'll see some debugging info during this installation." ;
debug "Do not worry unless told otherwise." ;

&db_connect ;

$dbh->{AutoCommit} = 0;
$dbh->{RaiseError} = 1;
eval {
    my ($sth, @array, $version, $action) ;

    # Do we have at least the basic schema?

    $query = "SELECT count(*) FROM pg_class WHERE relname = 'groups'";
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;

    # Create Sourceforge database

    if ($array [0] == 0) {
	$action = "installation" ;
	debug "Creating initial Sourceforge database from files." ;

	my @filelist = qw{ /usr/lib/sourceforge/db/SourceForge.sql
			       /usr/lib/sourceforge/db/trove_defaults.sql
			       /usr/lib/sourceforge/db/init-extra.sql } ;
	# TODO: user_rating.sql
			      
	foreach my $file (@filelist) {
	    debug "Processing $file" ;
	    @reqlist = @{ &parse_sql_file ($file) } ;
	    
 	    foreach my $s (@reqlist) {
 		$query = $s ;
  		$sth = $dbh->prepare ($query) ;
  		$sth->execute () ;
  		$sth->finish () ;
 	    }
 	}
	@reqlist = () ;

	debug "Adding local data." ;

	
	do "/etc/sourceforge/local.pl" or die "Cannot read /etc/sourceforge/local.pl" ;

	my ($login, $pwd, $md5pwd, $email, $shellbox, $noreplymail, $date) ;

	$login = $admin_login ;
	$pwd = $admin_password ;
	$md5pwd=qx/echo -n $pwd | md5sum/ ;
	chomp $md5pwd ;
	# Next line is a hack to work around the change in behaviour of
	# /usr/bin/md5sum in dpkg 1.10 (as compared to 1.9.*)
	$md5pwd =~ s/(.{32})  -/$1/ ;
	$email = $server_admin ;
	$shellbox = "shell" ;
	$noreplymail="noreply\@$domain_name" ;
	$date = time () ;
	
 	@reqlist = (
 	    "INSERT INTO groups (group_id, group_name, homepage, is_public, status, unix_group_name, unix_box,
                     http_domain, short_description, cvs_box, license, register_purpose,
                     license_other, register_time, use_bugs, rand_hash, use_mail, use_survey,
                     use_patch, use_forum, use_pm, use_cvs, use_news, use_support, new_bug_address,
                     new_patch_address, new_support_address, type, use_docman, send_all_bugs,
                     send_all_patches, send_all_support, new_task_address, send_all_tasks,
                     use_bug_depend_box, use_pm_depend_box)
 	    VALUES (1, 'Site Admin', '$domain_name/admin/', 1, 'A', 'siteadmin', '$shellbox', 
 	    	    NULL, NULL, 'cvs1', 'website', NULL, NULL, 0, 0, NULL, 1, 0, 0, 0, 0, 0, 1, 1, '', '', '', 1, 1, 0, 0, 0, '', 0, 0, 0)",
 	    "INSERT INTO groups (group_id, group_name, homepage, is_public, status, unix_group_name, unix_box,
                     http_domain, short_description, cvs_box, license, register_purpose,
                     license_other, register_time, use_bugs, rand_hash, use_mail, use_survey,
                     use_patch, use_forum, use_pm, use_cvs, use_news, use_support, new_bug_address,
                     new_patch_address, new_support_address, type, use_docman, send_all_bugs,
                     send_all_patches, send_all_support, new_task_address, send_all_tasks,
                     use_bug_depend_box, use_pm_depend_box)
 	    VALUES ($newsadmin_groupid, 'Site News Admin', '$domain_name/news/', 0, 'A', 'newsadmin', '$shellbox',
                     NULL, NULL, 'cvs1', 'website', NULL, NULL, 0, 0, NULL, 1, 0, 0, 0, 0, 0, 1, 1, '', '',
                     '', 1, 0, 0, 0, 0, '', 0, 0, 0)",
 	    "INSERT INTO users (user_id, user_name, email, user_pw)  
 		    VALUES (100,'None','$noreplymail','*********')", 
 	    "INSERT INTO users VALUES (101,'$login','$email','$md5pwd','Sourceforge admin','A','/bin/bash','','N',2000,'$shellbox',$date,'',1,0,NULL,NULL,0,'','GMT', 1)", 
 	    "INSERT INTO user_group (user_id, group_id, admin_flags) VALUES (101, 1, 'A')",
 	    "INSERT INTO user_group (user_id, group_id, admin_flags) VALUES (101, $newsadmin_groupid, 'A')",
 	    "INSERT INTO bug_category (bug_category_id, group_id, category_name) VALUES (100,1,'None')",
 	    "INSERT INTO bug_group (bug_group_id, group_id, group_name) VALUES (100,1,'None')",
 	    "INSERT INTO bug (bug_id,group_id,status_id,category_id,bug_group_id,submitted_by,assigned_to,resolution_id)
 		    VALUES (100,1,100,100,100,100,100,100)",
 	    "INSERT INTO patch_category (patch_category_id, group_id, category_name) VALUES (100,1,'None')",
 	    "INSERT INTO patch (group_id,patch_status_id,patch_category_id,submitted_by,assigned_to)
 		    VALUES (1,100,100,100,100)",
 	    "INSERT INTO project_group_list (group_project_id,group_id) VALUES (1,1)",
 	    "INSERT INTO project_task (group_project_id,created_by,status_id)
 		    VALUES (1,100,100)",
 	    "INSERT INTO support_category VALUES ('100','1','None')"
 		    ) ;

 	foreach my $s (@reqlist) {
 	    $query = $s ;
 	    $sth = $dbh->prepare ($query) ;
 	    $sth->execute () ;
 	    $sth->finish () ;
 	}
	@reqlist = () ;

	debug "Initialising sequences." ;

	@filelist = qw{ /usr/lib/sourceforge/db/init-sequences.sql } ;
			      
	foreach my $file (@filelist) {
	    debug "Processing $file" ;
	    @reqlist = @{ &parse_sql_file ($file) } ;
	    
 	    foreach my $s (@reqlist) {
 		$query = $s ;
  		$sth = $dbh->prepare ($query) ;
  		$sth->execute () ;
  		$sth->finish () ;
 	    }
 	} 
	@reqlist = () ;

	debug "Inserting skills." ;

	foreach my $skill (split /;/, $skill_list) {
	    push @reqlist, "INSERT INTO people_skill (name) VALUES ('$skill')" ;
	}

 	foreach my $s (@reqlist) {
 	    $query = $s ;
 	    $sth = $dbh->prepare ($query) ;
 	    $sth->execute () ;
 	    $sth->finish () ;
 	}
	@reqlist = () ;

 	debug "Committing." ;
 	$dbh->commit () ;
    } else {
	$action = "upgrade" ;
    }

    # Do we have the metadata table?

    $query = "SELECT count(*) FROM pg_class WHERE relname = 'debian_meta_data'";
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;

    # Let's create this table if we have it not

    if ($array [0] == 0) {
	debug "Creating debian_meta_data table." ;
	$query = "CREATE TABLE debian_meta_data (key varchar primary key, value text not null)" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;
	
	# Now table should exist, let's enter its first value in it

	debug "Inserting first data into debian_meta_data table." ;
	$query = "INSERT INTO debian_meta_data (key, value) VALUES ('db-version', '2.5-7+just+before+8')" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Committing." ;
	$dbh->commit () ;
    }

    # Now we have the metadata table with at least the "db-version" key
    # We can continue our work based on the associated value
    
    $query = "select value from debian_meta_data where key = 'db-version'" ;
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;
    
    $version = $array [0] ;

    # $version is the last successfully installed version
    
    if (is_lesser $version, "2.5-8") {
	debug "Found version $version lesser than 2.5-8, adding row to people_job_category." ;
	$query = "INSERT INTO people_job_category VALUES (100, 'Undefined', 0)" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Updating debian_meta_data table." ;
	$query = "UPDATE debian_meta_data SET value = '2.5-8' where key = 'db-version'" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Committing." ;
	$dbh->commit () ;
    }

    $query = "select value from debian_meta_data where key = 'db-version'" ;
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;
    
    $version = $array [0] ;

    if (is_lesser $version, "2.5-25") {
	debug "Found version $version lesser than 2.5-25, adding row to supported_languages." ;
	$query = "INSERT INTO supported_languages VALUES (15, 'Korean', 'Korean.class', 'Korean', 'kr')" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Updating debian_meta_data table." ;
	$query = "UPDATE debian_meta_data SET value = '2.5-25' where key = 'db-version'" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Committing." ;
	$dbh->commit () ;
    }

    $query = "select value from debian_meta_data where key = 'db-version'" ;
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;
    
    $version = $array [0] ;

    if (is_lesser $version, "2.5-27") {
	debug "Found version $version lesser than 2.5-27, fixing unix_box entries." ;

	$query = "update groups set unix_box = 'shell'" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	$query = "update users set unix_box = 'shell'" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Also fixing a few sequences." ;

	&bump_sequence_to ("bug_pk_seq", 100) ;
	&bump_sequence_to ("project_task_pk_seq", 100) ;
	
	debug "Updating debian_meta_data table." ;
	$query = "UPDATE debian_meta_data SET value = '2.5-27' where key = 'db-version'" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Committing." ;
	$dbh->commit () ;
    }

    $query = "select value from debian_meta_data where key = 'db-version'" ;
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;
    
    $version = $array [0] ;
    
    if (is_lesser $version, "2.5-30") {
	debug "Found version $version lesser than 2.5-30, adding rows to supported_languages." ;
	@reqlist = (
		    "INSERT INTO supported_languages VALUES (16,'Bulgarian','Bulgarian.class','Bulgarian','bg')",
		    "INSERT INTO supported_languages VALUES (17,'Greek','Greek.class','Greek','el')",
		    "INSERT INTO supported_languages VALUES (18,'Indonesian','Indonesian.class','Indonesian','id')",
		    "INSERT INTO supported_languages VALUES (19,'Portuguese (Brazillian)','PortugueseBrazillian.class','PortugueseBrazillian', 'br')",
		    "INSERT INTO supported_languages VALUES (20,'Polish','Polish.class','Polish','pl')",
		    "INSERT INTO supported_languages VALUES (21,'Portuguese','Portuguese.class','Portuguese', 'pt')",
		    "INSERT INTO supported_languages VALUES (22,'Russian','Russian.class','Russian','ru')"
		    ) ;
	
 	foreach my $s (@reqlist) {
 	    $query = $s ;
 	    $sth = $dbh->prepare ($query) ;
 	    $sth->execute () ;
 	    $sth->finish () ;
 	}
	@reqlist = () ;

 	debug "Updating debian_meta_data table." ;
 	$query = "UPDATE debian_meta_data SET value = '2.5-30' where key = 'db-version'" ;
 	$sth = $dbh->prepare ($query) ;
 	$sth->execute () ;
 	$sth->finish () ;

 	debug "Committing." ;
 	$dbh->commit () ;
     }

    $query = "select value from debian_meta_data where key = 'db-version'" ;
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;
    
    $version = $array [0] ;

    if (is_lesser $version, "2.5-32") {
	debug "Found version $version lesser than 2.5-32, fixing unix_uid entries." ;

	$query = "update users set unix_uid = nextval ('unix_uid_seq')
                  where unix_status != 'N'
                    and status != 'P'
                    and unix_uid = 0" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;
	
	debug "Updating debian_meta_data table." ;
	$query = "UPDATE debian_meta_data SET value = '2.5-32' where key = 'db-version'" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	$sth->finish () ;

	debug "Committing." ;
	$dbh->commit () ;
    }

    $query = "select value from debian_meta_data where key = 'db-version'" ;
    $sth = $dbh->prepare ($query) ;
    $sth->execute () ;
    @array = $sth->fetchrow_array () ;
    $sth->finish () ;
    
    $version = $array [0] ;

    if (is_lesser $version, "2.5-33+") {
	debug "Found version $version lesser than 2.5-33+, updating language codes";
	@reqlist = (
		    "UPDATE supported_languages SET language_code='en' where classname='English'",
		    "UPDATE supported_languages SET language_code='ja' where classname='Japanese'",
		    "UPDATE supported_languages SET language_code='iw' where classname='Hebrew'",
		    "UPDATE supported_languages SET language_code='es' where classname='Spanish'",
		    "UPDATE supported_languages SET language_code='th' where classname='Thai'",
		    "UPDATE supported_languages SET language_code='de' where classname='German'",
		    "UPDATE supported_languages SET language_code='it' where classname='Italian'",
		    "UPDATE supported_languages SET language_code='no' where classname='Norwegian'",
		    "UPDATE supported_languages SET language_code='sv' where classname='Swedish'",
		    "UPDATE supported_languages SET language_code='zh' where classname='Chinese'",
		    "UPDATE supported_languages SET language_code='nl' where classname='Dutch'",
		    "UPDATE supported_languages SET language_code='eo' where classname='Esperanto'",
		    "UPDATE supported_languages SET language_code='ca' where classname='Catalan'",
		    "UPDATE supported_languages SET language_code='ko' where classname='Korean'",
		    "UPDATE supported_languages SET language_code='bg' where classname='Bulgarian'",
		    "UPDATE supported_languages SET language_code='el' where classname='Greek'",
		    "UPDATE supported_languages SET language_code='id' where classname='Indonesian'",
		    "UPDATE supported_languages SET language_code='pt' where classname='Portuguese (Brazillian)'",
		    "UPDATE supported_languages SET language_code='pl' where classname='Polish'",
		    "UPDATE supported_languages SET language_code='pt' where classname='Portuguese'",
		    "UPDATE supported_languages SET language_code='ru' where classname='Russian'",
		    "UPDATE supported_languages SET language_code='fr' where classname='French'",
		    ) ;
	
 	foreach my $s (@reqlist) {
 	    $query = $s ;
 	    $sth = $dbh->prepare ($query) ;
 	    $sth->execute () ;
 	    $sth->finish () ;
 	}
	@reqlist = () ;

 	debug "Updating debian_meta_data table." ;
 	$query = "UPDATE debian_meta_data SET value = '2.5-33+' where key = 'db-version'" ;
 	$sth = $dbh->prepare ($query) ;
 	$sth->execute () ;
 	$sth->finish () ;

 	debug "Committing." ;
 	$dbh->commit () ;
     }



    debug "It seems your database $action went well and smoothly.  That's cool." ;
    debug "Please enjoy using Debian Sourceforge." ;
    
    # There should be a commit at the end of every block above.
    # If there is not, then it might be symptomatic of a problem.
    # For safety, we roll back.
    $dbh->rollback ();
};

if ($@) {
    warn "Transaction aborted because $@" ;
    debug "Transaction aborted because $@" ;
    debug "Last SQL query was:\n$query\n(end of query)" ;
    $dbh->rollback ;
    debug "Please report this bug on the Debian bug-tracking system." ;
    debug "Please include the previous messages as well to help debugging." ;
    debug "You should not worry too much about this," ;
    debug "your DB is still in a consistent state and should be usable." ;
    exit 1 ;
}

$dbh->rollback ;
$dbh->disconnect ;

sub is_lesser ( $$ ) {
    my $v1 = shift || 0 ;
    my $v2 = shift || 0 ;

    my $rc = system "dpkg --compare-versions $v1 lt $v2" ;
    
    return (! $rc) ;
}

sub is_greater ( $$ ) {
    my $v1 = shift || 0 ;
    my $v2 = shift || 0 ;

    my $rc = system "dpkg --compare-versions $v1 gt $v2" ;
    
    return (! $rc) ;
}

sub debug ( $ ) {
    my $v = shift ;
    chomp $v ;
    print STDERR "$v\n" ;
}

sub parse_sql_file ( $ ) {
    my $f = shift ;
    open F, $f || die "Could not open file $f: $!\n" ;

    # This is a state machine to parse potentially complex SQL files
    # into individual SQL requests/statements
    
    # Init the state machine

    my ($l, $level, $inquote, $chunk, $rest) ;
    my $sql = "" ;
    my @sql_list = () ;
    
    # my $n = 0 ;

  FILELOOP: while ($l = <F>) {	# Loop over the file
      chomp $l ;
      $level = 0 ;
      $inquote = 0 ;
      $chunk = "" ;
      $rest = "" ;
      
    PARSELOOP: while (1) {	# Parse a request

	while ( ($l eq "") or ((! $inquote) and ($l =~ /^\s*$/)) ) {
	    $l = <F> ;
	    if ($l) {
 		chomp $l ;
 	    } else {
 		last PARSELOOP ;
 	    }
	}
	($chunk, $rest) = ($l =~ /^([^()\\\';]*)(.*)/) ;
	$sql .= $chunk ;
	# debug "level = $level, inquote = $inquote, chunk = <$chunk>, rest = <$rest>, sql = <$sql>";

	# Here come the state transitions
      SWITCH: {
	  if ($rest =~ /^\(/) {	# Enter a paren block (unless we're inside a string)
	      $level += 1 unless $inquote ;
	      $sql .= '(' ;
	      $rest = substr $rest, 1 ;
	      last SWITCH;
	  }
	  if ($rest =~ /^\)/) {	# Exit a paren block (unless we're inside a string)
	      $level -= 1 unless $inquote ;
	      $sql .= ')' ;
	      $rest = substr $rest, 1 ;
	      last SWITCH;
	  }
	  if ($rest =~ /^\\\'/) { # Escaped single quote
	      if (!$inquote) {
		  debug "Encountered a \' sequence outside of a string." ;
		  debug "This really shouldn't have happened." ;
		  debug "I find it more prudent to just die now." ;
		  die "\' outside of a string -- check SQL file" ;
	      }
	      $sql .= '\\\'' ;
	      $rest = substr $rest, 2 ;
	      last SWITCH;
	  }
	  if ($rest =~ /^\\/) { # Other backslash is a normal character
	      $sql .= '\\' ;
	      $rest = substr $rest, 1 ;
	      last SWITCH;
	  }
	  if ($rest =~ /^;/) { # Semi-colon
	      if ($inquote) { # If inside a string, treat as a normal character
		  $sql .= ';' ;
		  $rest = substr $rest, 1 ;
	      } elsif ($level == 0) { # If out of a string and toplevel, end of SQL statement
		  last PARSELOOP;
	      } else{ # What, a semi-colon by itself not at toplevel?
		  debug "Encountered a semi-colon outside of a string and not at toplevel" ;
		  debug "This really shouldn't have happened." ;
		  debug "I find it more prudent to just die now." ;
		  die "semi-colon outside of a string and not at toplevel -- check SQL file" ;
	      }
	      last SWITCH;
	  }
	  if ($rest =~ /^\'/) { # Non-escaped single quote -- string delimiter
	      $inquote = $inquote ? 0 : 1 ; # Toggle $inquote
	      $sql .= '\'' ;
	      $rest = substr $rest, 1 ;
	      last SWITCH;
	  }
      } # SWITCH
	$l = $rest ;
    } # PARSELOOP
      
      # (Do something with $sql now that we have it :-)
      push @sql_list, $sql unless $sql eq "" ;
      # $n++ ; debug "SQL OK $n" ;
      $sql = "" ;
  } # FILELOOP
    
    close F ;
    
    return \@sql_list ;
}

sub bump_sequence_to ( $$ ) {
    my ($sth, @array, $seqname, $targetvalue) ;

    $seqname = shift ;
    $targetvalue = shift ;

    do {
	$query = "select nextval ('$seqname')" ;
	$sth = $dbh->prepare ($query) ;
	$sth->execute () ;
	@array = $sth->fetchrow_array () ;
	$sth->finish () ;
    } until $array[0] >= $targetvalue ;
}
