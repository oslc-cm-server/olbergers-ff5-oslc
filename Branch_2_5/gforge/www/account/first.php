<?php
//
// SourceForge: Breaking Down the Barriers to Open Source Development
// Copyright 1999-2000 (c) The SourceForge Crew
// http://sourceforge.net
//
// $Id$

require "pre.php";    
site_user_header(array(title=>"Welcome!"));

echo $Language->FIRSTBLURB;

site_user_footer(array());
?>
