DROP TABLE IF EXISTS plugin_scmsvn_group_usage;
DROP TABLE IF EXISTS plugin_scmsvn_stats;
DROP SEQUENCE IF EXISTS plugin_scmsvn_grp_usage_pk_seq;
DROP SEQUENCE IF EXISTS plugin_scmsvn_stats_pk_seq;
