Date: Sat, 24 Jan 2009 20:18:24 +0100
Mime-Version: 1.0 (Produced by PhpWiki 1.3.14-20080124)
X-Rcs-Id: $Id: Help%2FPhpWeatherPlugin 6451 2009-01-24 21:40:21Z vargenau $
Content-Type: application/x-phpwiki;
  pagename=Help%2FPhpWeatherPlugin;
  flags=PAGE_LOCKED;
  markup=2;
  charset=iso-8859-1
Content-Transfer-Encoding: binary

The *~PhpWeather* [[Help:WikiPlugin|plugin]] is a [[Help:WikiPlugin|plugin]] that uses ~PhpWeather to display
a block of text with the current weather for some airport in the
world. It looks like this:

<?plugin PhpWeather menu=true ?>

== Arguments

__menu__:
  Set this to =true= to have the plugin generate a menu after the
  report. The user will able to select a country from this menu, and
  after that, an airport and a language. The default value is
  <tt>false</tt>.

__icao__:
  Use this to pre-select a specific airport instead of using the
  default one which is <tt>EKAH</tt>. If you want the user to be able
  to change the station using the menu, then you have to use this as
  <verbatim>
  <?plugin PhpWeather menu=true icao||=EKYT ?>
  </verbatim>
  so that the value can be overwritten when the user submits the
  form. If you just use
  <verbatim>
  <?plugin PhpWeather menu=true icao=EKYT ?>
  </verbatim>
  then nothing will happen when the user selects another station from
  the list.

__cc__:
  Specify the country code. You can use this if you want pre-select a
  different country than the one specified in the ICAO. So using
  <verbatim>
  <?plugin PhpWeather menu=true cc||=GB icao||=KTTS ?>
  </verbatim>
  will show the current weather at the NASA Shuttle Facility, United
  States (<tt>KTTS</tt>) and at the same time give the user a list of
  stations in the United Kingdom. As the example shows, then you
  should use <tt>cc||=XX</tt> when combining it with __menu__ set to
  <tt>true</tt>.

__language__:
  The default language. When combining __language__ with __menu__ set
  to <tt>true</tt> then remember to use the <code>language||=xx</code>
  form.

__units__:
  You can specify the way the units are printed. The choice is between
  having both metric and imperial units printed, or just one of
  them. Setting __units__ to =both_metric= will print the metric value
  first and then the imperial value in parenthesis. Using
  =both_imperial= instead will do the opposite.

  If you only need the metric or imperial units to be shown, then
  setting __units__ to =only_metric= or =only_imperial= will do just
  that.

----

[[PhpWikiDocumentation]] [[CategoryWikiPlugin]]
