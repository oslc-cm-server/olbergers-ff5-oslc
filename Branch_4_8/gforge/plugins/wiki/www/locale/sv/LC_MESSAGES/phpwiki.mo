��       d     <   �  \      �     �     �     �     �     �     �     �     �     �     	     	     	     	*     	1     	:   	  	A     	K     	T     	f     	k     	t     	y     	�     	�     	�     	�     	�   !  	�     	�     	�     	�     	�     	�     
     
     
     
"     
'     
3     
A     
N     
]   	  
o     
y     
�     
�     
�   
  
�     
�     
�   	  
�     
�     
�     
�     
�     
�     
�               0     =   	  B     L     S     l     t     }     �     �     �   '  �   '  �   D  $   	  i     s     �     �     �     �     �   
  �     �     �     �                0     2   5  8     n     t     �     �     �     �     �   
  �   
  �   	  �  0  �        &     &  ;     b     �     �     �     �     �     �     �     �     �     �     �   	  �          
               +     4     @     M     Z     l     �   (  �     �     �     �     �     �     �     �     �     
          (     6     H     Y     j     s     x     �     �     �     �     �     �     �   
  �     �     �     �               &     ;     L     R     ^     b     w     ~     �     �     �     �   .  �   &     >  /   	  n     x   '  �     �     �     �     �   
  �     �     �          *     9     L     N   4  T     �     �     �     �     �   "  �     �   
  �   
  �     �         $       !      =      	   a       ?   W          /      `                 #       6   L   Y   d         *   M   "               C   F      3   >   J   K   D   B   S      1       +   A   :                 9       
       4      Q                      ;   7   N                 T       I       O           b      (   0       X   5   %   -   E      ]                 Z              P   ,       &   .      [                  8   \           H   <   R   2          '   G                 )   U   V   @   _      ^   c %4d  %s
 %d best incoming links:  %d best outgoing links:  %d most popular nearby:  %s at %s %s days %s of this page %s: %s %s: file not found (diff) 1 day <no matches> Adjust Calendar Cancel Complete. Contents Describe %s here. Diff Diff: %s Edit Edit %s Edit: %s EditText Empty pagename! Fatal PhpWiki Error FindPage Full text search results for '%s' FullTextSearch Go H Height Hits Hits: HomePage Included from %s Last Last Author Last Modified Last Summary Last edited %s Last edited on %s Lock Page Locked Modern MostPopular Next Next Month None Page Locked Page Name Preview Previous Previous Author Previous Month Printer RecentChanges Remove Page Removed page '%s' successfully. Return to %s Save Saved: %s Search See %s tips for editing. Sign In Sign Out Sorry for the inconvenience. String "%s" not found. Synopsis Thank you for editing %s. The %d most popular pages of this wiki: The %d most recent %s are listed below. This page has been locked by the administrator and cannot be edited. Time Zone Title Search Title search results for '%s' TitleSearch Unlock Page UserId Version Version %d Version %s, saved %s Version %s, saved on %s Versions are identical View Source View Source: %s W Width Your careful attention to detail is much appreciated. by %s current version diff from %s locked revision by previous author today version %d version %s yesterday Project-Id-Version: PhpWiki-1.3.4pre
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-09-10 18:03+0200
PO-Revision-Date: 2001-01-27 01:58+0200
Last-Translator: Jon �slund <jooon@hem.passagen.se>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 %4d  %s
 De %d mest anv�nda ing�ende l�nkarna:  De %d mest anv�nda utg�ende l�nkarna:  De %d mest popul�ra grannarna:  %s i %s %s dar %s f�r den h�r sidan %s: %s %s: fila ingen funna (diff) 1 dag <ingen matchar> Justera Kalender Avbyrt Komplett. Inneh�ll Beskriv %s h�r. Diff Diff av: %s Redigera Redigera %s Redigera: %s RedigeraText Ange ett sidnamn! PhpWiki Fatal Error S�kEfterSida S�kresultat f�r fulltexts�kning f�r '%s' Fulltexts�kning "G� till sidan" H H�jd Tr�ffar Tr�ffar: Framsida Ins�ttning fr�n %s Senaste G�reg�ende F�rfattare �ndringsdatum Senaste Kommentar Senast �ndrad %s Senast �ndrad %s L�s Sida L�st Ny|modig MestPopul�r N�sta F�ljande M�nad Ingen Sidan �r L�st Namn p� Sidan �versikt F�reg�ende G�reg�ende F�rfattare F�reg�ende M�nad Skrivare Senaste�ndringar Ta bort sida Tog bort sidan '%s'. Tillbaka till %s Spara Sparade: %s S�k %s: Redigeringstips. Anslut Logga ut Ledsen f�r allt besv�r. "%s" ingen funna. �versikt Tack f�r att du redigerade %s. De %d mest popul�ra sidorna f�r den h�r wikin: De %d senaste %s sidorna listas nedan. Den h�r sidan �r l�st av administrat�ren och kan ej redigeras. Timme Zon Titels�kningen S�kresultat f�r titels�kningen f�r '%s' Titels�kningen L�s upp Sida Namn Version Version %d Version %s, sparade %s Version %s, sparade %s Versionerna �r identiska Visa K�llkoden Visa K�llkoden: %s B Bredd Din omst�nksamhet f�r detaljer �r mycket uppskattad. av %s nuvarande version diff fr�n %s l�st version fr�n f�reg�ende f�rfattare i dag version %d version %s i g�r 