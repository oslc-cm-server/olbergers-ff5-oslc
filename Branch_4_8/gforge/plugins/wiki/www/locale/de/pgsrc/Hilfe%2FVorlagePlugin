Date: Tue, 2 Jan 2007 13:30:17 +0000
Mime-Version: 1.0 (Produced by PhpWiki 1.3.13)
X-Rcs-Id: $Id: Hilfe%2FVorlagePlugin 6193 2008-08-23 17:49:48Z vargenau $
Content-Type: application/x-phpwiki;
  pagename=Hilfe%2FVorlagePlugin;
  pgsrc_version="1 $Revision: 6193 $";
  flags="";
  markup=2;
  charset=iso-8859-1
Content-Transfer-Encoding: binary

!! Synopsis

Vorlage = Parametrische Bl�cke.

Include text from a wiki page and replace certain placeholders by parameters.
Similiar to CreatePage with the template argument, but at run-time.

Similiar to the mediawiki templates but not with the "|" parameter seperator. 
Note: The mediawiki syntax is also supported.

!! Usage
<verbatim>
  <?plugin Template page=Template/Footer?>

  <?plugin Template page=Template/Film vars="title=SomeFilm&year=1999" ?>

  {{Template/Film|title=SomeFilm|year=1999}}
</verbatim>

!! Plugin Argumente
Argument|
  Vorgabewert|
    Beschreibung

page|
  (empty)|
    pagename to be included as template

vars|
  (empty)|
    optional parameters to be expanded inside the template

! Parameter expansion:
  vars="var1=value1&var2=value2"

We only support named parameters, not numbered ones as in mediawiki, and 
the placeholder is %%var%% and not {{~{var~}}} as in mediawiki.

The following predefined variables are automatically expanded if existing:
<verbatim>
  pagename
  mtime     - last modified date + time
  ctime     - creation date + time
  author    - last author
  owner     
  creator   - first author
  SERVER_URL, DATA_PATH, SCRIPT_NAME, PHPWIKI_BASE_URL and BASE_URL
</verbatim>

<noinclude> .. </noinclude> is stripped

! In work:
* ENABLE_MARKUP_TEMPLATE = true: (lib/InlineParser.php)
  Support a mediawiki-style syntax extension which maps 
<verbatim>
    {{Template/Film|title=Some Good Film|year=1999}}
</verbatim>
  to 
<verbatim>
    <?plugin Template page=Template/Film vars="title=Some Good Film&year=1999" ?>
</verbatim>

!! Examples

<verbatim>
  <?plugin Template page=Vorlage/Beispiel vars="title=TestTitle" ?>
</verbatim>

Standard syntax:
  <?plugin Template page=Vorlage/Beispiel vars="title=TestTitle" ?>

Shorter syntax:
  {{Vorlage/Beispiel|title=TestTitel}}

-------------
PhpWikiDokumentation Hilfe:WikiPlugin
