��       o     �   �        	`     	a     	j   n  	q     	�   !  	�     
	   	  
     
&   	  
-     
7     
N     
`     
e     
n     
s     
{     
�     
�     
�     
�   !  
�     
�     
�     
�     
�     
�     
�               #     /     =     L   
  ^     i   
  o     z   	  �     �     �     �     �     �     �     �     �   	  �     �                "     >     \     q     y     �     �     �     �   
  �     �     �     �     �     
        	       &     -     >     E     ^     q     z     �   '  �   '  �        D     P  c     �     �     �     �     �     
        
  "     -     B     Z     q     }     �     �     �   '  �   5  �   P       b     p     v     �     �     �     �     �   
  �   
  �     �  z  �     Y     b   c  i     �   '  �     �   
            !   A  (     j     }     �     �     �     �     �     �     �   
  �   )  �          ,     .     5     <     D     J     V     o     }     �     �     �     �     �     �                    (     6     C     Q     Z     _     m     y     �     �     �     �     �     �     �     �               %     6     B     O     `     r     �     �     �     �     �     �     �     �     �        #      +  D   &  p     �   I  �   W  �   
  U     `   	  �     �     �     �     �   	  �     �     �     �   
            .     0   )  8   3  b   :  �   W  �     )     7     ?     N     S     Z     _     o   	  ~   	  �     �      /          M   G   m   d   U          O   :   [          W   (       1   F   h      \   P          N       "   !   j   9   f   R   +          ;      7   S   I          K   T   a      g       4                    3   @   `   8   -   &   Q                  $   D   ,   2      0           A   6   b      k   E   B       L          _      Z       V   n   o   H   ]             Y   	          e      >       =                      l       .   <          X       '   
   i   ^   C               *   #   5                 J   )   %                    c           ?     %4d  %s
 %s: %s (Copy your changes to the clipboard. You can try editing a different page or save your text in a text editor.) (diff) - saved to database as version %d An unnamed PhpWiki BackLinks Cancel Complete. Couldn't connect to %s Describe %s here. Diff Diff: %s Edit Edit %s Edit: %s EditText Edited by: %s Fatal PhpWiki Error FindPage Full text search results for '%s' FullTextSearch H Height Hits Hits: Home HomePage Include of '%s' failed. Last Author Last Modified Last edited %s Last edited on %s LdapSearch Links LiveSearch Loading up virgin wiki Lock Page Locked MIME file %s MostPopular New page Newer page: Next None Page Locked Page Name Page removed %s PageHistory PhpWikiAdministration PhpWikiAdministration/Chmod PhpWikiAdministration/Replace PhpWikiDocumentation Preview Previous Previous Author Printer RecentChanges RecentVisitors References ReleaseNotes Remove Page Removed by: %s Removed page '%s' successfully. Return to %s Save Saved: %s Search Search & Replace See %s See %s tips for editing. Serialized file %s Skipping Sorry for the inconvenience. Thank you for editing %s. The %d most popular pages of this wiki: The %d most recent %s are listed below. The PhpWiki programming team This page has been locked by the administrator and cannot be edited. This page has been locked by the administrator so your changes can not be saved. Title Search Title search results for '%s' TitleSearch Unlock Page Update Preferences UserPreferences Version Version %d Version %s, saved %s Version %s, saved on %s Versions are identical View Source View Source: %s W Width You are about to remove '%s'! You must specify a directory to dump to Your careful attention to detail is much appreciated. Your home page has not been created yet so your preferences cannot not be saved. [%d] See [%s] by %s current version diff from %s locked plain file %s saved as %s version %d version %s version <em>%s</em> Project-Id-Version: PhpWiki-1.3.4pre
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-09-10 18:03+0200
PO-Revision-Date: 2000-09-30 02:23+0200
Last-Translator: Jan Nieuwenhuizen <janneke@gnu.org>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
Date: 1998-05-26 11:26:28+0200
From:  <janneke@gnu.org>
 %4d  %s
 %s: %s Kopier je veranderingen naar het klipbord of een andere tijdelijke plaats (bijv. een tekst editor). (diff) - bewaard in de data base als versie %d Naamloos PhpWiki TerugLinks Annuleer Klaar. Kan verbinding naar data base %s niet tot stand brengen, geef op. Beschrijf %s hier. Diff Diff van: %s Verander Verander %s Verander: %s VeranderTekst Verander von: %s PhpWiki Fatale Fout ZoekPagina Volledige tekst zoek resultaten naar '%s' VolledigeTekstZoek H Hoogte Teller Teller: Thuis ThuisPagina Insert van '%s' gefaald. Vorige Auteur meest recente verandering Meest recente verandering %s Meest recente verandering op %s LdapZoek Links LiveZoek Laden van maagdelijke wiki Open Pagina L�st MIME bestand %s Meestbezochte Nieuw pagina Nieuw pagina: Volgende Geen Sidan �r L�st Pagina Naam Verwijder %s PaginaGeschiedenis PhpWikiBeheer PhpWikiBeheer/Chmod PhpWikiBeheer/Vervangt PhpWikiDocumentatie Voorvertonig Vorige Vorige Auteur Drukker RecenteVeranderingen RecenteBezoekers Referenties UitgaveNoten Verwijder Pagina Verwijder von: %s Pagina '%s' verwijderd. Terug naar %s Bewaar Bewaard: %s Zoek Zoek & Vervangt Zie %s %s tips for veranderen. Geserialiseerd bestand %s Overgeslagen Excuses voor het ongemak. Bedankt voor het veranderen van %s. De %d meestbezochte pagina's van deze Wiki: De %d meest recent %s staan hieronder. Het PhpWiki programmeerteam Deze pagina is afgesloten door de beheerder en kan niet veranderd worden. Deze pagina is afgesloten door de beheerder en je veranderingen kan niet bewaar worden. Titel Zoek Titel zoek resultaten naar '%s' TitelZoek Sluit Pagina Af Zet Voorkeuren GebruikersVoorkeuren Versie Versie %d Versie %s, bewarrd %s Versie %s, bewarrd op %s Versies zijn identiek Bron Tekst Bron tekst: %s B Breedte Je staat te verwijderen '%s' op het punt! Je moet een directory specificeren om in te storten Je zorgvuldige aandacht voor detail wordt erg gewaardeerd. Deze pagina is afgesloten door de beheerder en je veranderingen kan niet bewaar worden. [%d] Zie [%s] door %s huidige versie diff van %s l�st plat bestand %s bewaard als %s versie %d versie %s versie <em>%s</em> 