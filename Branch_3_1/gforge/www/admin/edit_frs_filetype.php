<?php
/**
  *
  * Site Admin page to edit File Release System file types
  *
  * SourceForge: Breaking Down the Barriers to Open Source Development
  * Copyright 1999-2001 (c) VA Linux Systems
  * http://sourceforge.net
  *
  * @version   $Id$
  *
  */


$unit        = 'file type';
$table       = 'frs_filetype';
$primary_key = 'type_id';

include_once('admin_table.php');

?>
