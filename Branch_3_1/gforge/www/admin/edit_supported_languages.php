<?php
/**
  *
  * Site Admin page to edit supported language treanslations
  *
  * SourceForge: Breaking Down the Barriers to Open Source Development
  * Copyright 1999-2001 (c) VA Linux Systems
  * http://sourceforge.net
  *
  * @version   $Id$
  *
  */


$unit        = 'supported language';
$table       = 'supported_languages';
$primary_key = 'language_id';

include_once('admin_table.php');

?>
