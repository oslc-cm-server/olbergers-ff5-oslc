<?php
/**
  * SourceForge login page
  *
  * This is main SF login page. It takes care of different account states
  * (by disallowing logging in with non-active account, with appropriate
  * notice).
  *
  * SourceForge: Breaking Down the Barriers to Open Source Development
  * Copyright 1999-2001 (c) VA Linux Systems
  * http://sourceforge.net
  *
  * @version   $Id$
  */

Header( "Expires: Wed, 11 Nov 1998 11:11:11 GMT"); 
Header( "Cache-Control: no-cache"); 
Header( "Cache-Control: must-revalidate"); 

require_once('pre.php');

/*

if (!session_issecure()) {
	//force use of SSL for login
	header('Location: https://'.$HTTP_HOST.'/account/login.php');
}

*/

// ###### first check for valid login, if so, redirect

if ($login) {
	$success=session_login_valid(strtolower($form_loginname),$form_pw);
	if ($success) {
		/*
			You can now optionally stay in SSL mode
		*/
		if ($return_to) {
			header ("Location: " . $return_to);
			exit;
		} else {
			header ("Location: /my/");
			exit;
		}
	}
}

if ($session_hash) {
	//nuke their old session
	session_logout();
}

//echo "\n\n$session_hash";
//echo "\n\nlogged in: ".session_loggedin();

$HTML->header(array('title'=>'Login','pagename'=>'account_login'));

if ($login && !$success) {
		
	if ($feedback == "Account Pending") {

		echo $Language->getText('account_login', 'pending_account', array($form_loginname));

	} else {
		
		echo '<h2 style="color:red">'. $feedback .'</h2>';
		if (stristr($feedback, "deleted")) {
			echo $Language->getText('account_login', 'deleted_account', $GLOBALS[sys_name]);
		}
	} //end else

}

?>
	
<p>
<span style="color:red"><strong><?php echo $Language->getText('account_login', 'cookiewarn'); ?></strong></span>
</p>
<form action="<?php echo $PHP_SELF; ?>" method="post">
<input type="hidden" name="return_to" value="<?php echo $return_to; ?>" />
<p>
<?php echo $Language->getText('account_login', 'loginname'); ?>
<br /><input type="text" name="form_loginname" value="<?php echo htmlspecialchars($form_loginname); ?>" />
</p>
<p>
<?php echo $Language->getText('account_login', 'passwd'); ?>
<br /><input type="password" name="form_pw" />
</p>
<p>
<input type="submit" name="login" value="<?php echo $Language->getText('account_login', 'login'); ?>" />
</p>
</form>
<p>
<a href="lostpw.php"><?php echo $Language->getText('account_login', 'lostpw'); ?></a></p>
<p>
<a href="register.php"><?php echo $Language->getText('account_login', 'newaccount'); ?></a></p>

<?php

$HTML->footer(array());

?>
