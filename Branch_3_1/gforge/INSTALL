Installation of GForge has been made much easier than the old SF2.x
codebase.  A great deal of unnecessary code and scalability hacks have
been removed to make the installation and use of GForge practical for
just about any Linux admin.

*
*  PROFESSIONAL SUPPORT
*
*  If you need professional support, customization,  
*  or advanced reporting tools for GForge, visit 
*  http://gforge.org/pro/
*

BASICS OF INSTALLATION
----------------------

You will need: 
	PostgreSQL 7.1 or higher ( http://postgresql.org/ )
	PHP 4.x or higher ( http://www.php.net/ )
	Apache 1.2x or higher ( http://www.apache.org/ )

	Optional (but highly recommended)
	PHP Accelerator ( http://www.php-accelerator.co.uk/ )

	Optional Mailing list support:
	GNU MailMan ( http://www.gnu.org/software/mailman/ )
	Python ( http://www.python.org/ )

	Optional Jabber Support:
	Jabberd ( http://jabberd.jabberstudio.org/ )

	Optional Gantt Charting and Graphing Support:
	JPGraph: ( http://www.aditus.nu/jpgraph/ )

Compile and install PostgreSQL, then PHP/Apache as per the
instructions. PHP should be compiled into Apache, as well as compiled
as a separate CGI. When compiling PHP, don't forget the --with-pgsql
option for PostgreSQL support.

If you are going to use Gantt charting and other graphing features,
your PHP must be compiled --with-gd. Follow the JPGraph installation
instructions (extra fonts for JPGraph are not necessary). Be sure your
/etc/gforge/local.inc file contains the proper path to the jpgraph/src/
directory.

Install Python and MailMan as per the instructions.


Database Setup
--------------

Create a blank database for postgres

# createdb alexandria

You may need to add Pl/Pgsql as a language.  To do so:

[user]$ su -
[root]# su postgres
[postgres]$ createlang plpgsql template1
[postgres]$ exit
[root]# exit

Next, import the database schema:

# psql alexandria < db/gforge3.sql > import.log

Be sure to watch for any errors during import.


WEB SETUP
---------

Move the GForge tarball into position and make the www/ directory the
"Document Root".

Apache's http.conf file should have these values:

ErrorDocument 404 /404.php
php_value include_path ".:/path/to/gforge/:/path/to/gforge/www/include/"
php_admin_value default_charset "UTF-8"
php_flag register_globals on
php_flag file_uploads on
<Location /projects>
  ForceType application/x-httpd-php
</Location>
<Location /users>
  ForceType application/x-httpd-php
</Location>

If you are using Apache 2, you should also add:

AcceptPathInfo on


PHP setup
---------

You may also add the include_path to the php.ini, as it will be
necessary for your php cgi to run the cron jobs.  Alternatively, you
may run the cronjobs with:

/usr/bin/php -dinclude_path='.:/path/to/gforge/:/path/to/gforge/www/include/' -q <file>

if you do not want to edit your php.ini file.


FRS - File Release System
-------------------------

FRS has been radically simplified. Simply create a directory and make
it owned by the webserver-user. Usually "chown -R nobody:nobody mydir"
will do the trick.

This directory will be referenced in the GForge Config File as $sys_upload_dir

GForge Config File
--------------

In the GForge distribution, you will find etc/local.inc. Move it to
/etc/gforge/local.inc and edit all of the settings.

Giving the first user "Site Admin" privileges. Register a new user,
and confirm it via the email link. Then enter the postgres command
line and issue these commands:

insert into user_group (user_id,group_id,admin_flags) values (*****YOUR NEW ID*****,1,'A');

/etc/aliases - add this line and run "newaliases"
noreply:        /dev/null


PEER RATINGS
------------

Add yourself, and any others you wish, to the "Peer Ratings" project,
which should be at /projects/peerrating/ on the website. Make yourself
an "admin" of the project, and then proceed to "rate" other users on
the website.

Members of the "Peer Ratings" project, who are "admins" of the project
become the first trusted users. This is the only way to prime the pump
for the peer ratings system.


CRON JOBS
---------

Cron jobs are in the cronjobs/ directory and the README file contains
a sample crontab. This gives you the basic cronjobs for updating
certain statistics and data on the site.

/cronjobs/cvs-cron/ contains scripts useful for creating blank cvs 
trees and managing the /etc/groups /etc/passwd and /etc/shadow files.
See /cronjobs/README.root for more info.

/cronjobs/mail/ contains files useful for the creation of new mailing 
lists in mailman and creating the /etc/aliases file.

