INSERT INTO supported_languages 
VALUES (1,'English','English.class','English','en'); 
INSERT INTO supported_languages 
VALUES (2,'Japanese','Japanese.class','Japanese','ja'); 
INSERT INTO supported_languages 
VALUES (3,'Hebrew','Hebrew.class','Hebrew','iw'); 
INSERT INTO supported_languages 
VALUES (4,'Spanish','Spanish.class','Spanish','es'); 
INSERT INTO supported_languages 
VALUES (5,'Thai','Thai.class','Thai','th'); 
INSERT INTO supported_languages 
VALUES (6,'German','German.class','German','de'); 
INSERT INTO supported_languages 
VALUES (7,'French','French.class','French','fr'); 
INSERT INTO supported_languages 
VALUES (8,'Italian','Italian.class','Italian','it'); 
INSERT INTO supported_languages 
VALUES (9,'Norwegian','Norwegian.class','Norwegian','no'); 
INSERT INTO supported_languages 
VALUES (10,'Swedish','Swedish.class','Swedish','sv'); 
INSERT INTO supported_languages 
VALUES (11,'Chinese','Chinese.class','Chinese','zh'); 
INSERT INTO supported_languages 
VALUES (12,'Dutch','Dutch.class','Dutch','nl'); 
INSERT INTO supported_languages 
VALUES (13,'Esperanto','Esperanto.class','Esperanto','eo'); 
INSERT INTO supported_languages 
VALUES (14,'Catalan','Catalan.class','Catalan','ca'); 
INSERT INTO supported_languages 
VALUES (15,'Polish','Polish.class','Polish','pl'); 
INSERT INTO supported_languages 
VALUES (16,'Portuguese (Brazillian)','PortugueseBrazillian.class','PortugueseBrazillian','pt'); 
INSERT INTO supported_languages 
VALUES (17,'Russian','Russian.class','Russian','ru'); 
INSERT INTO supported_languages 
VALUES (18,'Portuguese','Portuguese.class','Portuguese','pt'); 
INSERT INTO supported_languages 
VALUES (19,'Greek','Greek.class','Greek','el'); 
INSERT INTO supported_languages 
VALUES (20,'Bulgarian','Bulgarian.class','Bulgarian','bg'); 
INSERT INTO supported_languages 
VALUES (21,'Indonesian','Indonesian.class','Indonesian','id');
