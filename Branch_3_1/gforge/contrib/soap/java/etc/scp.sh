#!/bin/bash

CLASSPATH=../build/
CLASSPATH=$CLASSPATH:../lib/axis.jar
CLASSPATH=$CLASSPATH:../lib/jaxrpc.jar
CLASSPATH=$CLASSPATH:../lib/commons-logging.jar
CLASSPATH=$CLASSPATH:../lib/commons-discovery.jar
CLASSPATH=$CLASSPATH:../lib/saaj.jar
CLASSPATH=$CLASSPATH:../lib/jfreechart-0.9.8.jar
CLASSPATH=$CLASSPATH:../lib/jcommon-0.8.0.jar
export CLASSPATH


