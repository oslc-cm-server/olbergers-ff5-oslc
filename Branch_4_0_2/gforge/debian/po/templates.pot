#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-11-28 12:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:9
#: ../dsf-helper/shellhost-variables.templates:3
msgid "Your shell server"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:9
#: ../dsf-helper/shellhost-variables.templates:3
msgid "The hostname of the server that will host your Gforge shell accounts"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:18
#: ../dsf-helper/downloadhost-variables.templates:3
msgid "Your download server"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:18
msgid ""
"The hostname of the server that will host your Gforge packages.  It should "
"not be the same as your main Gforge host."
msgstr ""

#. Type: string
#. Default
#: ../gforge-db-postgresql.templates.dsfh-in:26
#: ../gforge-dns-bind9.templates.dsfh-in:23
msgid "admin"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:27
#: ../gforge-dns-bind9.templates.dsfh-in:24
msgid "The Gforge administrator login"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:27
#: ../gforge-dns-bind9.templates.dsfh-in:24
msgid ""
"This Gforge account will have all privileges on the Gforge system.  It is "
"needed to approve the creation of the projects."
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:33
#: ../dsf-helper/host-variables.templates:3
msgid "Your IP address"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:33
#: ../dsf-helper/host-variables.templates:3
msgid ""
"The IP address of the server that will host your Gforge installation. This "
"is needed for the Apache virtualhosting configuration."
msgstr ""

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:39
#: ../gforge-dns-bind9.templates.dsfh-in:30
msgid "The Gforge administrator password"
msgstr ""

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:39
#: ../gforge-dns-bind9.templates.dsfh-in:30
msgid ""
"This Gforge account will have all privileges on the Gforge system.  It is "
"needed to approve the creation of the projects.  Please type a password here."
msgstr ""

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:46
#: ../gforge-dns-bind9.templates.dsfh-in:37
msgid "The Gforge administrator password - again"
msgstr ""

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:46
#: ../gforge-dns-bind9.templates.dsfh-in:80
#: ../dsf-helper/dbpasswd-variables.templates:9
#: ../dsf-helper/ldap-variables.templates:20
msgid "Please re-type the password for confirmation."
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:52
#: ../gforge-dns-bind9.templates.dsfh-in:43
msgid "The initial list of skills"
msgstr ""

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:52
#: ../gforge-dns-bind9.templates.dsfh-in:43
msgid ""
"Gforge allows users to define a list of their skills, to be chosen amongst "
"those present in the database.  This list is the initial list of skills that "
"will enter the database.  Please enter the skill names separated by semi-"
"colons `;'."
msgstr ""

#. Type: password
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:12
#: ../dsf-helper/dbpasswd-variables.templates:3
msgid "Password used for the database"
msgstr ""

#. Type: password
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:12
msgid ""
"The connection to the DB system requires a password.  Please choose a "
"password here."
msgstr ""

#. Type: password
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:18
#: ../dsf-helper/dbpasswd-variables.templates:9
msgid "Password used for the database - again"
msgstr ""

#. Type: string
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:51
#: ../dsf-helper/ldap-variables.templates:3
msgid "The LDAP base DN"
msgstr ""

#. Type: string
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:51
#: ../dsf-helper/ldap-variables.templates:3
msgid ""
"The DN is used to refer to the LDAP directory unambiguously.  You could use, "
"for instance, \"dc=gforge,dc=example,dc=com\""
msgstr ""

#. Type: string
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:57
#: ../dsf-helper/ldap-variables.templates:9
msgid "The LDAP host"
msgstr ""

#. Type: string
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:57
#: ../dsf-helper/ldap-variables.templates:9
msgid "The hostname of the LDAP server."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:63
#: ../gforge-mta-exim.templates.dsfh-in:8
#: ../gforge-mta-postfix.templates.dsfh-in:8
msgid "Do you want mail to ${noreply} to be deleted?"
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:63
#: ../gforge-mta-exim.templates.dsfh-in:8
#: ../gforge-mta-postfix.templates.dsfh-in:8
msgid ""
"Gforge sends plenty of e-mail from the \"${noreply}\" address, and maybe "
"even some e-mail to that address too."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:63
#: ../gforge-mta-exim.templates.dsfh-in:8
#: ../gforge-mta-postfix.templates.dsfh-in:8
msgid ""
"It is advised that you let the package direct e-mail to that address to a "
"black hole (/dev/null), unless you have another use for that address."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:63
#: ../gforge-mta-exim.templates.dsfh-in:8
#: ../gforge-mta-postfix.templates.dsfh-in:8
msgid "Accepting this option will perform that redirection."
msgstr ""

#. Type: password
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:74
#: ../dsf-helper/ldap-variables.templates:14
msgid "LDAP password used to add users from the web"
msgstr ""

#. Type: password
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:74
#: ../dsf-helper/ldap-variables.templates:14
msgid ""
"In order to add users into the LDAP directory from the web, you need to "
"provide a password."
msgstr ""

#. Type: password
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:80
#: ../dsf-helper/ldap-variables.templates:20
msgid "LDAP password - again"
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:85
msgid "Generate an SSL certificate"
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:85
msgid ""
"You need a valid SSL/TLS certificate to run Gforge. If you don't already "
"have one, you'll have to run the \"mod-ssl-makecert\" command to generate "
"one, and restart Apache afterwards."
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:85
msgid ""
"Please note you will not be able to start Apache if you have no such "
"certificate."
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:96
#: ../gforge-shell-ldap.templates.dsfh-in:5
msgid "Gforge requires appropriate PAM-LDAP configuration"
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:96
#: ../gforge-shell-ldap.templates.dsfh-in:5
msgid ""
"Gforge requires the libpam-ldap package to be configured appropriately.  You "
"might need to \"dpkg-reconfigure libpam-ldap\" in order to fix incorrect "
"parameters."
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:96
#: ../gforge-shell-ldap.templates.dsfh-in:5
msgid ""
"Known good values are: LDAP protocol version=3, make root database "
"admin=yes, root login account=\"cn=admin,dc=<your-dc-here>\", crypt to use "
"for passwords=crypt.  Make sure that you type the same password for the "
"libpam-ldap root password and the Gforge LDAP password."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:109
msgid "Do you want a simple DNS setup?"
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:109
msgid ""
"You can have a simple DNS setup wich uses wildcards to map all project web-"
"hosts to a single IP, and direct all the cvs-hosts to a single CVS server, "
"or a complex setup wich allows you to have many servers as project web "
"servers or CVS servers."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:109
msgid ""
"Even though you have a simple DNS setup, you can still have seperate boxes "
"for your project servers, it just assumes you have all the project web dirs "
"on the same server and a single server for CVS."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:123
#: ../dsf-helper/replace-files.templates:18
msgid "Do you want ${file} to be updated?"
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:123
#: ../dsf-helper/replace-files.templates:18
msgid ""
"In order for Gforge to be fully deinstalled, some changes must be made to "
"the ${file} file."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:123
#: ../dsf-helper/replace-files.templates:18
msgid ""
"I can either do them for you (and backup the current version in ${file}."
"gforge-old), or write the changes to another file (${file}.gforge-new) and "
"let you propagate the changes to ${file} yourself."
msgstr ""

#. Type: boolean
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:123
#: ../dsf-helper/replace-files.templates:18
msgid "Shall I modify the ${file} automatically?"
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:136
#: ../dsf-helper/replace-files.templates:31
msgid "Please check ${file}"
msgstr ""

#. Type: note
#. Description
#: ../gforge-dns-bind9.templates.dsfh-in:136
#: ../dsf-helper/replace-files.templates:31
msgid ""
"You have chosen not to let the Gforge package update the ${file} file.  Very "
"well, I have not changed it.  Instead, I have written the suggested changes "
"into ${file}.gforge-new. Please check this file and apply the appropriate "
"changes to ${file}."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:3
msgid "Your Gforge domain or subdomain name"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:3
msgid ""
"The domain that will host your Gforge installation.  Some services will be "
"given their own subdomain in that domain (cvs, lists, etc.)."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:9
msgid "The Gforge admin email address."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:9
msgid ""
"The email address of the Gforge administrator of your site.  Needed in case "
"a problem occurs."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:16
msgid "The Gforge system name"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:16
msgid "This name is used in various places throughout the system."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/cvshost-variables.templates:3
msgid "Your CVS server"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/cvshost-variables.templates:3
msgid ""
"The hostname of the server that will host your Gforge CVS repositories.  It "
"should not be the same as your main Gforge host."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:3
msgid "Your database server"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:3
msgid ""
"The IP address (or host name) of the server that will host your GForge "
"database.  This is needed for virtually all the subpackages."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:10
msgid "Your database name"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:10
msgid ""
"The name of the database that will host your GForge database.  This is "
"needed for virtually all the subpackages."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:17
msgid "Your database admin username"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:17
msgid ""
"The admin username of the database that will host your GForge database.  "
"This is needed for virtually all the subpackages."
msgstr ""

#. Type: password
#. Description
#: ../dsf-helper/dbpasswd-variables.templates:3
msgid ""
"The connecton to the DB system requires a password.  Please choose a "
"password here."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/downloadhost-variables.templates:3
msgid ""
"The hostname of the server that will host your Gforge packages. It should "
"not be the same as your main Gforge host."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:4
msgid "The news admin group id"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:4
msgid ""
"Members of the news admin group can approve news for the Gforge main page.  "
"This group id MUST NOT be 1. You only need to touch this group if you "
"upgrade from a previous version and want to keep your data."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:12
msgid "The stats admin group id"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:17
msgid "The peer rating admin group id"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/lists-variables.templates:3
msgid "Your mailing-lists server"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/lists-variables.templates:3
msgid ""
"The hostname of the server that will host your Gforge mailing-lists. It "
"should not be the same as your main Gforge host."
msgstr ""

#. Type: boolean
#. Description
#: ../dsf-helper/migration.templates:4
msgid "Migrate to Gforge?"
msgstr ""

#. Type: boolean
#. Description
#: ../dsf-helper/migration.templates:4
msgid ""
"The free software version of the Sourceforge software has been renamed to "
"\"Gforge\".  Accordingly, the Debian package has been renamed to \"gforge\", "
"and \"sourceforge\" is no longer supported or maintained.  The transition to "
"Gforge should be rather simple, as most of the work is automated: once this "
"package has been successfully installed, just install the \"gforge\" package "
"and things should go smoothly."
msgstr ""

#. Type: boolean
#. Description
#: ../dsf-helper/migration.templates:4
msgid ""
"You still have the option not to upgrade to Gforge yet, for instance if you "
"want to backup things beforehand.  If you refuse the upgrade, nothing will "
"happen."
msgstr ""

#. Type: note
#. Description
#: ../dsf-helper/migration.templates:19
msgid "Sourceforge is no more"
msgstr ""

#. Type: note
#. Description
#: ../dsf-helper/migration.templates:19
msgid ""
"Sourceforge has been renamed Gforge, and is no longer maintained as free "
"software. This \"sourceforge\" package is only here to help you upgrade to "
"Gforge.  Once you have installed this package and the one it depends on "
"(gforge-sourceforge-transition), your data have been saved, and you can "
"safely uninstall the \"sourceforge\" package and proceed to install Gforge."
msgstr ""

#. Type: boolean
#. Description
#: ../dsf-helper/replace-files.templates:4
msgid ""
"In order for Gforge to fully work, some changes must be made to the ${file} "
"file."
msgstr ""

#. Type: boolean
#. Description
#: ../dsf-helper/replace-files.templates:4
msgid "Shall I modify ${file} automatically?"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/users-variables.templates:3
msgid "Your user mail redirector server"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/users-variables.templates:3
msgid ""
"The hostname of the server that will host your Gforge user mail redirector. "
"It should not be the same as your main Gforge host."
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Bulgarian"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Catalan"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Chinese"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Dutch"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Esperanto"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "French"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "German"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Greek"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Hebrew"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Indonesian"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Italian"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Japanese"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Korean"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Latin"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Norwegian"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Polish"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "PortugueseBrazilian"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Portuguese"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Russian"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "SimplifiedChinese"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Spanish"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Swedish"
msgstr ""

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:3
msgid "Thai"
msgstr ""

#. Type: select
#. DefaultChoice
#: ../dsf-helper/web-variables.templates:4
msgid "English"
msgstr ""

#. Type: select
#. Description
#: ../dsf-helper/web-variables.templates:5
msgid "Default language"
msgstr ""

#. Type: select
#. Description
#: ../dsf-helper/web-variables.templates:5
msgid "Here is the default language for web pages."
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/web-variables.templates:11
msgid "Default theme"
msgstr ""

#. Type: string
#. Description
#: ../dsf-helper/web-variables.templates:11
msgid ""
"Here is the default theme for web pages, be carreful to enter a valid name."
msgstr ""
