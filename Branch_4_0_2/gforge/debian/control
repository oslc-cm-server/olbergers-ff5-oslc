Source: gforge
Section: devel
Priority: optional
Maintainer: Roland Mas <lolando@debian.org>
Build-Depends-Indep: debhelper (>= 4.1.16), sharutils, docbook-to-man
Standards-Version: 3.6.1

Package: gforge
Architecture: all
Conflicts: sourceforge
Depends: debconf (>= 1.0.32), gforge-common (= ${Source-Version}), gforge-web-apache (= ${Source-Version}) | gforge-web (= ${Source-Version}), gforge-db-postgresql (= ${Source-Version}) | gforge-db (= ${Source-Version}), gforge-mta-exim4 (= ${Source-Version}) | gforge-mta-postfix (= ${Source-Version}) | gforge-mta-exim (= ${Source-Version}) | gforge-mta (= ${Source-Version}), gforge-shell-postgresql (= ${Source-Version}) | gforge-shell (= ${Source-Version}), gforge-ftp-proftpd (= ${Source-Version}) | gforge-ftp (= ${Source-Version}), gforge-dns-bind9 (= ${Source-Version}) | gforge-dns (= ${Source-Version}), gforge-lists-mailman (= ${Source-Version}) | gforge-lists (= ${Source-Version})
Suggests: gforge-cvs (= ${Source-Version})
Description: Collaborative development tool - meta-package
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This meta-package installs a complete GForge site.

Package: gforge-common
Architecture: all
Depends: debconf (>= 1.0.32), cpio, lockfile-progs
Conflicts: sourceforge
Description: Collaborative development tool - shared files
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package contains files and programs used by several other
 subpackages.

Package: gforge-web-apache
Architecture: all
Depends: gforge-common, gforge-db-postgresql | gforge-db, libapache2-mod-php4 | apache (>= 1.3.26) | apache-ssl (>= 1.3.26) | apache-perl (>= 1.3.26), apache2 (>= 2.0.52) | libapache-mod-ssl | apache-ssl, libapache2-mod-php4 | libapache-mod-php4 | php4, php4-cgi, php4-pgsql, php4-gd, perl, perl-suid, libdbi-perl, libdbd-pg-perl, debianutils (>= 1.7), debconf (>= 1.0.32), cronolog
Recommends: libphp-jpgraph
Provides: gforge-web
Conflicts: gforge-web
Description: Collaborative development tool - web part (using Apache)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package contains the files needed to run the web part of
 GForge on an Apache webserver.

Package: gforge-db-postgresql
Architecture: all
Depends: gforge-common, postgresql (>= 7.1.2), perl, libdbi-perl, libdbd-pg-perl, libmime-base64-perl, libhtml-parser-perl, libtext-autoformat-perl, libmail-sendmail-perl, debianutils (>= 1.7), debconf (>= 1.0.32), php4-cgi, php4-pgsql
Provides: gforge-db
Conflicts: gforge-db
Description: Collaborative development tool - database (using PostgreSQL)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package installs, configures and maintains the GForge
 database.

Package: gforge-mta-exim4
Architecture: all
Depends: gforge-common, gforge-db-postgresql | gforge-db, perl, debianutils (>= 1.7), debconf (>= 1.0.32), exim4, exim4-daemon-heavy
Provides: gforge-mta
Conflicts: gforge-mta
Description: Collaborative development tool - mail tools (using Exim 4)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package configures the Exim 4 mail transfer agent to run
 GForge.

Package: gforge-mta-exim
Architecture: all
Depends: gforge-common, gforge-ldap-openldap | gforge-ldap, perl, debianutils (>= 1.7), debconf (>= 1.0.32), exim | exim-tls
Provides: gforge-mta
Conflicts: gforge-mta
Description: Collaborative development tool - mail tools (using Exim)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package configures the Exim mail transfer agent to run
 GForge.

Package: gforge-mta-postfix
Architecture: all
Depends: gforge-common, gforge-db-postgresql | gforge-db, perl, debianutils (>= 1.7), debconf (>= 1.0.32), postfix, postfix-pgsql
Provides: gforge-mta
Conflicts: gforge-mta
Description: Collaborative development tool - mail tools (using Postfix)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package configures the Postfix mail transfer agent to run
 GForge.

Package: gforge-shell-ldap
Architecture: all
Depends: gforge-common, gforge-ldap-openldap | gforge-ldap, perl, debianutils (>= 1.7), debconf (>= 1.0.32), ssh, libnss-ldap (>=184-1), libpam-ldap
Provides: gforge-shell
Conflicts: gforge-shell
Description: Collaborative development tool - shell accounts (using LDAP)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package provides shell accounts authenticated via LDAP to
 GForge users.

Package: gforge-shell-postgresql
Architecture: all
Depends: gforge-common, gforge-db-postgresql, perl, debianutils (>= 1.7), debconf (>= 1.0.32), ssh, libnss-pgsql1
Provides: gforge-shell
Conflicts: gforge-shell
Description: Collaborative development tool - shell accounts (using PostgreSQL)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package provides shell accounts authenticated via the PostgreSQL
 database to GForge users.

Package: gforge-cvs
Architecture: all
Depends: gforge-common, gforge-shell-postgresql | gforge-shell, cvs, rcs, apache (>= 1.3.9) | apache-ssl (>= 1.3.9) | apache-perl (>= 1.3.9), libapache-mod-ssl | apache-ssl, perl, debianutils (>= 1.7), debconf (>= 1.0.32), ssh, gforge-plugin-scmcvs
Description: Collaborative development tool - CVS management
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package installs and configures CVS for GForge.

Package: gforge-ftp-proftpd
Architecture: all
Depends: gforge-common, gforge-shell-postgresql | gforge-shell, proftpd, perl, debianutils (>= 1.7), debconf (>= 1.0.32)
Provides: gforge-ftp
Conflicts: gforge-ftp
Description: Collaborative development tool - FTP management (using ProFTPd)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package configures the ProFTPd FTP server for GForge.

Package: gforge-ldap-openldap
Architecture: all
Depends: gforge-common, gforge-db-postgresql | gforge-db, perl, libdbi-perl, libdbd-pg-perl, debianutils (>= 1.7), debconf (>= 1.0.32), slapd (>= 2.0.23-3), ldap-utils
Provides: gforge-ldap
Conflicts: gforge-ldap
Description: Collaborative development tool - LDAP directory (using OpenLDAP)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package configures and maintains an LDAP directory for GForge.

Package: gforge-dns-bind9
Architecture: all
Depends: gforge-common, gforge-db-postgresql | gforge-db, perl, libdbi-perl, libdbd-pg-perl, debianutils (>= 1.7), debconf (>= 1.0.32), bind9 
Provides: gforge-dns
Conflicts: gforge-dns
Description: Collaborative development tool - DNS management (using Bind9)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package configures and maintains the DNS zones for GForge.

Package: gforge-lists-mailman
Architecture: all
Depends: gforge-common, gforge-db-postgresql | gforge-db, gforge-mta-exim4 | gforge-mta, apache2 (>= 2.0.52) | apache (>= 1.3.9) | apache-ssl (>= 1.3.9) | apache-perl (>= 1.3.9), perl, libdbi-perl, libdbd-pg-perl, debianutils (>= 1.7), debconf (>= 1.0.32), mailman (>= 2.1-3)
Provides: gforge-lists
Conflicts: gforge-lists
Description: Collaborative development tool - mailing-lists (using Mailman)
 GForge provides many tools to help collaboration in a
 development project, such as bug-tracking, task management,
 mailing-lists, CVS repository, forums, support request helper, web
 page / FTP hosting, release management, etc.  All these services are
 integrated into one web site and managed via a nice web interface.
 .
 This package controls the interaction of GForge and Mailman.
