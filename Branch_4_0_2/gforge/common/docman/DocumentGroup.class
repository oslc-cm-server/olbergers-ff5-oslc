<?php
/**
 * GForge Doc Mgr Facility
 *
 * Copyright 2002 GForge, LLC
 * http://gforge.org/
 *
 * @version   $Id$
 *
 * This file is part of GForge.
 *
 * GForge is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GForge; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/*
	Document Manager

	by Quentin Cregan, SourceForge 06/2000

	Complete OO rewrite by Tim Perdue 1/2003
*/

require_once('common/include/Error.class');

class DocumentGroup extends Error {

	/**
	 * The Group object.
	 *
	 * @var		object	$Group.
	 */
	var $Group; //object

	/**
	 * Array of data.
	 *
	 * @var		array	$data_array.
	 */
	var $data_array;

	/**
	 *  DocumentGroup - constructor.
	 *
	 *  Use this constructor if you are modifying an existing doc_group.
	 *
	 *	@param	object	Group object.
	 *  @param	array	(all fields from doc_groups) OR doc_group from database.
	 *  @return boolean	success.
	 */
	function DocumentGroup(&$Group, $data=false) {
		$this->Error();

		//was Group legit?
		if (!$Group || !is_object($Group)) {
			$this->setError('DocumentGroup: No Valid Group');
			return false;
		}
		//did Group have an error?
		if ($Group->isError()) {
			$this->setError('DocumentGroup: '.$Group->getErrorMessage());
			return false;
		}
		$this->Group =& $Group;

		if ($data) {
			if (is_array($data)) {
				$this->data_array =& $data;
//
//	should verify group_id
//
				return true;
			} else {
				if (!$this->fetchData($data)) {
					return false;
				} else {
					return true;
				}
			}
		}
	}

	/**
	 *	create - create a new item in the database.
	 *
	 *	@param	string	Item name.
	 *  @return id on success / false on failure.
	 */
	function create($name) {
		global $Language;
		//
		//	data validation
		//
		if (!$name) {
			$this->setError($Language->getText('docman_common_docgroup','name_required'));
			return false;
		}

		$perm =& $this->Group->getPermission (session_get_user());
		if (!$perm || !$perm->isDocEditor()) {
			$this->setPermissionDeniedError();
			return false;
		}
		$sql="INSERT INTO doc_groups (group_id,groupname)
			VALUES ('".$this->Group->getID()."','".htmlspecialchars($name)."')";

		$result=db_query($sql);

		if ($result && db_affected_rows($result) > 0) {
			$this->clearError();
			return true;
		} else {
			$this->setError(db_error());
			return false;
		}

/*
			//
			//	Now set up our internal data structures
			//
			if (!$this->fetchData($id)) {
				return false;
			}
*/
	}

	/**
	 *	fetchData - re-fetch the data for this DocumentGroup from the database.
	 *
	 *	@param	int		ID of the doc_group.
	 *	@return boolean.
	 */
	function fetchData($id) {
		global $Language;
		$res=db_query("SELECT * FROM doc_groups WHERE doc_group='$id'");
		if (!$res || db_numrows($res) < 1) {
			$this->setError($Language->getText('docman_common_docgroup','invalid_id'));
			return false;
		}
		$this->data_array =& db_fetch_array($res);
		db_free_result($res);
		return true;
	}

	/**
	 *	getGroup - get the Group Object this DocumentGroup is associated with.
	 *
	 *	@return Object Group.
	 */
	function &getGroup() {
		return $this->Group;
	}

	/**
	 *	getID - get this DocumentGroup's ID.
	 *
	 *	@return	int	The id #.
	 */
	function getID() {
		return $this->data_array['doc_group'];
	}

	/**
	 *	getName - get the name.
	 *
	 *	@return	String	The name.
	 */
	function getName() {
		return $this->data_array['groupname'];
	}

	/**
	 *  update - update a DocumentGroup.
	 *
	 *  @param	string	Name of the category.
	 *  @return boolean.
	 */
	function update($name) {
		$perm =& $this->Group->getPermission (session_get_user());
		if (!$perm || !$perm->isDocEditor()) {
			$this->setPermissionDeniedError();
			return false;
		}
		if (!$name) {
			$this->setMissingParamsError();
			return false;
		}
		$sql="UPDATE doc_groups
			SET groupname='".htmlspecialchars($name)."'
			WHERE doc_group='". $this->getID() ."'
			AND group_id='".$this->Group->getID()."'";
		$result=db_query($sql);
		if ($result && db_affected_rows($result) > 0) {
			return true;
		} else {
			$this->setError(db_error());
			return false;
		}
	}
}

?>
