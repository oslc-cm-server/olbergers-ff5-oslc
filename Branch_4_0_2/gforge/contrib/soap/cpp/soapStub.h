/* soapStub.h
   Generated by gSOAP 2.3 rev 1 from SoapAPI.h
   Copyright (C) 2001-2003 Genivia inc.
   All Rights Reserved.
*/
#ifndef soapStub_H
#define soapStub_H
#include "stdsoap2.h"

SOAP_BEGIN_NAMESPACE(soap)

/* Enumerations */

/* Classes and Structs */

/* tns:bugUpdateResponse: */
class SOAP_CMAC tns__bugUpdateResponse
{
public:
	char *_bugUpdateResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:userResponse: */
class SOAP_CMAC tns__userResponse
{
public:
	class tns__ArrayOfstring *_userResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:ArrayOfstring: */
class SOAP_CMAC tns__ArrayOfstring
{
public:
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:bugFetchResponse: */
class SOAP_CMAC tns__bugFetchResponse
{
public:
	class tns__Bug *_bugFetchResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:logoutResponse: */
class SOAP_CMAC tns__logoutResponse
{
public:
	char *_logoutResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:getPublicProjectNamesResponse: */
class SOAP_CMAC tns__getPublicProjectNamesResponse
{
public:
	tns__ArrayOfstring *_projectNames;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:getSiteStatsResponse: */
class SOAP_CMAC tns__getSiteStatsResponse
{
public:
	class ArrayOfSiteStatsDataPoint *_siteStats;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:GroupObject: */
class SOAP_CMAC tns__GroupObject
{
public:
	char *group_USCORE_id;
	char *group_USCORE_name;
	char *is_USCORE_public;
	char *status;
	char *unix_USCORE_group_USCORE_name;
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* Array of tns:Bug schema type: */
class SOAP_CMAC ArrayOfBug
{
public:
	tns__Bug *__ptr;
	int __size;
	int __offset;
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* Array of tns:GroupObject schema type: */
class SOAP_CMAC ArrayOfGroupObject
{
public:
	tns__GroupObject *__ptr;
	int __size;
	int __offset;
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:getNumberOfActiveUsersResponse: */
class SOAP_CMAC tns__getNumberOfActiveUsersResponse
{
public:
	char *_activeUsers;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:groupResponse: */
class SOAP_CMAC tns__groupResponse
{
public:
	ArrayOfGroupObject *_groupResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:loginResponse: */
class SOAP_CMAC tns__loginResponse
{
public:
	char *_loginResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* Array of tns:SiteStatsDataPoint schema type: */
class SOAP_CMAC ArrayOfSiteStatsDataPoint
{
public:
	class tns__SiteStatsDataPoint *__ptr;
	int __size;
	int __offset;
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:bugListResponse: */
class SOAP_CMAC tns__bugListResponse
{
public:
	tns__ArrayOfstring *_bugListResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:Bug: */
class SOAP_CMAC tns__Bug
{
public:
	char *id;
	char *summary;
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:bugAddResponse: */
class SOAP_CMAC tns__bugAddResponse
{
public:
	char *_bugAddResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:helloResponse: */
class SOAP_CMAC tns__helloResponse
{
public:
	char *_helloResponse;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:SiteStatsDataPoint: */
class SOAP_CMAC tns__SiteStatsDataPoint
{
public:
	char *date;
	char *users;
	char *pageviews;
	char *sessions;
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:getNumberOfHostedProjectsResponse: */
class SOAP_CMAC tns__getNumberOfHostedProjectsResponse
{
public:
	char *_hostedProjects;	/* return */
public:
	virtual void soap_default(struct soap*); 
	virtual void soap_serialize(struct soap*) const;
	virtual void soap_mark(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*); 
};

/* tns:user: */
struct tns__user
{
	char *func;
	tns__ArrayOfstring *params;
};

/* tns:logout: */
struct tns__logout
{
};

/* tns:hello: */
struct tns__hello
{
	char *parm;
};

/* tns:getNumberOfActiveUsers: */
struct tns__getNumberOfActiveUsers
{
};

/* tns:bugList: */
struct tns__bugList
{
	char *sessionkey;
	char *project;
};

/* tns:bugUpdate: */
struct tns__bugUpdate
{
	char *sessionkey;
	char *project;
	char *bugid;
	char *comment;
};

/* tns:group: */
struct tns__group
{
	char *func;
	tns__ArrayOfstring *params;
};

/* tns:getPublicProjectNames: */
struct tns__getPublicProjectNames
{
};

/* tns:getSiteStats: */
struct tns__getSiteStats
{
};

/* tns:login: */
struct tns__login
{
	char *userid;
	char *passwd;
};

/* tns:bugAdd: */
struct tns__bugAdd
{
	char *sessionkey;
	char *project;
	char *summary;
	char *details;
};

/* tns:getNumberOfHostedProjects: */
struct tns__getNumberOfHostedProjects
{
};

/* tns:bugFetch: */
struct tns__bugFetch
{
	char *sessionkey;
	char *project;
	char *bugid;
};

/* SOAP Header: */
struct SOAP_ENV__Header
{
	void *dummy;	/* transient */
};

/* SOAP Fault Code: */
struct SOAP_ENV__Code
{
	char *SOAP_ENV__Value;
	char *SOAP_ENV__Node;
	char *SOAP_ENV__Role;
};

/* SOAP Fault: */
struct SOAP_ENV__Fault
{
	char *faultcode;
	char *faultstring;
	char *faultactor;
	char *detail;
	struct SOAP_ENV__Code *SOAP_ENV__Code;
	char *SOAP_ENV__Reason;
	char *SOAP_ENV__Detail;
};

/* Types With Custom (De)serializers: */

/* Typedefs */
typedef char *_QName;
typedef char *xsd__integer;
typedef char *xsd__string;

/* Extern */

/* Remote Methods */

SOAP_FMAC3 int SOAP_FMAC4 tns__user(struct soap*, char *, tns__ArrayOfstring *, tns__userResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__logout(struct soap*, tns__logoutResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__hello(struct soap*, char *, tns__helloResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__getNumberOfActiveUsers(struct soap*, tns__getNumberOfActiveUsersResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__bugList(struct soap*, char *, char *, tns__bugListResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__bugUpdate(struct soap*, char *, char *, char *, char *, tns__bugUpdateResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__group(struct soap*, char *, tns__ArrayOfstring *, tns__groupResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__getPublicProjectNames(struct soap*, tns__getPublicProjectNamesResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__getSiteStats(struct soap*, tns__getSiteStatsResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__login(struct soap*, char *, char *, tns__loginResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__bugAdd(struct soap*, char *, char *, char *, char *, tns__bugAddResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__getNumberOfHostedProjects(struct soap*, tns__getNumberOfHostedProjectsResponse *);

SOAP_FMAC3 int SOAP_FMAC4 tns__bugFetch(struct soap*, char *, char *, char *, tns__bugFetchResponse *);

/* Stubs */

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__user(struct soap*, const char*, const char*, char *, tns__ArrayOfstring *, tns__userResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__logout(struct soap*, const char*, const char*, tns__logoutResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__hello(struct soap*, const char*, const char*, char *, tns__helloResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__getNumberOfActiveUsers(struct soap*, const char*, const char*, tns__getNumberOfActiveUsersResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__bugList(struct soap*, const char*, const char*, char *, char *, tns__bugListResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__bugUpdate(struct soap*, const char*, const char*, char *, char *, char *, char *, tns__bugUpdateResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__group(struct soap*, const char*, const char*, char *, tns__ArrayOfstring *, tns__groupResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__getPublicProjectNames(struct soap*, const char*, const char*, tns__getPublicProjectNamesResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__getSiteStats(struct soap*, const char*, const char*, tns__getSiteStatsResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__login(struct soap*, const char*, const char*, char *, char *, tns__loginResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__bugAdd(struct soap*, const char*, const char*, char *, char *, char *, char *, tns__bugAddResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__getNumberOfHostedProjects(struct soap*, const char*, const char*, tns__getNumberOfHostedProjectsResponse *);

SOAP_FMAC5 int SOAP_FMAC6 soap_call_tns__bugFetch(struct soap*, const char*, const char*, char *, char *, char *, tns__bugFetchResponse *);

/* Skeletons */

SOAP_FMAC5 int SOAP_FMAC6 soap_serve(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__user(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__logout(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__hello(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__getNumberOfActiveUsers(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__bugList(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__bugUpdate(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__group(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__getPublicProjectNames(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__getSiteStats(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__login(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__bugAdd(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__getNumberOfHostedProjects(struct soap*);

SOAP_FMAC5 int SOAP_FMAC6 soap_serve_tns__bugFetch(struct soap*);

SOAP_END_NAMESPACE(soap)

#endif

/* end of soapStub.h */
