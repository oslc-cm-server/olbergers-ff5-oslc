Installation of GForge has been made much easier than the old SF2.x
codebase.  A great deal of unnecessary code and scalability hacks have
been removed to make the installation and use of GForge practical for
just about any Linux admin.

*
*  PROFESSIONAL SUPPORT
*
*  If you need professional support, customization,  
*  or advanced reporting tools for GForge, visit 
*  http://gforgegroup.com/
*

BASICS OF INSTALLATION
----------------------

You will need: 
	PostgreSQL 7.3 or higher ( http://postgresql.org/ )
	PHP 4.x or higher ( http://www.php.net/ )
	Apache 1.2x or higher ( http://www.apache.org/ )

	Optional (but highly recommended)
	PHP Accelerator ( http://www.php-accelerator.co.uk/ )

	Optional Mailing list support:
	GNU MailMan ( http://www.gnu.org/software/mailman/ )
	Python ( http://www.python.org/ )

	Optional Jabber Support:
	Jabberd ( http://jabberd.jabberstudio.org/ )

	Optional Gantt Charting and Graphing Support:
	JPGraph: ( http://www.aditus.nu/jpgraph/ )

        Optional (required for some plugins):
        Perl ( http://www.perl.org/ )
        the DBI module ( http://dbi.perl.org/ ) and associated DBD::Pg

Compile and install PostgreSQL, then PHP/Apache as per the
instructions. PHP should be compiled into Apache, as well as compiled
as a separate CGI. When compiling PHP, don't forget the --with-pgsql
option for PostgreSQL support.

If you are going to use Gantt charting and other graphing features,
your PHP must be compiled --with-gd. Follow the JPGraph installation
instructions (extra fonts for JPGraph are not necessary). Be sure your
/etc/gforge/local.inc file contains the proper path to the jpgraph/src/
directory.

Install Python and MailMan as per the instructions.


BLANK DATABASE - FRESH INSTALL
------------------------------

First, make sure you create a 'gforge' user at the unix command prompt:

# adduser gforge

Then create a postgres account for gforge:

[root]# su - postgres

You may need to add Pl/Pgsql as a language.  To do so:

[postgres]$ createlang plpgsql template1

[postgres]# createuser -A -d -E -P gforge

and enter the password for the user.

[postgres]$ exit

[root]# su - gforge
[gforge]# createdb gforge
[gforge]# psql gforge < db/gforge.sql > import.log

Be sure to watch for any errors during import and check the 
import.log file.

Now, find your pg_hba.conf file and edit it to include this line:

local    all         all                                             md5


UPGRADING DATABASE - EXISTING INSTALL
-------------------------------------

You will upgrade your database from a prior version by applying 
each database schema change, in order, and applying it only once.
Only apply the schema changes in the db/ folder that are dated 
AFTER your existing installation.

There may also be migration scripts that have to be run. In the 
db/ folder, looked for migrate-*.php scripts and run them.


WEB SETUP
---------

Move the GForge tarball into position and make the www/ directory the
"Document Root".

The following are sample Apache 2.0 httpd.conf entries:

NameVirtualHost 192.168.1.1

#
#	Primary GForge vhost
#
<VirtualHost 192.168.1.1>
ServerName gforge.company.com
ServerAdmin webmaster@gforge.company.com
DocumentRoot /var/www/gforge3/www
ErrorDocument 404 /404.php
php_value include_path ".:/var/www/gforge3/:/var/www/gforge3/www/include/"
<Files projects>
SetOutputFilter PHP
SetInputFilter PHP
AcceptPathInfo on
</Files>
<Files users>
SetOutputFilter PHP
SetInputFilter PHP
AcceptPathInfo on
</Files>
<Files *.php>
    SetOutputFilter PHP
    SetInputFilter PHP
    AcceptPathInfo On
    LimitRequestBody 2097152
</Files>
#
#	LOCATION may have to be used instead of FILES for some installs
#
#<Location /projects>
#  ForceType application/x-httpd-php
#</Location>
#<Location /users>
#  ForceType application/x-httpd-php
#</Location>
DirectoryIndex index.php
</VirtualHost>

#
#	Lists vhosts - where mailman lives
#
<VirtualHost 192.168.1.1>
ServerName lists.gforge.company.com
ServerAdmin mailman@lists.gforge.company.com
DocumentRoot /var/www/mailman
ScriptAlias   /mailman/ /var/mailman/cgi-bin/
Alias /pipermail/ /var/mailman/archives/public/
DirectoryIndex index.php index.cgi index.html index.htm
</VirtualHost>

#
#	CVS Vhost - allows viewing of CVSWeb for each project
#
<VirtualHost 192.168.1.1>
ServerName cvs.gforge.company.com
ServerAdmin webmaster@cvs.gforge.company.com
DocumentRoot /var/www/cvs
DirectoryIndex index.php index.cgi index.html index.htm
</VirtualHost>

#
#	*.gforge.company.com vhosts
#	Each project can have its own vhost
#
#	WARNING - security is degraded by having this
#	on the same machine as the primary GForge
#
<VirtualHost 192.168.1.1>
  ServerName projects.gforge.company.com
  ServerAlias *.gforge.company.com
  DocumentRoot /var/www/homedirs/groups
  VirtualDocumentRoot /var/www/homedirs/groups/%1
  <Directory /var/www/homedirs/groups>
    Options Indexes
#
#	WARNING - turning on php will allow any user
#	to upload a php file to your server, and include 
#	the gforge local.inc file and get your password to 
#	connect to the database and have total control.
#
    php_flag engine off
    AllowOverride None
    order allow,deny
    allow from all
  </Directory>
  DirectoryIndex index.html index.htm
</VirtualHost>


PHP setup
---------

You may also add the include_path to the php.ini, as it will be
necessary for your php cgi to run the cron jobs. 

register_globals = On
magic_quotes_gpc = On
file_uploads = On
include_path = ".:/var/www/gforge3/:/var/www/gforge3/www/include/"


FRS - File Release System
-------------------------

FRS has been radically simplified. Simply create a directory and make
it owned by the webserver-user. Usually "chown -R nobody:nobody mydir"
will do the trick.

This directory will be referenced in the GForge Config File as $sys_upload_dir


GForge Config File
--------------

In the GForge distribution, you will find etc/local.inc. Move it to
/etc/gforge/local.inc and edit all of the settings.

Usually, you will want to make it readable only by apache:

[root]# chown -R apache:apache /etc/gforge/
[root]# chmod 600 /etc/gforge/local.inc


Site Admin
----------

Site admins are anyone who is an admin of group_id=1

To give the first user "Site Admin" privileges, register a new user,
and confirm it via the email link. Then enter the postgres command
line and issue these commands:

[gforge]# psql gforge

psql> insert into user_group (user_id,group_id,admin_flags) values (*****YOUR NEW NUMERIC USER ID*****,1,'A');


Mail Aliases
------------

/etc/aliases - add this line and run "newaliases"
noreply:        /dev/null


PEER RATINGS
------------

Add yourself, and any others you wish, to the "Peer Ratings" project,
which should be at /projects/peerrating/ on the website. Make yourself
an "admin" of the project, and then proceed to "rate" other users on
the website.

Members of the "Peer Ratings" project, who are "admins" of the project
become the first trusted users. This is the only way to prime the pump
for the peer ratings system.


CRON JOBS
---------

Cron jobs are in the cronjobs/ directory and the README file contains
a sample crontab. This gives you the basic cronjobs for updating
certain statistics and data on the site.

/cronjobs/cvs-cron/ contains scripts useful for creating blank cvs 
trees and managing the /etc/groups /etc/passwd and /etc/shadow files.
See /cronjobs/README.root for more info.

/cronjobs/mail/ contains files useful for the creation of new mailing 
lists in mailman and creating the /etc/aliases file.


[root]# adduser anonymous
[root]# cp /etc/aliases /etc/aliases.org
[root]# cp /etc/shadow /etc/shadow.org
[root]# cp /etc/passwd /etc/passwd.org
[root]# cp /etc/group /etc/group.org
[root]# mkdir /cvsroot

WARNING!!! the following command will blow away any existing root crontab.

[root]# crontab cronjobs/crontab.in

Now edit the paths to the cron scripts:

[root]# crontab -e

IMPORTANT!!!! - the cvs-cron/usergroup.php cron script will meddle 
with your /etc/passwd, /etc/group, and /etc/shadow files. By default,
this cron will save these files with a .new extension. You will have 
to edit the cron script to remove the .new extension, but you must 
make sure that it is properly generating your files or your server 
could be unusable.


JPGRAPH
-------

When you get your preferred version of jpgraph installed, you will 
have to edit one setting in jpgraph.php: DEFINE("USE_CACHE",false);


PERL
----

If you want to use some of the Perl scripts that access the database,
you'll need the DBI and DBD::Pg Perl modules.  On Red Hat systems (and
variants), you can get them by installing the libdbi and libdbd-pgsql
packages.  On Debian systems (and variants), the packages are called
libdbi-perl and libdbd-pg-perl.

  You'll also need to install utils/include.pl to
/usr/lib/gforge/lib/, and put some configuration variables into
/etc/gforge/local.pl.  In particular, you'll need something like:
,----[ local.pl ]
| $sys_default_domain = 'gforge.example.com' ;
| $sys_dbhost = '192.168.12.34' ;
| $sys_dbname = 'gforge' ;
| $sys_dbuser = 'gforge' ;
| $sys_dbpasswd = 'p455w0rd' ;
`----


Jabber Support
--------------

GForge supports the sending of messages to jabber accounts. To 
accomplish this, you must have a user account setup on the jabber 
server that gforge can connect to and send messages.

Once you have that user account, server, and password set up, 
just edit /etc/gforge/local.inc and add the information to the 
jabber section.
