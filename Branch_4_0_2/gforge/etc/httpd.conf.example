Listen 80
<Files .htaccess>
  order allow,deny
  deny from all
</Files>

<IfModule mod_ssl.c>
  Listen 443
</IfModule>

NameVirtualHost 10.GF.OR.GE:80
NameVirtualHost 10.GF.OR.GE:443
# This is magic for virtual hosting!
UseCanonicalName Off
#
# Main host
#
<Directory /path/to/gforge/www>
  Options Indexes FollowSymlinks
  AllowOverride All
  order allow,deny
  allow from all
  php_admin_value include_path "/path/to/gforge/etc/custom:/path/to/gforge:/path/to/gforge/www/include:."
  php_admin_value default_charset "UTF-8"
  php_admin_value register_globals "On"
</Directory>

# HTTP
<VirtualHost 10.GF.OR.GE:80>
	ServerName gforge.company.com
	ServerAlias www.gforge.company.com
	ServerAdmin webmaster@gforge.company.com
	DocumentRoot /path/to/gforge/www
	Alias /images/ /path/to/gforge/www/images/
	DirectoryIndex index.html index.php
	<IfModule mod_userdir.c>
		UserDir disabled
	</IfModule>

	php_admin_value default_charset "UTF-8"

	<Directory /path/to/gforge/www>
		Include   /etc/httpd.secrets
	</Directory>

	ScriptAliasMatch ^/plugins/([^/]*)/cgi-bin/(.*) /usr/lib/gforge/plugins/$1/cgi-bin/$2

	# Projects and Users script
	<Location /projects>
		ForceType application/x-httpd-php
	</Location>
	<Location /users>
		ForceType application/x-httpd-php
	</Location>

	# 404 Error document
	ErrorDocument 404 /404.php
	LogFormat "%h %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" gforge
	CustomLog "|/usr/bin/cronolog /var/log/gforge/%Y/%m/%d/gforge.log" gforge

	# GForge without the DNS delegation
	# Project home pages are in a virtual /www/<group> location
	AliasMatch ^/www/([^/]*)/(.*) /home/groups/$1/htdocs/$2
	ScriptAliasMatch ^/([^/]*)/cgi-bin/(.*) /home/groups/$1/cgi-bin/$2
	<Directory /home/groups>
		Options Indexes FollowSymlinks
		AllowOverride All
		order allow,deny
		allow from all
	</Directory>

	# Ensure that we don't try to use SSL on SSL Servers
	<IfModule apache_ssl.c>
		SSLDisable
	</IfModule>
</VirtualHost>

# HTTPS
<VirtualHost 10.GF.OR.GE:443>
	ServerName gforge.company.com
	ServerAlias www.gforge.company.com
	ServerAdmin webmaster@gforge.company.com
	DocumentRoot /path/to/gforge/www
	Alias /images/ /path/to/gforge/www/images/
	DirectoryIndex index.html index.php
	<IfModule mod_userdir.c>
		UserDir disabled
	</IfModule>

	php_admin_value default_charset "UTF-8"

	<Directory /path/to/gforge/www>
		Include   /etc/httpd.secrets
	</Directory>

	ScriptAliasMatch ^/plugins/([^/]*)/cgi-bin/(.*) /usr/lib/gforge/plugins/$1/cgi-bin/$2

	# Projects and Users script
	<Location /projects>
		ForceType application/x-httpd-php
	</Location>
	<Location /users>
		ForceType application/x-httpd-php
	</Location>

	# 404 Error document
	ErrorDocument 404 /404.php
	LogFormat "%h %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" gforge
	CustomLog "|/usr/bin/cronolog /var/log/gforge/%Y/%m/%d/gforge.log" gforge

	<IfModule mod_ssl.c>
		SSLEngine on
		SSLCertificateFile /etc/apache/ssl.crt/server.crt
		SSLCertificateKeyFile /etc/apache/ssl.key/server.key
		<Files ~ "\.(cgi|shtml)$">
			SSLOptions +StdEnvVars
		</Files>
		<Directory "/usr/lib/cgi-bin">
			SSLOptions +StdEnvVars
		</Directory>
		SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
	</IfModule>

	<IfModule apache_ssl.c>
		SSLEnable
		SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
	</IfModule>
</VirtualHost>

#
# Download host
#
<VirtualHost 10.GF.OR.GE:80>
  ServerName download.gforge.company.com
  DocumentRoot /var/lib/gforge/download
  LogFormat "%h %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" gforge
  CustomLog "|/usr/bin/cronolog /var/log/gforge/%Y/%m/%d/gforge.log" gforge
  # Ensure that we don't try to use SSL on SSL Servers
  <IfModule apache_ssl.c>
    SSLDisable
  </IfModule>
</VirtualHost>

#
# List host
#
# HTTP
<VirtualHost 10.GF.OR.GE:80>
  ServerName lists.gforge.company.com
  AddHandler cgi-script .cgi

  ScriptAlias /mailman/ /var/lib/mailman/cgi-bin/

  Alias /pipermail /var/lib/mailman/archives/public
  <Directory /var/lib/mailman/archives/public>
    AllowOverride Options
    Options FollowSymLinks
  </Directory>

  Alias /images/mailman /usr/share/images/mailman
  <Location /images/mailman>
    order allow,deny
    allow from all
  </Location>

  RedirectMatch permanent ^/$ http://lists.gforge.company.com/mailman/listinfo
  LogFormat "%h %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" gforge
  CustomLog "|/usr/bin/cronolog /var/log/gforge/%Y/%m/%d/gforge.log" gforge
  # Ensure that we don't try to use SSL on SSL Servers
  <IfModule apache_ssl.c>
    SSLDisable
  </IfModule>
</VirtualHost>

# HTTPS
<VirtualHost 10.GF.OR.GE:443>
  ServerName lists.gforge.company.com
  AddHandler cgi-script .cgi

  <IfModule mod_ssl.c>
    SSLEngine on
    SSLCertificateFile /etc/apache/ssl.crt/server.crt
    SSLCertificateKeyFile /etc/apache/ssl.key/server.key
    <Files ~ "\.(cgi|shtml)$">
      SSLOptions +StdEnvVars
    </Files>
    <Directory "/usr/lib/cgi-bin">
      SSLOptions +StdEnvVars
    </Directory>
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
  </IfModule>

  <IfModule apache_ssl.c>
    SSLEnable
  </IfModule>

  ScriptAlias /mailman/ /var/lib/mailman/cgi-bin/

  Alias /pipermail /var/lib/mailman/archives/public
  <Directory /var/lib/mailman/archives/public>
    AllowOverride Options
    Options FollowSymLinks
  </Directory>

  Alias /images/mailman /usr/share/images/mailman
  <Location /images/mailman>
    order allow,deny
    allow from all
  </Location>

  RedirectMatch permanent ^/$ https://lists.gforge.company.com/mailman/listinfo
  LogFormat "%h %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" gforge
  CustomLog "|/usr/bin/cronolog /var/log/gforge/%Y/%m/%d/gforge.log" gforge
</VirtualHost>

#
# * hosts
#
<VirtualHost 10.GF.OR.GE:80>
	ServerName gforge.company.com
	ServerAlias *.gforge.company.com
	VirtualDocumentRoot /home/groups/%1/htdocs
        VirtualScriptAlias /home/groups/%1/cgi-bin
	DirectoryIndex index.html index.php
	php_admin_value default_charset "UTF-8"
        <Directory /home/groups>
               Options Indexes FollowSymlinks
               AllowOverride All
               order allow,deny
               allow from all
       </Directory>
  LogFormat "%h %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" gforge
  CustomLog "|/usr/bin/cronolog /var/log/gforge/%Y/%m/%d/gforge.log" gforge
  # Ensure that we don't try to use SSL on SSL Servers
  <IfModule apache_ssl.c>
    SSLDisable
  </IfModule>
</VirtualHost>

