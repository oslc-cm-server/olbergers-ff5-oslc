<dl>
<!-- Supprimer l'instance -->
<dt>delete_instance</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "delete_instance"); ?></dd>
<!-- Modifier les param&eacute;tres -->
<dt>edit_parameters</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "edit_parameters"); ?></dd>
<!-- G&eacute;rer les cat&eacute;gories -->
<dt>manage_category</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "manage_category"); ?></dd>
<!-- Accès robots -->
<dt>ROLE_FORUM_BOT</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_BOT"); ?></dd>
<!-- Accès total -->
<dt>ROLE_FORUM_FULL</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_FULL"); ?></dd>
<!-- Accès limité -->
<dt>ROLE_FORUM_LIMITED</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_LIMITED"); ?></dd>
<!-- Accès limité + Sondages -->
<dt>ROLE_FORUM_LIMITED_POLLS</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_LIMITED_POLLS"); ?></dd>
<!-- Aucun accès -->
<dt>ROLE_FORUM_NOACCESS</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_NOACCESS"); ?></dd>
<!-- Attente de modération -->
<dt>ROLE_FORUM_ONQUEUE</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_ONQUEUE"); ?></dd>
<!-- Accès standard + Sondages -->
<dt>ROLE_FORUM_POLLS</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_POLLS"); ?></dd>
<!-- Accès en lecture uniquement -->
<dt>ROLE_FORUM_READONLY</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_READONLY"); ?></dd>
<!-- Accès standard -->
<dt>ROLE_FORUM_STANDARD</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "ROLE_FORUM_STANDARD"); ?></dd>
</dl>
