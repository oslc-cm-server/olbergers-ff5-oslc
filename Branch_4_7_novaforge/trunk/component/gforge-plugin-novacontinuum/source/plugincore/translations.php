<?php
/* Ensemble de traduction nécessaire à l'affichage des messages remontés par Continuum */
?>
<dl>
<!-- Salut le monde !!! -->
<dt>title_helloworld</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "title_helloworld"); ?></dd>
<!-- Ajouter l'instance -->
<dt>submit_add_instance</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_instance"); ?></dd>
<!-- Mettre &agrave; jour l'instance -->
<dt>submit_edit_instance</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_instance"); ?></dd>
<!-- Ajouter le proxy HTTP -->
<dt>submit_add_proxy</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_proxy"); ?></dd>
<!-- Mettre &agrave; jour le proxy HTTP -->
<dt>submit_edit_proxy</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_proxy"); ?></dd>
<!-- Mettre &agrave; jour la configuration -->
<dt>submit_update_configuration</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_update_configuration"); ?></dd>
<!-- Suppression de l'instance priv&eacute;e -->
<dt>delete_private_instance</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "delete_private_instance"); ?></dd>
<!-- Ajouter le projet -->
<dt>submit_add_project</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_project"); ?></dd>
<!-- Mettre &agrave; jour le projet -->
<dt>submit_edit_project</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_project"); ?></dd>
<!-- D&eacute;finitions des constructions -->
<dt>build_definitions</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "build_definitions"); ?></dd>
<!-- Projets -->
<dt>projects</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projects"); ?></dd>
<!-- d&eacute;faut -->
<dt>default_build_def</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "default_build_def"); ?></dd>
<!-- Ajouter la d&eacute;finition de construction -->
<dt>submit_add_build_def</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_build_def"); ?></dd>
<!-- Mettre &agrave; jour la d&eacute;finition de construction -->
<dt>submit_edit_build_def</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_build_def"); ?></dd>
<!-- Alertes -->
<dt>notifiers</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifiers"); ?></dd>
<!-- Ajouter la planification -->
<dt>submit_add_schedule</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_schedule"); ?></dd>
<!-- Mettre &agrave; jour la planification -->
<dt>submit_edit_schedule</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_schedule"); ?></dd>
<!-- Ajouter l&#146;alerte -->
<dt>submit_add_notifier</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_notifier"); ?></dd>
<!-- Mettre &agrave; jour l&#146;alerte -->
<dt>submit_edit_notifier</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_notifier"); ?></dd>
<!-- Mettre &agrave; jour l&#146;installation -->
<dt>submit_edit_installation</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_installation"); ?></dd>
<!-- Ajouter l&#146;installation -->
<dt>submit_add_installation</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_installation"); ?></dd>
<!-- Ajouter le profil -->
<dt>submit_add_profile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_add_profile"); ?></dd>
<!-- Mettre &agrave; jour le profil -->
<dt>submit_edit_profile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit_edit_profile"); ?></dd>
<!-- Tag Base : -->
<dt>form_release_project_tag_base</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "form_release_project_tag_base"); ?></dd>
<!-- Tag : -->
<dt>form_release_project_tag</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "form_release_project_tag"); ?></dd>
<!-- dd MMM yyyy HH:mm:ss z -->
<dt>webwork.date</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "webwork.date"); ?></dd>
<!-- Succ&egrave;s -->
<dt>message.success</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "message.success"); ?></dd>
<!-- Echec -->
<dt>message.failed</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "message.failed"); ?></dd>
<!-- Erreur -->
<dt>message.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "message.error"); ?></dd>
<!-- Activ&eacute; -->
<dt>enabled</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "enabled"); ?></dd>
<!-- D&eacute;sactiv&eacute; -->
<dt>disabled</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "disabled"); ?></dd>
<!-- Annuler -->
<dt>cancel</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "cancel"); ?></dd>
<!-- Soumettre -->
<dt>submit</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "submit"); ?></dd>
<!-- Editer -->
<dt>edit</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "edit"); ?></dd>
<!-- Supprimer -->
<dt>delete</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "delete"); ?></dd>
<!-- Construire -->
<dt>build</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "build"); ?></dd>
<!-- Faire une ''release'' -->
<dt>release</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "release"); ?></dd>
<!-- Sauvegarder -->
<dt>save</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "save"); ?></dd>
<!-- Ajouter -->
<dt>add</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add"); ?></dd>
<!-- Retour -->
<dt>back</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "back"); ?></dd>
<!-- Copie de travail -->
<dt>workingCopy</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "workingCopy"); ?></dd>
<!-- Constructions -->
<dt>builds</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "builds"); ?></dd>
<!-- Informations sur le projet -->
<dt>info</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "info"); ?></dd>
<!-- OU -->
<dt>or</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "or"); ?></dd>
<!-- S&eacute;lectionner tous -->
<dt>selectAll</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "selectAll"); ?></dd>
<!-- D&eacute;selectionner tous -->
<dt>unselectAll</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "unselectAll"); ?></dd>
<!-- Tri d&eacute;croissant -->
<dt>sort.descending</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "sort.descending"); ?></dd>
<!-- Tri croissant -->
<dt>sort.ascending</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "sort.ascending"); ?></dd>
<!-- [Le logo de votre entreprise] -->
<dt>top.logo.default</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "top.logo.default"); ?></dd>
<!-- ${pom.parent.parent.version} -->
<dt>bottom.version.number</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "bottom.version.number"); ?></dd>
<!-- A propos -->
<dt>menu.continuum.about</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.continuum.about"); ?></dd>
<!-- Afficher les projets -->
<dt>menu.continuum.showProjects</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.continuum.showProjects"); ?></dd>
<!-- Groupes de projets -->
<dt>menu.continuum.showProjectGroups</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.continuum.showProjectGroups"); ?></dd>
<!-- Ajouter un projet -->
<dt>menu.addProject</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.addProject"); ?></dd>
<!-- Projet Maven 2.0.X -->
<dt>menu.add.m2Project</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.add.m2Project"); ?></dd>
<!-- Projet Maven 1.x -->
<dt>menu.add.m1Project</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.add.m1Project"); ?></dd>
<!-- Projet Ant -->
<dt>menu.add.antProject</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.add.antProject"); ?></dd>
<!-- Projet Console -->
<dt>menu.add.shellProject</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.add.shellProject"); ?></dd>
<!-- Administration -->
<dt>menu.administration</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration"); ?></dd>
<!-- Configuration -->
<dt>menu.administration.configuration</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.configuration"); ?></dd>
<!-- Apparence -->
<dt>menu.administration.appearance</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.appearance"); ?></dd>
<!-- Environnement de Build -->
<dt>menu.administration.profile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.profile"); ?></dd>
<!-- Installations -->
<dt>menu.administration.installations</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.installations"); ?></dd>
<!-- Planifications -->
<dt>menu.administration.schedules</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.schedules"); ?></dd>
<!-- Utilisateurs -->
<dt>menu.administration.users</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.users"); ?></dd>
<!-- Groupes d&#146;utilisateurs -->
<dt>menu.administration.userGroups</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.userGroups"); ?></dd>
<!-- Mod&egrave;les de d&eacute;f. des contructions -->
<dt>menu.administration.buildDefinitionTemplates</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.buildDefinitionTemplates"); ?></dd>
<!-- Mon compte -->
<dt>menu.account.options</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.account.options"); ?></dd>
<!-- Editer les informations utilisateur -->
<dt>user.edit.account</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.edit.account"); ?></dd>
<!-- Files d&#146;attente -->
<dt>menu.administration.queues</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "menu.administration.queues"); ?></dd>
<!-- Page d&#146;erreur -->
<dt>error.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.page.title"); ?></dd>
<!-- Une erreur interne s&#146;est produite -->
<dt>error.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.section.title"); ?></dd>
<!-- Message de l&#146;exception :  -->
<dt>error.exception.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.exception.message"); ?></dd>
<!-- Le serveur n&#146;&eacute;tait pas en mesure d&#146;ex&eacute;cuter la demande.  Merci d&#146;essayer ult&eacute;rieurement. Contacter l&#146;administrateur si le probl&egrave;me persiste. -->
<dt>error.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.message"); ?></dd>
<!-- Erreur 403 -->
<dt>error.403.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.403.title"); ?></dd>
<!-- Ressource interdite -->
<dt>error.403.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.403.section.title"); ?></dd>
<!-- L&#146;URL demand&eacute; est une page interdite. -->
<dt>error.403.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.403.message"); ?></dd>
<!-- Erreur 404 -->
<dt>error.404.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.404.title"); ?></dd>
<!-- Ressource non trouv&eacute;e -->
<dt>error.404.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.404.section.title"); ?></dd>
<!-- L&#146;URL demand&eacute;e ne peut &ecirc;tre trouv&eacute;e. -->
<dt>error.404.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.404.message"); ?></dd>
<!-- Erreur 500 -->
<dt>error.500.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.500.title"); ?></dd>
<!-- Erreur interne -->
<dt>error.500.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.500.section.title"); ?></dd>
<!-- L&#146;URL demand&eacute;e a produit une erreur interne. -->
<dt>error.500.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "error.500.message"); ?></dd>
<!-- Continuum - A propos -->
<dt>about.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "about.page.title"); ?></dd>
<!-- A propos de Continuum -->
<dt>about.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "about.section.title"); ?></dd>
<!-- Version -->
<dt>about.version.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "about.version.label"); ?></dd>
<!-- ${project.version} -->
<dt>about.version.number</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "about.version.number"); ?></dd>
<!-- Build Number -->
<dt>about.buildnumber.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "about.buildnumber.label"); ?></dd>
<!-- ${buildNumber} -->
<dt>about.buildnumber</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "about.buildnumber"); ?></dd>
<!-- Continuum - Authentification -->
<dt>login.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "login.page.title"); ?></dd>
<!-- Authentification -->
<dt>login.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "login.section.title"); ?></dd>
<!-- Nom d&#146;utilisateur -->
<dt>login.username</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "login.username"); ?></dd>
<!-- Mot de passe -->
<dt>login.password</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "login.password"); ?></dd>
<!-- Se souvenir de moi -->
<dt>login.rememberMe</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "login.rememberMe"); ?></dd>
<!-- Connexion -->
<dt>login.submit</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "login.submit"); ?></dd>
<!-- Continuum - Bilan -->
<dt>summary.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.page.title"); ?></dd>
<!-- Projets Continuum -->
<dt>summary.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.section.title"); ?></dd>
<!-- Nom du projet -->
<dt>summary.projectTable.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.projectTable.name"); ?></dd>
<!-- Version -->
<dt>summary.projectTable.version</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.projectTable.version"); ?></dd>
<!-- Construire -->
<dt>summary.projectTable.build</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.projectTable.build"); ?></dd>
<!-- Groupe -->
<dt>summary.projectTable.group</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.projectTable.group"); ?></dd>
<!-- Date de la derni&egrave;re construction -->
<dt>summary.projectTable.lastBuildDateTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.projectTable.lastBuildDateTime"); ?></dd>
<!-- Tout contruire -->
<dt>summary.buildAll</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.buildAll"); ?></dd>
<!-- Historique des constructions -->
<dt>summary.buildHistory</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.buildHistory"); ?></dd>
<!-- Construire maintenant -->
<dt>summary.buildNow</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "summary.buildNow"); ?></dd>
<!-- Continuum - Bilan du Groupe -->
<dt>groups.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.page.title"); ?></dd>
<!-- Groupes de projets -->
<dt>groups.page.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.page.section.title"); ?></dd>
<!-- Nom -->
<dt>groups.table.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.table.name"); ?></dd>
<!-- Id&nbsp;du&nbsp;groupe -->
<dt>groups.table.groupId</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.table.groupId"); ?></dd>
<!-- Total -->
<dt>groups.table.totalProjects</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.table.totalProjects"); ?></dd>
<!-- Bilan -->
<dt>groups.table.summary</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.table.summary"); ?></dd>
<!-- La liste des groupes de projets est vide. -->
<dt>groups.page.list.empty</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.page.list.empty"); ?></dd>
<!-- Groupe de projet : -->
<dt>groups.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.section.title"); ?></dd>
<!-- G&eacute;rer le Groupe -->
<dt>groups.manage.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.manage.label"); ?></dd>
<!-- Etes vous s&ucirc;r de vouloir supprimer le groupe de projets \"%1$s\" ? -->
<dt>groups.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "groups.confirmation.message"); ?></dd>
<!-- Continuum - Groupe de projets -->
<dt>projectGroup.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.page.title"); ?></dd>
<!-- Information sur le groupe de projets -->
<dt>projectGroup.information.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.information.title"); ?></dd>
<!-- Nom du groupe de projets -->
<dt>projectGroup.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.name.label"); ?></dd>
<!-- Id du groupe de projets -->
<dt>projectGroup.groupId.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.groupId.label"); ?></dd>
<!-- Description -->
<dt>projectGroup.description.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.description.label"); ?></dd>
<!-- Membres du projet -->
<dt>projectGroup.projects.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.projects.title"); ?></dd>
<!-- Bilan du groupe de projets -->
<dt>projectGroup.tab.summary</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.tab.summary"); ?></dd>
<!-- Membres -->
<dt>projectGroup.tab.members</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.tab.members"); ?></dd>
<!-- D&eacute;finitions des constructions -->
<dt>projectGroup.tab.buildDefinitions</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.tab.buildDefinitions"); ?></dd>
<!-- Alertes -->
<dt>projectGroup.tab.notifiers</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.tab.notifiers"); ?></dd>
<!-- Actions sur le groupe -->
<dt>projectGroup.actions.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.actions.title"); ?></dd>
<!-- Ne peut pas faire la \"release\" d&#146;un groupe vide -->
<dt>projectGroup.release.error.emptyGroup</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.release.error.emptyGroup"); ?></dd>
<!-- Ne peut pas faire la \"release\" de deux projets parents ou plus qui sont dans le m&ecirc;me groupe de projets en m&ecirc;me temps. -->
<dt>projectGroup.release.error.severalParentProjects</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.release.error.severalParentProjects"); ?></dd>
<!-- Ne peut pas faire la \"release\" du groupe de projet : un ou plusieurs projets dans le groupe n&#146;ont pas pu &ecirc;tre construit avec succ&egrave;s. -->
<dt>projectGroup.release.error.projectNotInSuccess</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.release.error.projectNotInSuccess"); ?></dd>
<!-- Supprimer groupe -->
<dt>projectGroup.deleteGroup</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.deleteGroup"); ?></dd>
<!-- Construire tous les projets -->
<dt>projectGroup.buildGroup</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildGroup"); ?></dd>
<!-- Construire le groupe maintenant -->
<dt>projectGroup.buildGroupNow</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildGroupNow"); ?></dd>
<!-- Supprimer projet(s) -->
<dt>projectGroup.deleteProjects</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.deleteProjects"); ?></dd>
<!-- Construire projet(s) -->
<dt>projectGroup.buildProjects</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildProjects"); ?></dd>
<!-- Annuler construction(s) -->
<dt>projectGroup.cancelBuilds</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.cancelBuilds"); ?></dd>
<!-- Aper&#231;u du r&eacute;sultat de la derni&egrave;re construction du groupe de projets -->
<dt>projectGroup.buildsStatut.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildsStatut.title"); ?></dd>
<!-- Erreurs -->
<dt>projectGroup.buildsStatut.errors</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildsStatut.errors"); ?></dd>
<!-- Echec -->
<dt>projectGroup.buildsStatut.failures</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildsStatut.failures"); ?></dd>
<!-- Succ&egrave;s -->
<dt>projectGroup.buildsStatut.success</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildsStatut.success"); ?></dd>
<!-- D&eacute;finitions de la construction par d&eacute;fault -->
<dt>projectGroup.buildDefinition.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.buildDefinition.label"); ?></dd>
<!-- Ajouter un nouveau projet -->
<dt>projectGroup.addProject.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.addProject.label"); ?></dd>
<!-- Release du groupe -->
<dt>projectGroup.releaseNow</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.releaseNow"); ?></dd>
<!-- Projects membres du group %1$s -->
<dt>projectGroup.members.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.section.title"); ?></dd>
<!-- Utilisateurs -->
<dt>projectGroup.members.users.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.users.title"); ?></dd>
<!-- Recherche d&#146;utilisateur -->
<dt>projectGroup.members.users.search.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.users.search.label"); ?></dd>
<!-- Recherche -->
<dt>projectGroup.members.users.search.button</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.users.search.button"); ?></dd>
<!-- Administrateur -->
<dt>projectGroup.members.user.role.administrator</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.user.role.administrator"); ?></dd>
<!-- D&eacute;veloppeur -->
<dt>projectGroup.members.user.role.developer</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.user.role.developer"); ?></dd>
<!-- Utilisateur -->
<dt>projectGroup.members.user.role.user</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.members.user.role.user"); ?></dd>
<!-- Continuum - Ajouter un groupe de projets -->
<dt>projectGroup.add.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.add.page.title"); ?></dd>
<!-- Ajouter un groupe de projets -->
<dt>projectGroup.add.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.add.section.title"); ?></dd>
<!-- Le nom du groupe de projets est obligatoire. -->
<dt>projectGroup.error.name.required</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.error.name.required"); ?></dd>
<!-- Le nom du groupe de projets ne peut pas contenir uniquement des espaces. -->
<dt>projectGroup.error.name.cannot.be.spaces</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.error.name.cannot.be.spaces"); ?></dd>
<!-- Le nom du groupe de projets existe d&eacute;j&agrave;. -->
<dt>projectGroup.error.name.already.exists</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.error.name.already.exists"); ?></dd>
<!-- L&#146;Id du groupe de projets est obligatoire. -->
<dt>projectGroup.error.groupId.required</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.error.groupId.required"); ?></dd>
<!-- L&#146;Id du groupe de projets ne peut pas contenir uniquement des espaces. -->
<dt>projectGroup.error.groupId.cannot.be.spaces</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.error.groupId.cannot.be.spaces"); ?></dd>
<!-- L&#146;Id du groupe de projets existe d&eacute;j&agrave;. -->
<dt>projectGroup.error.groupId.already.exists</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.error.groupId.already.exists"); ?></dd>
<!-- Continuum - Mise &agrave; jour du groupe de projets -->
<dt>projectGroup.edit.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.edit.page.title"); ?></dd>
<!-- Mise &agrave; jour du groupe de projets -->
<dt>projectGroup.edit.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.edit.section.title"); ?></dd>
<!-- Projets -->
<dt>projectGroup.edit.section.projects.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.edit.section.projects.title"); ?></dd>
<!-- Nom du projet -->
<dt>projectGroup.edit.project.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.edit.project.name"); ?></dd>
<!-- D&eacute;placer vers un groupe -->
<dt>projectGroup.edit.move.to.group</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectGroup.edit.move.to.group"); ?></dd>
<!-- Ne peut pas &eacute;diter ce groupe de projets. Des projets qui sont des membres de ce groupe de projets sont en cours de r&eacute;cup&eacute;ration du gestionnaire de sources. -->
<dt>project.in.checkout.queue.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "project.in.checkout.queue.error"); ?></dd>
<!-- Continuum - Configuration -->
<dt>configuration.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.page.title"); ?></dd>
<!-- Configuration g&eacute;n&eacute;rale -->
<dt>configuration.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.section.title"); ?></dd>
<!-- Invit&eacute;s -->
<dt>configuration.guest.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.guest.label"); ?></dd>
<!-- L&#146;utilisateur invit&eacute; est <strong style=\"color: green;\">%1$s</strong> -->
<dt>configuration.guest.value</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.guest.value"); ?></dd>
<!-- R&eacute;pertoire de travail -->
<dt>configuration.workingDirectory.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.workingDirectory.label"); ?></dd>
<!-- R&eacute;pertoire des logs de construction -->
<dt>configuration.buildOutputDirectory.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.buildOutputDirectory.label"); ?></dd>
<!-- R&eacute;pertoire du repository de d&eacute;ploiement -->
<dt>configuration.deploymentRepositoryDirectory.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.deploymentRepositoryDirectory.label"); ?></dd>
<!-- URL de base -->
<dt>configuration.baseUrl.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.baseUrl.label"); ?></dd>
<!-- Editer -->
<dt>configuration.submit.edit</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.submit.edit"); ?></dd>
<!-- Activ&eacute;/D&eacute;sactiv&eacute; l&#146;utilisateur invit&eacute; -->
<dt>configuration.guest.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.guest.message"); ?></dd>
<!-- Entrer le r&eacute;pertoire de travail de l&#146;application web Continuum -->
<dt>configuration.workingDirectory.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.workingDirectory.message"); ?></dd>
<!-- Entrer le r&eacute;pertoire de sortie de l&#146;application web Continuum -->
<dt>configuration.buildOutputDirectory.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.buildOutputDirectory.message"); ?></dd>
<!-- Entrer le r&eacute;pertoire de d&eacute;ploiment de l&#146;application web Continuum -->
<dt>configuration.deploymentRepositoryDirectory.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.deploymentRepositoryDirectory.message"); ?></dd>
<!-- Entrer l&#146;URL de base de l&#146;application web Continuum -->
<dt>configuration.baseUrl.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.baseUrl.message"); ?></dd>
<!-- Entrer le nom de l&#146;entreprise -->
<dt>configuration.companyName.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.companyName.message"); ?></dd>
<!-- Entrer le logo de l&#146;entreprise -->
<dt>configuration.companyLogo.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.companyLogo.message"); ?></dd>
<!-- Entrer l&#146;URL de base du site officiel de la soci&eacute;t&eacute; -->
<dt>configuration.companyUrl.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "configuration.companyUrl.message"); ?></dd>
<!-- L&#146;URL du fichier POM ou l&#146;upload du fichier POM est obligatoire. -->
<dt>add.project.field.required.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.field.required.error"); ?></dd>
<!-- L&#146;h&ocirc;te sp&eacute;cifi&eacute; est inconnu ou inaccessible. -->
<dt>add.project.unknown.host.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.unknown.host.error"); ?></dd>
<!-- Impossible de contacter le serveur distant -->
<dt>add.project.connect.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.connect.error"); ?></dd>
<!-- L&#146;URL fournie est malform&eacute;e. -->
<dt>add.project.malformed.url.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.malformed.url.error"); ?></dd>
<!-- Le contenu XML du fichier POM ne peut &ecirc;tre pars&eacute;. -->
<dt>add.project.xml.parse.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.xml.parse.error"); ?></dd>
<!-- Impossible d&#146;utiliser un fichier POM avec un &eacute;l&eacute;ment \"extend\". -->
<dt>add.project.extend.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.extend.error"); ?></dd>
<!-- Le fichier POM n&#146;existe pas. Le fichier POM que vous avez sp&eacute;cifi&eacute; ou un de ses modules n&#146;existe pas. -->
<dt>add.project.missing.pom.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.pom.error"); ?></dd>
<!-- L&#146;&eacute;l&eacute;ment \"groupId\" est manquant dans le fichier POM. -->
<dt>add.project.missing.groupid.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.groupid.error"); ?></dd>
<!-- L&#146;&eacute;l&eacute;ment \"artifactId\" est manquant dans le fichier POM. -->
<dt>add.project.missing.artifactid.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.artifactid.error"); ?></dd>
<!-- L&#146;&eacute;l&eacute;ment \"version\" est manquant dans le fichier POM. -->
<dt>add.project.missing.version.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.version.error"); ?></dd>
<!-- L&#146;&eacute;l&eacute;ment \"name\" est manquant dans le fichier POM. -->
<dt>add.project.missing.name.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.name.error"); ?></dd>
<!-- L&#146;&eacute;l&eacute;ment \"repository\" est manquant dans le fichier POM. -->
<dt>add.project.missing.repository.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.repository.error"); ?></dd>
<!-- L&#146;&eacute;l&eacute;ment \"scm\" est manquant dans le fichier POM, projet  -->
<dt>add.project.missing.scm.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.scm.error"); ?></dd>
<!-- Le sous-&eacute;l&eacute;ment \"connection est manquant dans le fichier POM. -->
<dt>add.project.missing.scm.connection.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.scm.connection.error"); ?></dd>
<!-- Le sous-&eacute;l&eacute;ment \"type\" dans l&#146;&eacute;l&eacute;ment \"notifier\" est manquant dans le fichier POM. -->
<dt>add.project.missing.notifier.type.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.notifier.type.error"); ?></dd>
<!-- Le sous-&eacute;l&eacute;ment \"configuration\" dans l'&eacute;l&eacute;ment \"notifier\" est manquant dans le fichier POM. -->
<dt>add.project.missing.notifier.configuration.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.missing.notifier.configuration.error"); ?></dd>
<!-- Le transfert des meta-donnn&eacute;es a &eacute;chou&eacute;. -->
<dt>add.project.metadata.transfer.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.metadata.transfer.error"); ?></dd>
<!-- La ressource sp&eacute;cifi&eacute; n&#146;est pas un fichier ou le protocol utilis&eacute; n&#146;est pas authoris&eacute;. -->
<dt>add.project.validation.protocol.not_allowed</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.validation.protocol.not_allowed"); ?></dd>
<!-- Vous n&#146;&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; l&#146;URL demand&eacute;e. Merci de v&eacute;rifier que le nom d&#146;utilisateur et le mot de passe fournis sont corrects -->
<dt>add.project.unauthorized.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.unauthorized.error"); ?></dd>
<!-- Artefact manquant lors de la tentative de construction du pom. V&eacute;rifier que le pom parent est disponible ou ajouter-le en premier dans Continuum. -->
<dt>add.project.artifact.not.found.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.artifact.not.found.error"); ?></dd>
<!-- Erreur inconnue lors de la tentative de construction du pom. -->
<dt>add.project.project.building.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.project.building.error"); ?></dd>
<!-- La ressource sp&eacute;cifi&eacute;e ne peut &ecirc;tre contact&eacute;e. S&#146;il vous pla&icirc;t, r&eacute;essayer ult&eacute;rieurement ou contacter votre administrateur. -->
<dt>add.project.unknown.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.unknown.error"); ?></dd>
<!-- Aucun groupe de projet sp&eacute;cifi&eacute;. -->
<dt>add.project.nogroup.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.nogroup.error"); ?></dd>
<!-- Continuum - Ajouter un Projet Maven 1 -->
<dt>add.m1.project.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.page.title"); ?></dd>
<!-- Ajouter un Projet Maven 1.x -->
<dt>add.m1.project.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.section.title"); ?></dd>
<!-- Url du Pom M1 -->
<dt>add.m1.project.m1PomUrl.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomUrl.label"); ?></dd>
<!-- Nom d&#146;utilisateur -->
<dt>add.m1.project.m1PomUrl.username.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomUrl.username.label"); ?></dd>
<!-- Mot de passe -->
<dt>add.m1.project.m1PomUrl.password.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomUrl.password.label"); ?></dd>
<!-- Entrez l&#146;URL du pom Maven 1. Fournissez le nom d&#146;utilisateur et le mot de passe si c&#146;est une ressource s&eacute;curis&eacute;e. -->
<dt>add.m1.project.m1PomUrl.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomUrl.message"); ?></dd>
<!-- Vous devez entrer une URL valide -->
<dt>add.m1.project.m1PomUrl.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomUrl.error"); ?></dd>
<!-- Uploader un fichier POM -->
<dt>add.m1.project.m1PomFile.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomFile.label"); ?></dd>
<!-- Entrer le nom local du fichier pom(version Maven 1) &agrave; uploader -->
<dt>add.m1.project.m1PomFile.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomFile.message"); ?></dd>
<!-- Vous devez entrer une URL valide -->
<dt>add.m1.project.m1PomFile.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.m1PomFile.error"); ?></dd>
<!-- Groupe du projet -->
<dt>add.m1.project.projectGroup</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.projectGroup"); ?></dd>
<!-- Mod&egrave;le de d&eacute;finition de la contruction -->
<dt>add.m1.project.buildDefinitionTemplate</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.buildDefinitionTemplate"); ?></dd>
<!-- D&eacute;faut -->
<dt>add.m1.project.defaultBuildDefinition</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m1.project.defaultBuildDefinition"); ?></dd>
<!-- Continuum - Ajouter un Projet Maven 2 -->
<dt>add.m2.project.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.page.title"); ?></dd>
<!-- Ajouter un Projet Maven 2.0+ -->
<dt>add.m2.project.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.section.title"); ?></dd>
<!-- URL du POM -->
<dt>add.m2.project.m2PomUrl.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomUrl.label"); ?></dd>
<!-- Nom d&#146;utilisateur -->
<dt>add.m2.project.m2PomUrl.username.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomUrl.username.label"); ?></dd>
<!-- Mot de passe -->
<dt>add.m2.project.m2PomUrl.password.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomUrl.password.label"); ?></dd>
<!-- Entrez l&#146;URL du pom Maven 2. Fournissez le nom d&#146;utilisateur et le mot de passe si c&#146;est une ressource s&eacute;curis&eacute;e. -->
<dt>add.m2.project.m2PomUrl.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomUrl.message"); ?></dd>
<!-- Vous devez entrer une URL valide -->
<dt>add.m2.project.m2PomUrl.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomUrl.error"); ?></dd>
<!-- Uploader un fichier POM -->
<dt>add.m2.project.m2PomFile.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomFile.label"); ?></dd>
<!-- Entrer le nom local du fichier pom(version Maven 2) &agrave; uploader (fonctionne uniquement avec un projet sans modules). -->
<dt>add.m2.project.m2PomFile.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomFile.message"); ?></dd>
<!-- Vous devez entrer une URL valide -->
<dt>add.m2.project.m2PomFile.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.m2PomFile.error"); ?></dd>
<!-- Groupe du projet -->
<dt>add.m2.project.projectGroup</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.projectGroup"); ?></dd>
<!-- Ne peut pas uploader un projet Maven 2 avec modules. -->
<dt>add.m2.project.upload.modules.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.upload.modules.error"); ?></dd>
<!-- Pour les projets multi modules, charger uniquement la racine comme une construction r&eacute;cursive -->
<dt>add.m2.project.nonRecursiveProject</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.nonRecursiveProject"); ?></dd>
<!-- Mod&egrave;le de d&eacute;finition de la contruction -->
<dt>add.m2.project.buildDefinitionTemplate</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.buildDefinitionTemplate"); ?></dd>
<!-- D&eacute;faut -->
<dt>add.m2.project.defaultBuildDefinition</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.m2.project.defaultBuildDefinition"); ?></dd>
<!-- Continuum - Ajouter un projet console -->
<dt>add.shell.project.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.shell.project.page.title"); ?></dd>
<!-- Continuum - Ajouter un projet console -->
<dt>add.shell.project.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.shell.project.section.title"); ?></dd>
<!-- Continuum - Ajouter un projet Ant -->
<dt>add.ant.project.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.ant.project.page.title"); ?></dd>
<!-- Ajouter un projet Ant -->
<dt>add.ant.project.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.ant.project.section.title"); ?></dd>
<!-- Nom du projet -->
<dt>projectName.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectName.label"); ?></dd>
<!-- Vous devez fournir un nom de projet -->
<dt>projectName.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectName.error"); ?></dd>
<!-- Le nom du projet existe d&eacute;j&agrave; -->
<dt>projectName.already.exist.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectName.already.exist.error"); ?></dd>
<!-- Entrer le nom du projet -->
<dt>projectName.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectName.message"); ?></dd>
<!-- SCM -->
<dt>projectScm.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScm.label"); ?></dd>
<!-- Vous devez selectionner un gestionnaire de sources (SCM) -->
<dt>projectScm.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScm.error"); ?></dd>
<!-- Choisir un gestionnaire de sources (SCM) -->
<dt>projectScm.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScm.message"); ?></dd>
<!-- URL du gestionnaire de sources -->
<dt>projectScmUrl.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmUrl.label"); ?></dd>
<!-- Vous devez fournir une URL de gestionnaire de sources (SCM) -->
<dt>projectScmUrl.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmUrl.error"); ?></dd>
<!-- Entrer <a href=\"http://maven.apache.org/scm/scm-url-format.html\">URL de gestionnaire de sources au format de Maven</a> -->
<dt>projectScmUrl.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmUrl.message"); ?></dd>
<!-- Nom d&#146;utilisateur du gestionnaire de sources -->
<dt>projectScmUsername.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmUsername.label"); ?></dd>
<!-- Entrer le nom d&#146;utilisateur du gestionnaire de sources  -->
<dt>projectScmUsername.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmUsername.message"); ?></dd>
<!-- Mot de passe du gestionnaire de sources -->
<dt>projectScmPassword.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmPassword.label"); ?></dd>
<!-- Entrer le mot de passe du gestionnaire de sources  -->
<dt>projectScmPassword.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmPassword.message"); ?></dd>
<!-- Branche/Tag du gestionnaire de sources -->
<dt>projectScmTag.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmTag.label"); ?></dd>
<!-- Entrer la/le branche/tag du gestionnaire de sources (Pour subversion, le nom du tag doit &ecirc;tre dans le format du gestionnaire version et non pas dans ce champ) -->
<dt>projectScmTag.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmTag.message"); ?></dd>
<!-- Version -->
<dt>projectVersion.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectVersion.label"); ?></dd>
<!-- Vous devez fournir une version -->
<dt>projectVersion.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectVersion.error"); ?></dd>
<!-- Entrer la version du projet -->
<dt>projectVersion.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectVersion.message"); ?></dd>
<!-- Utiliser le cache d&#146;authentification du gestionnaire de sources -->
<dt>projectScmUseCache.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectScmUseCache.label"); ?></dd>
<!-- Mod&egrave;le de d&eacute;finition de la contruction -->
<dt>add.project.buildDefinitionTemplate</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.buildDefinitionTemplate"); ?></dd>
<!-- D&eacute;faut -->
<dt>add.project.defaultBuildDefinition</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "add.project.defaultBuildDefinition"); ?></dd>
<!-- Continuum - Supprimer un projet Continuum -->
<dt>deleteProject.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteProject.page.title"); ?></dd>
<!-- Supprimer un projet Continuum -->
<dt>deleteProject.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteProject.section.title"); ?></dd>
<!-- Etes vous s&ucirc;r de vouloir supprimer le projet \"%1$s\" ? -->
<dt>deleteProject.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteProject.confirmation.message"); ?></dd>
<!-- Continuum - Projet Continuum -->
<dt>projectView.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.page.title"); ?></dd>
<!-- Projet Continuum -->
<dt>projectView.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.section.title"); ?></dd>
<!-- Nom du projet -->
<dt>projectView.project.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.project.name"); ?></dd>
<!-- Version -->
<dt>projectView.project.version</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.project.version"); ?></dd>
<!-- URL du gestionnaire de sources -->
<dt>projectView.project.scmUrl</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.project.scmUrl"); ?></dd>
<!-- Branche/Tag du gestionnaire de sources -->
<dt>projectView.project.scmTag</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.project.scmTag"); ?></dd>
<!-- Groupe -->
<dt>projectView.project.group</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.project.group"); ?></dd>
<!-- Date de la derni&egrave;re construction -->
<dt>projectView.project.lastBuildDateTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.project.lastBuildDateTime"); ?></dd>
<!-- D&eacute;finitions des constructions -->
<dt>projectView.buildDefinitions</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinitions"); ?></dd>
<!-- Goals -->
<dt>projectView.buildDefinition.goals</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.goals"); ?></dd>
<!-- Arguments -->
<dt>projectView.buildDefinition.arguments</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.arguments"); ?></dd>
<!-- Fichier de construction -->
<dt>projectView.buildDefinition.buildFile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.buildFile"); ?></dd>
<!-- Environnement de Build -->
<dt>projectView.buildDefinition.profile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.profile"); ?></dd>
<!-- Planification -->
<dt>projectView.buildDefinition.schedule</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.schedule"); ?></dd>
<!-- Type -->
<dt>projectView.buildDefinition.type</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.type"); ?></dd>
<!-- Toujours Construire -->
<dt>projectView.buildDefinition.alwaysBuild</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.alwaysBuild"); ?></dd>
<!-- Environnement de Build -->
<dt>buildDefinition.profile.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.profile.label"); ?></dd>
<!-- De -->
<dt>projectView.buildDefinition.from</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.from"); ?></dd>
<!-- Par d&eacute;faut -->
<dt>projectView.buildDefinition.default</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.default"); ?></dd>
<!-- Extraction compl&egrave;te -->
<dt>projectView.buildDefinition.buildFresh</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.buildFresh"); ?></dd>
<!-- Description -->
<dt>projectView.buildDefinition.description</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.buildDefinition.description"); ?></dd>
<!-- Alertes -->
<dt>projectView.notifiers</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.notifiers"); ?></dd>
<!-- Type -->
<dt>projectView.notifier.type</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.notifier.type"); ?></dd>
<!-- Destinataire -->
<dt>projectView.notifier.recipient</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.notifier.recipient"); ?></dd>
<!-- &Eacute;v&eacute;nements -->
<dt>projectView.notifier.events</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.notifier.events"); ?></dd>
<!-- De -->
<dt>projectView.notifier.from</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.notifier.from"); ?></dd>
<!-- D&eacute;pendances -->
<dt>projectView.dependencies</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.dependencies"); ?></dd>
<!-- Nom -->
<dt>projectView.dependency.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.dependency.name"); ?></dd>
<!-- Id du groupe -->
<dt>projectView.dependency.groupId</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.dependency.groupId"); ?></dd>
<!-- Id de l'artefact -->
<dt>projectView.dependency.artifactId</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.dependency.artifactId"); ?></dd>
<!-- Version -->
<dt>projectView.dependency.version</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.dependency.version"); ?></dd>
<!-- Utilis&eacute; par -->
<dt>projectView.usedBy</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.usedBy"); ?></dd>
<!-- D&eacute;veloppeurs -->
<dt>projectView.developers</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.developers"); ?></dd>
<!-- Nom -->
<dt>projectView.developer.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.developer.name"); ?></dd>
<!-- Email -->
<dt>projectView.developer.email</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.developer.email"); ?></dd>
<!-- Nom d&#146;utilisateur -->
<dt>projectView.username</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.username"); ?></dd>
<!-- Affichage -->
<dt>projectView.role.view</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.role.view"); ?></dd>
<!-- Editer -->
<dt>projectView.role.edit</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.role.edit"); ?></dd>
<!-- Supprimer -->
<dt>projectView.role.delete</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.role.delete"); ?></dd>
<!-- Construire -->
<dt>projectView.role.build</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.role.build"); ?></dd>
<!-- Administrer -->
<dt>projectView.role.administer</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectView.role.administer"); ?></dd>
<!-- Continuum - Mettre &agrave; jour un projet Continuum -->
<dt>projectEdit.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.page.title"); ?></dd>
<!-- Mettre &agrave; jour un projet Continuum -->
<dt>projectEdit.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.section.title"); ?></dd>
<!-- Nom du projet -->
<dt>projectEdit.project.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.name.label"); ?></dd>
<!-- Version -->
<dt>projectEdit.project.version.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.version.label"); ?></dd>
<!-- URL du gestionnaire de sources -->
<dt>projectEdit.project.scmUrl.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.scmUrl.label"); ?></dd>
<!-- Nom d&#146;utilisateur du gestionnaire de sources -->
<dt>projectEdit.project.scmUsername.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.scmUsername.label"); ?></dd>
<!-- Mot de passe du gestionnaire de sources -->
<dt>projectEdit.project.scmPassword.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.scmPassword.label"); ?></dd>
<!-- Utiliser le cache d&#146;authentification du gestionnaire de sources -->
<dt>projectEdit.project.scmUseCache.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.scmUseCache.label"); ?></dd>
<!-- Branche/Tag du gestionnaire de sources -->
<dt>projectEdit.project.scmTag.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "projectEdit.project.scmTag.label"); ?></dd>
<!-- Continuum - Ajouter/Editer une d&eacute;finition de construction -->
<dt>buildDefinition.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.page.title"); ?></dd>
<!-- Ajouter/Editer une d&eacute;finition de construction -->
<dt>buildDefinition.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.section.title"); ?></dd>
<!-- Scripts de g&eacute;n&eacute;ration Ant -->
<dt>buildDefinition.buildFile.ant.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.buildFile.ant.label"); ?></dd>
<!-- Excutable console -->
<dt>buildDefinition.buildFile.shell.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.buildFile.shell.label"); ?></dd>
<!-- Nom du fichier POM -->
<dt>buildDefinition.buildFile.maven.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.buildFile.maven.label"); ?></dd>
<!-- Cibles -->
<dt>buildDefinition.goals.ant.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.goals.ant.label"); ?></dd>
<!-- Goals -->
<dt>buildDefinition.goals.maven.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.goals.maven.label"); ?></dd>
<!-- Arguments -->
<dt>buildDefinition.arguments.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.arguments.label"); ?></dd>
<!-- Extraction compl&egrave;te (Ex&eacute;cuter toujours une extraction &agrave; la place d&#146;une mise &agrave; jour) -->
<dt>buildDefinition.buildFresh.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.buildFresh.label"); ?></dd>
<!-- Type -->
<dt>buildDefinition.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.type.label"); ?></dd>
<!-- Est-ce par d&eacute;faut? -->
<dt>buildDefinition.defaultForProject.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.defaultForProject.label"); ?></dd>
<!-- Planification -->
<dt>buildDefinition.schedule.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.schedule.label"); ?></dd>
<!-- Description -->
<dt>buildDefinition.description.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.description.label"); ?></dd>
<!-- Toujours construire -->
<dt>buildDefinition.alwaysBuild.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.alwaysBuild.label"); ?></dd>
<!-- Il y a eu une erreur lors de l&#146;enregistrement de la planification, aucun nom de programme fourni. -->
<dt>buildDefinition.noname.save.error.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.noname.save.error.message"); ?></dd>
<!-- Il y a eu une erreur lors de l&#146;enregistrement de la planification, un programme avec le m&ecirc;me nom existe d&eacute;j&agrave;. -->
<dt>buildDefinition.duplicatename.save.error.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.duplicatename.save.error.message"); ?></dd>
<!-- D&eacute;finition de construction du groupe de projets %1$s -->
<dt>buildDefinitionSummary.projectGroup.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinitionSummary.projectGroup.section.title"); ?></dd>
<!-- Continuum - Supprimer une d&eacute;finition de construction -->
<dt>deleteBuildDefinition.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteBuildDefinition.page.title"); ?></dd>
<!-- Supprimer une d&eacute;finition de construction -->
<dt>deleteBuildDefinition.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteBuildDefinition.section.title"); ?></dd>
<!-- Etes-vous s&ucirc;r de vouloir supprimer la d&eacute;finition de contruction avec la description \"%2$s\", de goals \"%3$s\" et d&#146;id \"%1$s\" ? -->
<dt>deleteBuildDefinition.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteBuildDefinition.confirmation.message"); ?></dd>
<!-- Continuum - Supprimer une alerte -->
<dt>deleteNotifier.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteNotifier.page.title"); ?></dd>
<!-- Supprimer une alerte -->
<dt>deleteNotifier.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteNotifier.section.title"); ?></dd>
<!-- Etes vous s&ucirc;r de vouloir supprimer l'alerte %1$s avec le destinataire \"%2$s\"? -->
<dt>deleteNotifier.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteNotifier.confirmation.message"); ?></dd>
<!-- Continuum - Ajouter une alerte -->
<dt>notifier.page.add.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.page.add.title"); ?></dd>
<!-- Ajouter une alerte -->
<dt>notifier.section.add.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.section.add.title"); ?></dd>
<!-- Continuum - Ajouter/Editer l'alerte %1$s -->
<dt>notifier.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.page.title"); ?></dd>
<!-- Ajouter/Editer l'alerte %1$s -->
<dt>notifier.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.section.title"); ?></dd>
<!-- Type -->
<dt>notifier.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.type.label"); ?></dd>
<!-- Adresse email du destinataire -->
<dt>notifier.mail.recipient.address.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.mail.recipient.address.label"); ?></dd>
<!-- Envoyer un email aux derniers contributeurs -->
<dt>notifier.mail.recipient.committers.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.mail.recipient.committers.label"); ?></dd>
<!-- H&ocirc;te IRC -->
<dt>notifier.irc.host.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.host.label"); ?></dd>
<!-- Port IRC -->
<dt>notifier.irc.port.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.port.label"); ?></dd>
<!-- Canal IRC -->
<dt>notifier.irc.channel.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.channel.label"); ?></dd>
<!-- Pseudonyme (la valeur par d&eacute;faut est 'continuum') -->
<dt>notifier.irc.nick.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.nick.label"); ?></dd>
<!-- Pseudonyme alternatif (la valeur par d&eacute;faut est 'continuum_') -->
<dt>notifier.irc.alternateNick.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.alternateNick.label"); ?></dd>
<!-- Nom d&#146;utilisateur (la valeur par d&eacute;faut est le pseudonyme) -->
<dt>notifier.irc.username.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.username.label"); ?></dd>
<!-- Nom complet (la valeur par d&eacute;faut est le pseudonyme) -->
<dt>notifier.irc.fullName.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.fullName.label"); ?></dd>
<!-- Mot de passe -->
<dt>notifier.irc.password.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.password.label"); ?></dd>
<!-- SSL -->
<dt>notifier.irc.isSSL.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.irc.isSSL.label"); ?></dd>
<!-- H&ocirc;te Jabber -->
<dt>notifier.jabber.host.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.host.label"); ?></dd>
<!-- Port Jabber -->
<dt>notifier.jabber.port.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.port.label"); ?></dd>
<!-- Nom d&#146;utilisateur Jabber -->
<dt>notifier.jabber.login.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.login.label"); ?></dd>
<!-- Mot de passe Jabber -->
<dt>notifier.jabber.password.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.password.label"); ?></dd>
<!-- Nom de domaine Jabber -->
<dt>notifier.jabber.domainName.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.domainName.label"); ?></dd>
<!-- Addresse de destination Jabber -->
<dt>notifier.jabber.address.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.address.label"); ?></dd>
<!-- Est-ce une connection SSL? -->
<dt>notifier.jabber.isSslConnection.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.isSslConnection.label"); ?></dd>
<!-- Est-ce un groupe Jabber? -->
<dt>notifier.jabber.isGroup.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.jabber.isGroup.label"); ?></dd>
<!-- Nom d&#146;utilisateur MSN -->
<dt>notifier.msn.login.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.msn.login.label"); ?></dd>
<!-- Mot de passe MSN -->
<dt>notifier.msn.password.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.msn.password.label"); ?></dd>
<!-- Adresse MSN de destination -->
<dt>notifier.msn.address.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.msn.address.label"); ?></dd>
<!-- Url du site du projet -->
<dt>notifier.wagon.url.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.wagon.url.label"); ?></dd>
<!-- Id du serveur (d&eacute;finis dans votre fichier settings.xml pour authentification) -->
<dt>notifier.wagon.id.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.wagon.id.label"); ?></dd>
<!-- Envoyer en cas de succ&egrave;s -->
<dt>notifier.event.sendOnSuccess</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.event.sendOnSuccess"); ?></dd>
<!-- Envoyer en cas d&#146;&eacute;chec -->
<dt>notifier.event.sendOnFailure</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.event.sendOnFailure"); ?></dd>
<!-- Envoyer en cas d&#146;erreur  -->
<dt>notifier.event.sendOnError</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.event.sendOnError"); ?></dd>
<!-- Envoyer en cas d&#146;avertissement  -->
<dt>notifier.event.sendOnWarning</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "notifier.event.sendOnWarning"); ?></dd>
<!-- Continuum - r&eacute;sultats de constructions -->
<dt>buildResults.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.page.title"); ?></dd>
<!-- R&eacute;sultats de constructions pour %1$s -->
<dt>buildResults.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.section.title"); ?></dd>
<!-- Construction # -->
<dt>buildResults.buildNumber</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.buildNumber"); ?></dd>
<!-- Date de d&eacute;but -->
<dt>buildResults.startTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.startTime"); ?></dd>
<!-- Date de fin -->
<dt>buildResults.endTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.endTime"); ?></dd>
<!-- Dur&eacute;e -->
<dt>buildResults.duration</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.duration"); ?></dd>
<!-- D&eacute;marr&eacute; depuis -->
<dt>buildResults.startedSince</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.startedSince"); ?></dd>
<!-- &Eacute;tat -->
<dt>buildResults.state</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.state"); ?></dd>
<!-- Description de la d&eacute;finition de construction -->
<dt>buildResults.buildDefinition.description</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResults.buildDefinition.description"); ?></dd>
<!-- Continuum - R&eacute;sultat de construction -->
<dt>buildResult.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.page.title"); ?></dd>
<!-- R&eacute;sultat de la construction pour %1$s -->
<dt>buildResult.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.section.title"); ?></dd>
<!-- Date de d&eacute;but -->
<dt>buildResult.startTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.startTime"); ?></dd>
<!-- Date de fin -->
<dt>buildResult.endTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.endTime"); ?></dd>
<!-- Dur&eacute;e -->
<dt>buildResult.duration</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.duration"); ?></dd>
<!-- D&eacute;marr&eacute; depuis -->
<dt>buildResult.startedSince</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.startedSince"); ?></dd>
<!-- D&eacute;clencheur de la construction -->
<dt>buildResult.trigger</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.trigger"); ?></dd>
<!-- &Eacute;tat -->
<dt>buildResult.state</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state"); ?></dd>
<!-- Construction # -->
<dt>buildResult.buildNumber</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildNumber"); ?></dd>
<!-- Erreur de construction -->
<dt>buildResult.buildError</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildError"); ?></dd>
<!-- Sortie -->
<dt>buildResult.buildOutput</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildOutput"); ?></dd>
<!-- Aucune Sortie. -->
<dt>buildResult.noOutput</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.noOutput"); ?></dd>
<!-- Modifications -->
<dt>buildResult.changes</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.changes"); ?></dd>
<!-- Aucune modification -->
<dt>buildResult.noChanges</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.noChanges"); ?></dd>
<!-- Auteur -->
<dt>buildResult.changes.author</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.changes.author"); ?></dd>
<!-- Date -->
<dt>buildResult.changes.date</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.changes.date"); ?></dd>
<!-- Commentaire -->
<dt>buildResult.changes.comment</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.changes.comment"); ?></dd>
<!-- Fichiers -->
<dt>buildResult.changes.files</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.changes.files"); ?></dd>
<!-- Modifications du gestionnaire de sources -->
<dt>buildResult.scmResult.changes</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.scmResult.changes"); ?></dd>
<!-- Aucune modification du gestionnaire de sources -->
<dt>buildResult.scmResult.noChanges</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.scmResult.noChanges"); ?></dd>
<!-- Auteur -->
<dt>buildResult.scmResult.changes.author</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.scmResult.changes.author"); ?></dd>
<!-- Date -->
<dt>buildResult.scmResult.changes.date</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.scmResult.changes.date"); ?></dd>
<!-- Commentaire -->
<dt>buildResult.scmResult.changes.comment</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.scmResult.changes.comment"); ?></dd>
<!-- Fichiers -->
<dt>buildResult.scmResult.changes.files</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.scmResult.changes.files"); ?></dd>
<!-- Modifications des d&eacute;pendances -->
<dt>buildResult.dependencies.changes</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.dependencies.changes"); ?></dd>
<!-- Aucune modification dans les d&eacute;pendances -->
<dt>buildResult.dependencies.noChanges</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.dependencies.noChanges"); ?></dd>
<!-- Id du groupe -->
<dt>buildResult.dependencies.groupId</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.dependencies.groupId"); ?></dd>
<!-- Id de l&#146;artefact -->
<dt>buildResult.dependencies.artifactId</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.dependencies.artifactId"); ?></dd>
<!-- Version -->
<dt>buildResult.dependencies.version</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.dependencies.version"); ?></dd>
<!-- Autres modifications depuis le derni&egrave;r succ&egrave;s -->
<dt>buildResult.changesSinceLastSuccess</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.changesSinceLastSuccess"); ?></dd>
<!-- Rapports g&eacute;n&eacute;r&eacute;s -->
<dt>buildResult.generatedReports.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.generatedReports.title"); ?></dd>
<!-- Rapport Surefire -->
<dt>buildResult.generatedReports.surefire</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.generatedReports.surefire"); ?></dd>
<!-- T&eacute;l&eacute;charger au format texte -->
<dt>buildResult.buildOutput.text</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildOutput.text"); ?></dd>
<!-- D&eacute;finition de construction utilis&eacute;e -->
<dt>buildResult.buildDefinition</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition"); ?></dd>
<!-- Scripts de g&eacute;n&eacute;ration Ant -->
<dt>buildResult.buildDefinition.ant.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.ant.label"); ?></dd>
<!-- Excutable console -->
<dt>buildResult.buildDefinition.shell.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.shell.label"); ?></dd>
<!-- Nom du fichier POM -->
<dt>buildResult.buildDefinition.maven.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.maven.label"); ?></dd>
<!-- Goals -->
<dt>buildResult.buildDefinition.goals</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.goals"); ?></dd>
<!-- Arguments -->
<dt>buildResult.buildDefinition.arguments</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.arguments"); ?></dd>
<!-- Extraction compl&egrave;te -->
<dt>buildResult.buildDefinition.buildFresh</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.buildFresh"); ?></dd>
<!-- Toujours construire -->
<dt>buildResult.buildDefinition.alwaysBuild</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.alwaysBuild"); ?></dd>
<!-- Est-ce par d&eacute;faut ? -->
<dt>buildResult.buildDefinition.defaultForProject</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.defaultForProject"); ?></dd>
<!-- Planification -->
<dt>buildResult.buildDefinition.schedule</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.schedule"); ?></dd>
<!-- Environnement de Build -->
<dt>buildResult.buildDefinition.profileName</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.profileName"); ?></dd>
<!-- Description -->
<dt>buildResult.buildDefinition.description</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.buildDefinition.description"); ?></dd>
<!-- Planifi&eacute;e -->
<dt>buildResult.trigger.0</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.trigger.0"); ?></dd>
<!-- Forc&eacute;e -->
<dt>buildResult.trigger.1</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.trigger.1"); ?></dd>
<!-- Nouveau -->
<dt>buildResult.state.1</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.1"); ?></dd>
<!-- OK -->
<dt>buildResult.state.2</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.2"); ?></dd>
<!-- Echec -->
<dt>buildResult.state.3</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.3"); ?></dd>
<!-- Erreur -->
<dt>buildResult.state.4</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.4"); ?></dd>
<!-- Construction en cours -->
<dt>buildResult.state.6</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.6"); ?></dd>
<!-- Extraction en cours -->
<dt>buildResult.state.7</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.7"); ?></dd>
<!-- Mise &agrave; jour en cours -->
<dt>buildResult.state.8</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.8"); ?></dd>
<!-- Avertissement -->
<dt>buildResult.state.9</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.9"); ?></dd>
<!-- Extraction termin&eacute;e -->
<dt>buildResult.state.10</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.state.10"); ?></dd>
<!-- Continuum - Copie de travail -->
<dt>workingCopy.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "workingCopy.page.title"); ?></dd>
<!-- Copie de travail de %1$s -->
<dt>workingCopy.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "workingCopy.section.title"); ?></dd>
<!-- T&eacute;l&eacute;charger au format texte -->
<dt>workingCopy.currentFile.text</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "workingCopy.currentFile.text"); ?></dd>
<!-- Continuum - Planifications -->
<dt>schedules.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.page.title"); ?></dd>
<!-- Planifications -->
<dt>schedules.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.section.title"); ?></dd>
<!-- Nom -->
<dt>schedules.table.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.table.name"); ?></dd>
<!-- Description -->
<dt>schedules.table.description</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.table.description"); ?></dd>
<!-- P&eacute;riode d'attente -->
<dt>schedules.table.delay</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.table.delay"); ?></dd>
<!-- Expression cron -->
<dt>schedules.table.cronExpression</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.table.cronExpression"); ?></dd>
<!-- Activer -->
<dt>schedules.table.active</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.table.active"); ?></dd>
<!-- Temp d'ex&eacute;cution max. -->
<dt>schedules.table.maxJobExecutionTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.table.maxJobExecutionTime"); ?></dd>
<!-- Etes vous s&ucirc;r de vouloir supprimer la planification \"%1$s\" ? -->
<dt>schedules.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedules.confirmation.message"); ?></dd>
<!-- Continuum - Ajouter une planification -->
<dt>addSchedule.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "addSchedule.page.title"); ?></dd>
<!-- Ajouter une planification -->
<dt>addSchedule.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "addSchedule.section.title"); ?></dd>
<!-- Continuum - Editer une planification -->
<dt>editSchedule.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "editSchedule.page.title"); ?></dd>
<!-- Editer une planification -->
<dt>editSchedule.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "editSchedule.section.title"); ?></dd>
<!-- Nom -->
<dt>schedule.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.name.label"); ?></dd>
<!-- Entrer le nom de la planification -->
<dt>schedule.name.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.name.message"); ?></dd>
<!-- Description -->
<dt>schedule.description.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.description.label"); ?></dd>
<!-- Entrer une description pour la planification -->
<dt>schedule.description.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.description.message"); ?></dd>
<!-- Expression Cron -->
<dt>schedule.cronExpression.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.cronExpression.label"); ?></dd>
<!-- Entrer l&#146;expression Cron Le format est d&eacute;crit l&agrave; : <a href=\"http://www.opensymphony.com/quartz/api/org/quartz/CronTrigger.html\" target=\"_blank\">Syntaxe<a> -->
<dt>schedule.cronExpression.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.cronExpression.message"); ?></dd>
<!-- Temp d&#146;ex&eacute;cution du travail maximum -->
<dt>schedule.maxJobExecutionTime.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.maxJobExecutionTime.label"); ?></dd>
<!-- Entrer le nombre maximal de secondes que le travail peut prendre pour s&#146;executer dans cette planification avant d&#146;&ecirc;tre tu&eacute;. -->
<dt>schedule.maxJobExecutionTime.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.maxJobExecutionTime.message"); ?></dd>
<!-- P&eacute;riode d&#146;attente (secondes) -->
<dt>schedule.quietPeriod.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.quietPeriod.label"); ?></dd>
<!-- Enter une p&eacute;riode d&#146;attente pour cette p&eacute;riode -->
<dt>schedule.quietPeriod.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.quietPeriod.message"); ?></dd>
<!-- Activ&eacute; -->
<dt>schedule.enabled.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.enabled.label"); ?></dd>
<!-- Activer/D&eacute;sactiver la planification -->
<dt>schedule.enabled.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.enabled.message"); ?></dd>
<!-- Seconde -->
<dt>schedule.second.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.second.label"); ?></dd>
<!-- Minute -->
<dt>schedule.minute.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.minute.label"); ?></dd>
<!-- Heure -->
<dt>schedule.hour.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.hour.label"); ?></dd>
<!-- Jour du mois -->
<dt>schedule.dayOfMonth.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.dayOfMonth.label"); ?></dd>
<!-- Mois -->
<dt>schedule.month.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.month.label"); ?></dd>
<!-- Jour de la semaine -->
<dt>schedule.dayOfWeek.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.dayOfWeek.label"); ?></dd>
<!-- Ann&eacute;e [optionnelle] -->
<dt>schedule.year.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.year.label"); ?></dd>
<!-- La planification ne peut &ecirc;tre supprim&eacute;e, c&#146;est probablement parce qu&#146;elle est utilis&eacute;e par une d&eacute;finition de construction. -->
<dt>schedule.remove.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "schedule.remove.error"); ?></dd>
<!-- Continuum - Rapport Surefire -->
<dt>surefireReport.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.page.title"); ?></dd>
<!-- Rapport Surefire pour %1$s la construction num&eacute;ro %2$s -->
<dt>surefireReport.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.section.title"); ?></dd>
<!-- Bilan -->
<dt>surefireReport.summary</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.summary"); ?></dd>
<!-- Tests -->
<dt>surefireReport.tests</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.tests"); ?></dd>
<!-- Erreurs -->
<dt>surefireReport.errors</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.errors"); ?></dd>
<!-- &Eacute;checs -->
<dt>surefireReport.failures</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.failures"); ?></dd>
<!-- Taux de r&eacute;ussite -->
<dt>surefireReport.successRate</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.successRate"); ?></dd>
<!-- Temps -->
<dt>surefireReport.time</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.time"); ?></dd>
<!-- Liste de paquets -->
<dt>surefireReport.packageList</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.packageList"); ?></dd>
<!-- Cas de test -->
<dt>surefireReport.testCases</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.testCases"); ?></dd>
<!-- Nom du paquet -->
<dt>surefireReport.package</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.package"); ?></dd>
<!-- Classe -->
<dt>surefireReport.class</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.class"); ?></dd>
<!-- Cas de test -->
<dt>surefireReport.testCase</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.testCase"); ?></dd>
<!-- D&eacute;tails de l&#146;echec -->
<dt>surefireReport.failureDetails</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "surefireReport.failureDetails"); ?></dd>
<!-- Continuum - Faire une \"release\" du Projet -->
<dt>releaseProject.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseProject.page.title"); ?></dd>
<!-- Choisir le \"Goal\" de \"release\" pour %1$s -->
<dt>releaseProject.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseProject.section.title"); ?></dd>
<!-- Pr&eacute;parer le projet pour la \"release\" -->
<dt>releaseProject.prepareReleaseOption</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseProject.prepareReleaseOption"); ?></dd>
<!-- Effectuer la \"release\" du projet -->
<dt>releaseProject.performReleaseOption</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseProject.performReleaseOption"); ?></dd>
<!-- Fournir les param&ecirc;tres de la \"release\" -->
<dt>releaseProject.provideReleaseParameters</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseProject.provideReleaseParameters"); ?></dd>
<!-- Pr&eacute;parer le projet pour la \"release\" -->
<dt>releasePrepare.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releasePrepare.section.title"); ?></dd>
<!-- Param&ecirc;tres de la pr&eacute;paration de la \"release\" -->
<dt>releasePrepare.parameters</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releasePrepare.parameters"); ?></dd>
<!-- Version de la \"release\"* -->
<dt>releasePrepare.releaseVersion</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releasePrepare.releaseVersion"); ?></dd>
<!-- Prochaine version de d&eacute;veloppement* -->
<dt>releasePrepare.nextDevelopmentVersion</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releasePrepare.nextDevelopmentVersion"); ?></dd>
<!-- Effectuer la \"release\" du projet -->
<dt>releasePerform.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releasePerform.section.title"); ?></dd>
<!-- Param&ecirc;tres de la r&eacute;alisation de la \"release\" -->
<dt>releasePerform.parameters</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releasePerform.parameters"); ?></dd>
<!-- Ex&eacute;cution du \"Release Goal\" -->
<dt>releaseInProgress.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseInProgress.section.title"); ?></dd>
<!-- Statut -->
<dt>releaseInProgress.status</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseInProgress.status"); ?></dd>
<!-- Phase de \"release\" -->
<dt>releaseInProgress.phase</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseInProgress.phase"); ?></dd>
<!-- Affichage de la sortie -->
<dt>releaseInProgress.viewOutput</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseInProgress.viewOutput"); ?></dd>
<!-- Bilan de la \"release\" du Project -->
<dt>releaseViewResult.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.section.title"); ?></dd>
<!-- D&eacute;tails de la \"release\" du Projet -->
<dt>releaseViewResult.summary</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.summary"); ?></dd>
<!-- Date de d&eacute;but -->
<dt>releaseViewResult.startTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.startTime"); ?></dd>
<!-- Date de fin -->
<dt>releaseViewResult.endTime</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.endTime"); ?></dd>
<!-- &Eacute;tat -->
<dt>releaseViewResult.state</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.state"); ?></dd>
<!-- SUCCES -->
<dt>releaseViewResult.success</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.success"); ?></dd>
<!-- ERREUR -->
<dt>releaseViewResult.error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.error"); ?></dd>
<!-- Sortie de l&#146;ex&eacute;cution de la \"release\" -->
<dt>releaseViewResult.output</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.output"); ?></dd>
<!-- Aucune Sortie -->
<dt>releaseViewResult.noOutput</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "releaseViewResult.noOutput"); ?></dd>
<!-- Nom d&#146;utilisateur -->
<dt>user.username.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.username.label"); ?></dd>
<!-- Entrer le nom d&#146;utilisateur -->
<dt>user.username.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.username.message"); ?></dd>
<!-- Nom complet -->
<dt>user.fullName.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.fullName.label"); ?></dd>
<!-- Entrer le nom complet de l&#146;utilisateur -->
<dt>user.fullName.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.fullName.message"); ?></dd>
<!-- Email -->
<dt>user.email.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.email.label"); ?></dd>
<!-- Entrer l&#146;adresse mail de l&#146;utilisateur -->
<dt>user.email.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.email.message"); ?></dd>
<!-- Mot de passe -->
<dt>user.password.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.password.label"); ?></dd>
<!-- Confirmer le mot de passe -->
<dt>user.passwordTwo.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.passwordTwo.label"); ?></dd>
<!-- Les mots de passe doivent correspondre -->
<dt>user.passwordTwo.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.passwordTwo.message"); ?></dd>
<!-- Groupe d&#146;utilisateurs -->
<dt>user.userGroup.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.userGroup.label"); ?></dd>
<!-- S&eacute;lectionner un groupe d&#146;utilisateurs -->
<dt>user.userGroup.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "user.userGroup.message"); ?></dd>
<!-- Nom -->
<dt>userGroup.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "userGroup.name.label"); ?></dd>
<!-- Entrer le nom du groupe d&#146;utilisateurs -->
<dt>userGroup.name.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "userGroup.name.message"); ?></dd>
<!-- Description -->
<dt>userGroup.description.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "userGroup.description.label"); ?></dd>
<!-- Entrer la description -->
<dt>userGroup.description.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "userGroup.description.message"); ?></dd>
<!-- Environnements de Build -->
<dt>profilesList.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profilesList.page.title"); ?></dd>
<!-- Environnements de Build -->
<dt>profilesList.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profilesList.section.title"); ?></dd>
<!-- Continuum - Environnement de Build -->
<dt>profile.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.page.title"); ?></dd>
<!-- Environnement de Build -->
<dt>profile.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.section.title"); ?></dd>
<!-- Nom de l'environnement de Build -->
<dt>profile.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.name.label"); ?></dd>
<!-- Nom du JDK -->
<dt>profile.jdk.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.jdk.name.label"); ?></dd>
<!-- Le nom du JDK -->
<dt>profile.jdk.name.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.jdk.name.message"); ?></dd>
<!-- Chemin du JDK -->
<dt>profile.jdk.path.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.jdk.path.label"); ?></dd>
<!-- Le Chemin du JDK (Valeur de JAVA_HOME) -->
<dt>profile.jdk.path.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.jdk.path.message"); ?></dd>
<!-- Aucune installation disponible -->
<dt>profile.no.installations</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.no.installations"); ?></dd>
<!-- Un environnement de Build avec le meme nom existe deja. -->
<dt>profile.name.already.exists</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.name.already.exists"); ?></dd>
<!-- Vous devez sp&eacute;cifier un nom. -->
<dt>profile.name.required</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "profile.name.required"); ?></dd>
<!-- Installations -->
<dt>installationsList.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationsList.page.title"); ?></dd>
<!-- Installations -->
<dt>installationsList.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationsList.section.title"); ?></dd>
<!-- Continuum - Installation -->
<dt>installation.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.page.title"); ?></dd>
<!-- Continuum - Installation -->
<dt>installation.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.section.title"); ?></dd>
<!-- Nom -->
<dt>installation.name.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.name.label"); ?></dd>
<!-- Type -->
<dt>installation.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.type.label"); ?></dd>
<!-- Valeur/Chemin -->
<dt>installation.value.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.value.label"); ?></dd>
<!-- Nom de variable d&#146;environnement -->
<dt>installation.varName.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.varName.label"); ?></dd>
<!-- JDK -->
<dt>installation.jdk.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.jdk.type.label"); ?></dd>
<!-- Maven 2 -->
<dt>installation.maven2.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.maven2.type.label"); ?></dd>
<!-- Maven 1 -->
<dt>installation.maven1.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.maven1.type.label"); ?></dd>
<!-- ariable d&#146;environnement -->
<dt>installation.envvar.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.envvar.type.label"); ?></dd>
<!-- ANT -->
<dt>installation.ant.type.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.ant.type.label"); ?></dd>
<!-- Cr&eacute;er un Environnement de Build avec le nom de l&#146;installation -->
<dt>installation.automaticProfile.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.automaticProfile.label"); ?></dd>
<!-- Une Installation avec le meme nom existe deja. -->
<dt>installation.name.duplicate</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installation.name.duplicate"); ?></dd>
<!-- Continuum - Choix du type de l&#146;installation -->
<dt>installationTypeChoice.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationTypeChoice.page.title"); ?></dd>
<!-- Choix du type de l&#146;installation -->
<dt>installationTypeChoice.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationTypeChoice.section.title"); ?></dd>
<!-- Type de l&#146;installation -->
<dt>installationTypeChoice.action.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationTypeChoice.action.label"); ?></dd>
<!-- Outil -->
<dt>installationTypeChoice.tool.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationTypeChoice.tool.label"); ?></dd>
<!-- Variable d&#146;environnement  -->
<dt>installationTypeChoice.envar.label</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "installationTypeChoice.envar.label"); ?></dd>
<!-- Continuum - Mod&egrave;les de d&eacute;finitions de construction -->
<dt>buildDefinition.templates.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.page.title"); ?></dd>
<!-- D&eacute;finition de construction par d&eacute;faut de Continuum -->
<dt>buildDefinition.templates.continuum.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.continuum.section.title"); ?></dd>
<!-- Goals -->
<dt>buildDefinition.templates.goals</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.goals"); ?></dd>
<!-- Arguments -->
<dt>buildDefinition.templates.arguments</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.arguments"); ?></dd>
<!-- Fichier de construction -->
<dt>buildDefinition.templates.buildFile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.buildFile"); ?></dd>
<!-- Planification -->
<dt>buildDefinition.templates.schedule</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.schedule"); ?></dd>
<!-- Environnement de Build -->
<dt>buildDefinition.templates.profile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.profile"); ?></dd>
<!-- Extraction compl&egrave;te -->
<dt>buildDefinition.templates.buildFresh</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.buildFresh"); ?></dd>
<!-- Toujours construire -->
<dt>buildDefinition.templates.alwaysBuild</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.alwaysBuild"); ?></dd>
<!-- Description -->
<dt>buildDefinition.templates.description</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.description"); ?></dd>
<!-- Type -->
<dt>buildDefinition.templates.type</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.type"); ?></dd>
<!-- Mod&egrave;les disponibles -->
<dt>buildDefinition.templates.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.section.title"); ?></dd>
<!-- D&eacute;finitions des constructions disponibles -->
<dt>buildDefinition.templates.buildDefinitions.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.templates.buildDefinitions.section.title"); ?></dd>
<!-- Continuum - Mod&egrave;le de d&eacute;finitions de construction -->
<dt>buildDefinition.template.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.page.title"); ?></dd>
<!-- Mod&egrave;le de d&eacute;finition de lacontruction -->
<dt>buildDefinition.template.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.section.title"); ?></dd>
<!-- Nom -->
<dt>buildDefinition.template.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.name"); ?></dd>
<!-- Goals -->
<dt>buildDefinition.template.buildDefinition.goals</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.goals"); ?></dd>
<!-- Arguments -->
<dt>buildDefinition.template.buildDefinition.arguments</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.arguments"); ?></dd>
<!-- Fichier de construction -->
<dt>buildDefinition.template.buildDefinition.buildFile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.buildFile"); ?></dd>
<!-- Planification -->
<dt>buildDefinition.template.buildDefinition.schedule</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.schedule"); ?></dd>
<!-- Environnement de Build -->
<dt>buildDefinition.template.buildDefinition.profile</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.profile"); ?></dd>
<!-- Extraction compl&egrave;te ? -->
<dt>buildDefinition.template.buildDefinition.buildFresh</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.buildFresh"); ?></dd>
<!-- D&eacute;faut -->
<dt>buildDefinition.template.buildDefinition.default</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.default"); ?></dd>
<!-- Description -->
<dt>buildDefinition.template.buildDefinition.description</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.description"); ?></dd>
<!-- Type -->
<dt>buildDefinition.template.buildDefinition.type</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinition.template.buildDefinition.type"); ?></dd>
<!-- Nom -->
<dt>buildDefinitionTemplate.name</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinitionTemplate.name"); ?></dd>
<!-- --- D&eacute;finitions des constructions disponibles --- -->
<dt>buildDefinitionTemplate.available.builddefinitions</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinitionTemplate.available.builddefinitions"); ?></dd>
<!-- --- D&eacute;finitions de construction utilis&eacute;es --- -->
<dt>buildDefinitionTemplate.available.builddefinitions.used</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinitionTemplate.available.builddefinitions.used"); ?></dd>
<!-- Configurer les d&eacute;finitions de constructions utilis&eacute;es -->
<dt>buildDefinitionTemplate.builddefinitions.define</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildDefinitionTemplate.builddefinitions.define"); ?></dd>
<!-- Continuum - Liste de constructions -->
<dt>buildQueue.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.page.title"); ?></dd>
<!-- Continuum - Liste de constructions -->
<dt>buildQueue.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.section.title"); ?></dd>
<!-- La Liste de constructions est vide -->
<dt>buildQueue.empty</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.empty"); ?></dd>
<!-- Construction en cours -->
<dt>buildQueue.currentTask.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.currentTask.section.title"); ?></dd>
<!-- Nom du Projet -->
<dt>buildQueue.currentTask.projectName</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.currentTask.projectName"); ?></dd>
<!-- D&eacute;finition de la construction -->
<dt>buildQueue.currentTask.buildDefinition</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.currentTask.buildDefinition"); ?></dd>
<!-- Aucune construction en cours -->
<dt>buildQueue.no.currentTaks</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.no.currentTaks"); ?></dd>
<!-- Annuler les entr&eacute;es -->
<dt>buildQueue.removeEntries</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildQueue.removeEntries"); ?></dd>
<!-- Extraction en cours -->
<dt>checkoutQueue.currentTask.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "checkoutQueue.currentTask.section.title"); ?></dd>
<!-- Nom du projet -->
<dt>checkoutQueue.currentTask.projectName</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "checkoutQueue.currentTask.projectName"); ?></dd>
<!-- Aucune extraction en cours -->
<dt>checkoutQueue.no.currentTaks</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "checkoutQueue.no.currentTaks"); ?></dd>
<!-- Extractions en attente -->
<dt>checkoutQueue.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "checkoutQueue.section.title"); ?></dd>
<!-- La liste des extractions en attente est vide -->
<dt>checkoutQueue.empty</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "checkoutQueue.empty"); ?></dd>
<!-- Annuler les entr&eacute;es -->
<dt>checkoutQueue.removeEntries</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "checkoutQueue.removeEntries"); ?></dd>
<!-- Continuum - Supprimer les r&eacute;sultats de contructions -->
<dt>buildResult.delete.confirmation.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.delete.confirmation.page.title"); ?></dd>
<!-- Supprimer les r&eacute;sultats de contructions -->
<dt>buildResult.delete.confirmation.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.delete.confirmation.section.title"); ?></dd>
<!-- Voulez-vous vraiment supprimer les r&eacute;sultats de construction %1$s&nbsp;?  -->
<dt>buildResult.delete.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.delete.confirmation.message"); ?></dd>
<!-- Impossible de supprimer un r&eacute;sultat de construction qui est actuellement en construction. -->
<dt>buildResult.cannot.delete</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "buildResult.cannot.delete"); ?></dd>
<!-- L&eacute;gende -->
<dt>legend.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.title"); ?></dd>
<!-- Construire maintenant -->
<dt>legend.buildNow</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.buildNow"); ?></dd>
<!-- Historique -->
<dt>legend.buildHistory</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.buildHistory"); ?></dd>
<!-- Construction en cours -->
<dt>legend.buildInProgress</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.buildInProgress"); ?></dd>
<!-- Copie de travail -->
<dt>legend.workingCopy</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.workingCopy"); ?></dd>
<!-- Extraction en cours -->
<dt>legend.checkingOutBuild</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.checkingOutBuild"); ?></dd>
<!-- Construction en attente -->
<dt>legend.queuedBuild</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.queuedBuild"); ?></dd>
<!-- Annuler construction -->
<dt>legend.cancelBuild</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.cancelBuild"); ?></dd>
<!-- Supprimer -->
<dt>legend.delete</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.delete"); ?></dd>
<!-- Editer -->
<dt>legend.edit</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.edit"); ?></dd>
<!-- Faire une &#146;release&#146; -->
<dt>legend.release</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.release"); ?></dd>
<!-- Construction r&eacute;ussie -->
<dt>legend.buildInSuccess</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.buildInSuccess"); ?></dd>
<!-- Construction en &eacute;chec -->
<dt>legend.buildInFailure</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.buildInFailure"); ?></dd>
<!-- Construction en erreur -->
<dt>legend.buildInError</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "legend.buildInError"); ?></dd>
<!-- L'ajout du projet est en cours ... -->
<dt>wait.addprocessing.processing</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "wait.addprocessing.processing"); ?></dd>
<!-- Continuum - Supprimer des projets Continuum -->
<dt>deleteProjects.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteProjects.page.title"); ?></dd>
<!-- Supprimer des projets Continuum -->
<dt>deleteProjects.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteProjects.section.title"); ?></dd>
<!-- Etes vous s&ucirc;r de vouloir supprimer les projets \"%1$s\" ? -->
<dt>deleteProjects.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteProjects.confirmation.message"); ?></dd>
<!-- Continuum - Supprimer des Build Environment -->
<dt>deleteBuildEnv.page.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteBuildEnv.page.title"); ?></dd>
<!-- Supprimer un Build Environment -->
<dt>deleteBuildEnv.section.title</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteBuildEnv.section.title"); ?></dd>
<!-- Etes vous s&ucirc;r de vouloir supprimer le Build Environment \"%1$s\" ? -->
<dt>deleteBuildEnv.confirmation.message</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "deleteBuildEnv.confirmation.message"); ?></dd>?>
<!-- Une erreur est survenue lors de l'acc&#232;s &#224; la base de donn&#233;es. -->
<dt>database_error</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "database_error"); ?></dd>?>
<!-- Supprimer l'instance priv&eacute;e -->
<dt>delete_private_instance</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "delete_private_instance"); ?></dd>?>
<!-- Tag : -->
<dt>form_release_project_tag</dt>
<dd><?php echo dgettext ("gforge-plugin-novacontinuum", "form_release_project_tag"); ?></dd>?>
</dl>
