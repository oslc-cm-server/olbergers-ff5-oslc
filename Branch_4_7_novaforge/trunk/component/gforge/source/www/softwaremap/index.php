<?php
/**
  *
  * SourceForge Trove Software Map
  *
  * SourceForge: Breaking Down the Barriers to Open Source Development
  * Copyright 1999-2001 (c) VA Linux Systems
  * http://sourceforge.net
  *
  * @version   $Id: index.php 184 2001-11-01 19:19:19Z lo-lan-do $
  *
  */

header('Location: trove_list.php');

?>
