<?php
/*
 *
 * Novaforge is a registered trade mark from Bull S.A.S
 * Copyright (C) 2007 Bull S.A.S.
 * 
 * http://novaforge.org/
 *
 *
 * This file has been developped within the Novaforge(TM) project from Bull S.A.S
 * and contributed back to GForge community.
 *
 * GForge is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
?>
<html>
<head>
<title><project_name></title>
</head>
<body>
<p>
<div align="center"><b><project_name></b></div>
<p>
<b>Project description:</b>
<br>
<project_description>
<p>
<b>Links:</b>
<br>
<b><a href="/projects/<group_name>/">Project summary</a></b>
<br>
<b><a href="/forum/?group_id=<group_id>">Forums</a></b>
<br>
<b><a href="/mail/?group_id=">Mailing lists</a></b>
<br>
<b><a href="/docman/?group_id=<group_id>">Documentation</a></b>
<br>
<b><a href="/scm/?group_id=<group_id>">Sources</a></b>
<br>
<b><a href="/frs/?group_id=<group_id>">Files</a></b>
</body>
</html>
