<?php
//
// %FRIENDLY_NAME% configuration
//
// Really Important Safety Tip: --> DO NOT LEAVE ANY WHITE
// SPACE AFTER THE CLOSING PHP TAG AT THE END OF THIS FILE!
//
// Doing so will really confuse the software and cause
// 1) cookies to fail and 2) HTML page headers to fail
// which will give you some preally hard-to-debug problems.
// Why? PHP is a *pre-processor* -- anything that's not PHP gets
// emitted as part of the HTML stream and processed by the browser,
// so white space is meaningful!
//
// GForge global paths
//
$sys_etc_path='%SYSCONFDIR%/%NAME%';
$sys_opt_path='%DATADIR%/%NAME%';
$sys_var_path='%LOCALSTATEDIR%/lib/%NAME%';



//
// Gforge database type
//
//$sys_database_type='mysql';
$sys_database_type='pgsql';

//
//
// GForge hostnames
//
// Hostnames should be fully qualified domain names (FQDNs); using short names
// would be prettier but would stop you from distributing your SourceForge
// implementation across multiple domains.
//
// Of course, if you have a lot of machines serving a particular purpose
// such as FTP or for shell accounts, the "hostname" here might be in
// reality an addr_list of machines that is serviced by a round-robin
// mechanism or something fancy like a local-director.
//
// The default GForge domain
// this is used where ever the "naked" form of the GForge domain
// might be used.  E.g., "mailto:admin@gforge.net"
$sys_default_domain = '@fqdn_hostname@';
$sys_forum_return_domain = '';
$sys_fallback_domain = '';

// Machine used for downloading sources/packages
$sys_download_host = $sys_default_domain;

// Machine(s) that host users' shell accounts
//  N.B. to the SourceForge Crew: What's the difference between the user
// host and the shell host?  They are clearly two different hostnames
// in the source code, but they seem to serve the same purpose..?
$sys_shell_host = $sys_default_domain;
$sys_users_host = $sys_default_domain;

// Machine that hosts the GForge mailing lists (This could also be
// the mail host if you have enough horsepower & bandwidth)
$sys_lists_host = $sys_default_domain;

//
// SCM configuration
//

// Machine that hosts SCM
$sys_scm_host = $sys_default_domain;
$sys_cvs_host = $sys_scm_host;

// Force the use of a single scm host instead of scm.project.domain.com
// Set to 1 to use scm.domain.com for all projects
// Set to 0 to use scm.project.domain.com
$sys_scm_single_host = 1;

// Path to tarballs directory
$sys_scm_tarballs_path = $sys_var_path.'/tarballs';

// Path to snapshots directory
$sys_scm_snapshots_path = $sys_var_path.'/snapshots';

// Path to SCMWEB
$sys_path_to_scmweb = $sys_opt_path.'/www/plugins';


//Databases, html/php/other paths
//server to use for updates and reads
//If this is null (i.e. ""), then gforge will use Unix sockets to connect
//to the database.
$sys_dbhost = 'localhost';
$sys_dbport = '5432';
$sys_dbuser = '%NAME%';
$sys_dbpasswd = '@sys_dbpasswd@';
$sys_dbname = '%NAME%';
$sys_server = 'pgsql';

//
// Passwords
//
$sys_ldap_passwd='';
$sys_jabber_pass='';

//
// Management of system accounts
//
// 'UNIX' : standard UNIX files or PostgreSQL backend
// 'LDAP' : LDAP backend
$sys_account_manager_type = 'UNIX';

// email address to send admin alerts to
$sys_admin_email = 'gforge@'.$sys_default_domain;

// Path to sendmail program
$sys_sendmail_path='/usr/sbin/sendmail';

// Select unix_password cipher
// Normally there will be no reason to change this for Linux based systems
// Valid choices are MD5, DES, Blowfish, or Plain
// DO NOT CHANGE ON EXISTING INSTALL!!!

// If using usergroup_wrapper.php (for non-Linux systems)
// this value MUST be plain
$unix_cipher = 'MD5';

//
// LDAP configuration
//
// enable(1) or disable(0) ldap use altogether
$sys_ldap_host='ldap.'.$sys_default_domain;
$sys_ldap_port=389;
$sys_ldap_version=3;
// this is dn under which all information stored
$sys_ldap_base_dn='dc=gforge,dc=company,dc=com';
// and this, how we do access it (add permission required)
$sys_ldap_bind_dn='cn=SF_robot,dc=gforge,dc=company,dc=com';
// admin dn - login dn which has permissions to delete entries
// NOT used by web code, only by support utilities
// note that password NOT stored here
$sys_ldap_admin_dn='cn=admin,dc=gforge,dc=company,dc=com';

//
// Jabber Configuration
//
$sys_use_jabber=0;
//messages from the system will be sent to this address
$sys_jabber_server='jabber.gforge.company.com';
$sys_jabber_port='5222';
// messages sent to jabber accounts will come from this user
// It is similar to the "From: noreply@gforge.org" used in emails
$sys_jabber_user='noreply';

//
//      FEATURES
//      You can turn features on/off sitewide
//
$sys_use_scm = @sys_use_scm@;
$sys_use_tracker = @sys_use_tracker@;
$sys_use_forum = @sys_use_forum@;
$sys_use_pm = @sys_use_pm@;
$sys_use_docman = @sys_use_docman@;
$sys_use_news = @sys_use_news@;
$sys_use_mail = @sys_use_mail@;
$sys_use_survey = @sys_use_survey@;
$sys_use_frs = @sys_use_frs@;
$sys_use_fti = @sys_use_fti@;
$sys_use_ftp = false;
$sys_use_trove = @sys_use_trove@;
$sys_use_snippet = @sys_use_snippet@;
$sys_use_ssl = @sys_use_ssl@;
$sys_use_people = @sys_use_people@;
$sys_use_shell = @sys_use_shell@;

// Enable/Disable user ratings
$sys_use_ratings=false;
// Enable/Disable the ability to upload files using FTP in FRS
$sys_use_ftpuploads = false;
// Enable/Disable the use of mail gateways for trackers and forums
$sys_use_gateways = true;

// Enable/Disable the ability to add additionnal vhost for a project
$sys_use_project_vhost = false;
// Enable/Disable the ability to have database for a project (backend not implemented)
$sys_use_project_database = false;
// Enable/Disable the ability to add images for a project (frontend not implemented)
$sys_use_project_multimedia = false;

//
//      Restricted project registration
//      If set to true, only a site admin can register projects
//
$sys_project_reg_restricted = false;
//
//      Restricted user registration
//      If set to true, only a site admin can register users
//
$sys_user_reg_restricted = false;

//
// Groups and Homes dir prefix
//
$homedir_prefix=$sys_var_path.'/home/users';
$groupdir_prefix=$sys_var_path.'/home/groups';
$sys_homepage_single_host = true;
$sys_homepage_prefix = '/home';

//
// SCM directory prefixes
//
$cvsdir_prefix = '/cvsroot';
$svndir_prefix = '/svnroot';

$sys_chroot = '';

//
// File Upload Configuration
//
// Create a directory, which is writable by your webserver, but not
// within its document root (does not fall under www/ in the tarball)
// Your php.ini file may have to be modified to allow writing outside
// the webserver's directory
//
$sys_upload_dir = $sys_var_path.'/uploads';
$sys_ftp_upload_dir = '';
$sys_ftp_upload_host = '';
//$sys_ftp_upload_chowner = '{ftpuploadchowner}';
$sys_apache_user = '%APACHE_USER%';
$sys_apache_group = '%APACHE_GROUP%';
$sys_apache_pid_file = '%LOCALSTATEDIR%/run/httpd.pid';

// Where the GForge files are placed
// *** IMPORTANT: sys_urlroot *MUST* be an ABSOLUTE FILEYSTEM PATH NAME
//             that points to the www directory of the GForge
//             installation.  If you use ANY form of relative path
//             you will break the html_image function in include/html.php
//
$sys_urlroot = $sys_opt_path.'/www';
$sys_urlprefix="";

// Name of the system as a whole (needed by various utils and titles)
$sys_name = '%FRIENDLY_NAME%';

// Mailman base installation directory
$sys_path_to_mailman = '%MAILMANBINARIESDIR%';

// session cookie settings
//
//      IMPORTANT - YOU MUST CHANGE "foobar" to a long, random number
//
$sys_session_key = '@sys_session_key@';
$sys_session_expire = 60 * 60 * 24 * 7;

// Require that user give unique (not yet existent in db) email upon
// registration
$sys_require_unique_email = 0;
//
// Require that all email be copied to this address if present
$sys_bcc_all_email_address = '';

// GUI modifications (menu colors, etc.)
//      See the top of the file include/html.php, this is where the menu colors
//      and colors used throughout GForge are defined.

// Themeing related vars... Some of this needs to change in the session stuff
// The theme base directory, everything else is handled by theme_sysinit()
$sys_themeroot = $sys_opt_path.'/www/themes/';
// If you want an other default theme or language
$sys_theme = '@sys_theme@';
$sys_lang = '@sys_lang@';
$sys_default_timezone = '@sys_default_timezone@';
$sys_default_country_code = '@sys_default_country_code@';

// Akamization of images
//      example: http://images.gforge.company.com
$sys_images_url = '';
$sys_images_secure_url = '';

// Groups
//      The GForge permission model is based on groups
//      certain parts of the site, like news, stats, etc
//      are based on special group_id numbers
//      group_id #1 is the super-user group of sitewide admins
$sys_news_group = 3;
$sys_stats_group = 2;
$sys_peer_rating_group = 4;
$sys_template_group = 5;
$default_trove_cat = 18;

// JPGRAPH Package
$sys_path_to_jpgraph = $sys_opt_path.'/www/jpgraph';

// Show Source
//      Setting this to 1 will add a "Show Source" link to the bottom of each page
$sys_show_source = 0;

// Force Login
$sys_force_login = 0;

// Place for customized files
$sys_custom_path = $sys_etc_path.'/custom';


//
// Localization caching Configuration
//

// Enable localization caching system
$sys_localization_enable_caching = true;

// Create a directory, which is writable by your webserver, but not
// within its document root (does not fall under www/ in the tarball)
// Your php.ini file may have to be modified to allow writing outside
// the webserver's directory
$sys_localization_cache_path = $sys_var_path.'/localizationcache/';

// Enable timestamp checking (if disabled, you have to remove manually cache files on update)
$sys_localization_enable_timestamp_checking = true;
//
// Plugins configuration
//
// Path to plugins directory
$sys_plugins_path = $sys_opt_path.'/plugins/';

?>