<dl>
<!-- Mettre à jour -->
<dt>udpate_project</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "udpate_project"); ?></dd>
<!-- Le projet Mantis n'existe pas. -->
<dt>error_mantis_project</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "error_mantis_project"); ?></dd>
<!-- Le plugin_mantis_project_id n'est pas renseigné. -->
<dt>error_mantis_project_id</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "error_mantis_project_id"); ?></dd>
<!-- Afficher -->
<dt>display_project</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "display_project"); ?></dd>
<!-- Aucun projet n'est défini. -->
<dt>no_project_available</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "no_project_available"); ?></dd>
<!-- Plugin Mantis -->
<dt>title_display_error</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "title_display_error"); ?></dd>
<!-- Projet Mantis $1 -->
<dt>title_display_project</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "title_display_project"); ?></dd>
<!-- Une erreur est survenue lors de l'authentification sur le serveur Mantis $1. -->
<dt>authentication_failure</dt>
<dd><?php echo dgettext ("gforge-plugin-mantis", "authentication_failure"); ?></dd>
</dl>

