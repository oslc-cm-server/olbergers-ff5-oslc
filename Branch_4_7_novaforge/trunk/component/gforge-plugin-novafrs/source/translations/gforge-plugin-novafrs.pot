# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-01-16 07:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: plugincore/include/AuthView.class.php:36
msgid "authLabel1"
msgstr ""

#: plugincore/include/AuthView.class.php:37
msgid "authLabel2"
msgstr ""

#: plugincore/include/AuthView.class.php:38
msgid "authLabel3"
msgstr ""

#: plugincore/include/AuthView.class.php:39
msgid "authLabel4"
msgstr ""

#: plugincore/include/AuthView.class.php:51
msgid "propagateAuth"
msgstr ""

#: plugincore/include/AuthView.class.php:221
msgid "infoDefaultAuth"
msgstr ""

#: plugincore/include/AuthView.class.php:222
#: plugincore/include/FileCardView.class.php:368
msgid "submit"
msgstr ""

#: plugincore/include/File.class.php:133 plugincore/include/File.class.php:202
msgid "fr_unique"
msgstr ""

#: plugincore/include/File.class.php:197 plugincore/include/File.class.php:654
msgid "error_min_title_length"
msgstr ""

#: plugincore/include/File.class.php:289 plugincore/include/File.class.php:310
#: plugincore/include/File.class.php:345
msgid "invalid_frid"
msgstr ""

#: plugincore/include/FileCardView.class.php:89
msgid "displayHistory"
msgstr ""

#: plugincore/include/FileCardView.class.php:94
#: plugincore/include/FileCardView.class.php:259
msgid "date"
msgstr ""

#: plugincore/include/FileCardView.class.php:95
#: plugincore/include/FileCardView.class.php:213 pluginwww/chronos.php:125
msgid "version"
msgstr ""

#: plugincore/include/FileCardView.class.php:96
#: plugincore/include/FileCardView.class.php:228
#: plugincore/include/FileTreeView.class.php:176 pluginwww/chronos.php:134
msgid "status"
msgstr ""

#: plugincore/include/FileCardView.class.php:97
#: plugincore/include/FileCardView.class.php:191 pluginwww/chronos.php:110
msgid "title"
msgstr ""

#: plugincore/include/FileCardView.class.php:123
#: plugincore/include/FileTreeView.class.php:245 pluginwww/chronos.php:62
msgid "viewCard"
msgstr ""

#: plugincore/include/FileCardView.class.php:180
msgid "file"
msgstr ""

#: plugincore/include/FileCardView.class.php:182
msgid "viewURL"
msgstr ""

#: plugincore/include/FileCardView.class.php:192 pluginwww/chronos.php:113
msgid "author"
msgstr ""

#: plugincore/include/FileCardView.class.php:197
#: plugincore/include/FileCardView.class.php:261 pluginwww/chronos.php:116
msgid "description"
msgstr ""

#: plugincore/include/FileCardView.class.php:202 pluginwww/chronos.php:119
msgid "writingDate"
msgstr ""

#: plugincore/include/FileCardView.class.php:206 pluginwww/chronos.php:128
msgid "frtype"
msgstr ""

#: plugincore/include/FileCardView.class.php:214 pluginwww/chronos.php:131
msgid "reference"
msgstr ""

#: plugincore/include/FileCardView.class.php:220
#: plugincore/include/utils.php:119
msgid "language"
msgstr ""

#: plugincore/include/FileCardView.class.php:235
msgid "state"
msgstr ""

#: plugincore/include/FileCardView.class.php:240
msgid "delete_fr"
msgstr ""

#: plugincore/include/FileCardView.class.php:250
msgid "observation"
msgstr ""

#: plugincore/include/FileCardView.class.php:260
msgid "name"
msgstr ""

#: plugincore/include/FileCardView.class.php:285
msgid "group"
msgstr ""

#: plugincore/include/FileCardView.class.php:313
msgid "upload_url"
msgstr ""

#: plugincore/include/FileCardView.class.php:320
msgid "upload"
msgstr ""

#: plugincore/include/FileCardView.class.php:327
msgid "archive"
msgstr ""

#: plugincore/include/FileCardView.class.php:331
#, php-format
msgid "upload_ftp"
msgstr ""

#: plugincore/include/FileCardView.class.php:344
msgid "upload_file"
msgstr ""

#: plugincore/include/FileCardView.class.php:352
msgid "or_upload_url"
msgstr ""

#: plugincore/include/FileCardView.class.php:390 pluginwww/admin/index.php:258
msgid "delete_warning"
msgstr ""

#: plugincore/include/FileCardView.class.php:392 pluginwww/admin/index.php:260
#: pluginwww/admin/index.php:280
msgid "sure"
msgstr ""

#: plugincore/include/FileCardView.class.php:394 pluginwww/admin/index.php:263
#: pluginwww/admin/index.php:282
msgid "delete"
msgstr ""

#: plugincore/include/FileFactory.class.php:191
msgid "no_frs"
msgstr ""

#: plugincore/include/FileGroupFrs.class.php:109
msgid "group_unique"
msgstr ""

#: plugincore/include/FileGroupFrs.class.php:128
msgid "name_required"
msgstr ""

#: plugincore/include/FileGroupFrs.class.php:136
#: plugincore/include/FileGroupFrs.class.php:273
msgid "invalid_parent_id"
msgstr ""

#: plugincore/include/FileGroupFrs.class.php:199
msgid "invalid_id"
msgstr ""

#: plugincore/include/FileTreeView.class.php:173
msgid "head_file"
msgstr ""

#: plugincore/include/FileTreeView.class.php:174
msgid "head_type"
msgstr ""

#: plugincore/include/FileTreeView.class.php:175
msgid "head_size"
msgstr ""

#: plugincore/include/FileTreeView.class.php:177
msgid "head_modif_by"
msgstr ""

#: plugincore/include/FileTreeView.class.php:178
msgid "head_the"
msgstr ""

#: plugincore/include/novafrsPlugin.class.php:92
msgid "tab_title"
msgstr ""

#: plugincore/include/novafrsPlugin.class.php:106
msgid "use_novafrs"
msgstr ""

#: plugincore/include/utils.php:49 pluginwww/admin/auth.php:98
#: pluginwww/admin/index.php:57 pluginwww/admin/index.php:92
#: pluginwww/admin/index.php:115 pluginwww/admin/index.php:297
#: pluginwww/admin/index.php:302 pluginwww/card.php:44 pluginwww/card.php:49
#: pluginwww/card.php:106 pluginwww/card.php:120 pluginwww/card.php:128
#: pluginwww/card.php:271 pluginwww/card.php:275 pluginwww/card.php:289
#: pluginwww/card.php:291 pluginwww/card.php:298 pluginwww/card.php:313
#: pluginwww/card.php:331 pluginwww/card.php:358 pluginwww/index.php:40
#: pluginwww/index.php:45
msgid "error"
msgstr ""

#: plugincore/include/utils.php:49
msgid "turned_off"
msgstr ""

#: plugincore/include/utils.php:55
msgid "view_fr"
msgstr ""

#: plugincore/include/utils.php:57
msgid "menuChrono"
msgstr ""

#: plugincore/include/utils.php:69
msgid "submit_new"
msgstr ""

#: plugincore/include/utils.php:74
msgid "copyBranch"
msgstr ""

#: plugincore/include/utils.php:76
msgid "adminDir"
msgstr ""

#: plugincore/include/utils.php:85
msgid "manageAuth"
msgstr ""

#: plugincore/include/utils.php:121
msgid "all_languages"
msgstr ""

#: plugincore/include/utils.php:129
msgid "go"
msgstr ""

#: plugincore/include/utils.php:192
msgid "add_frs"
msgstr ""

#: pluginwww/admin/auth.php:125 pluginwww/admin/copy.php:47
#: pluginwww/admin/index.php:137 pluginwww/admin/index.php:216
#: pluginwww/admin/index.php:253 pluginwww/admin/index.php:273
#: pluginwww/admin/index.php:309
msgid "title_admin"
msgstr ""

#: pluginwww/admin/copy.php:64
msgid "copyBranchToCopy"
msgstr ""

#: pluginwww/admin/copy.php:72
msgid "copyBranchName"
msgstr ""

#: pluginwww/admin/index.php:57 pluginwww/admin/index.php:92
#: pluginwww/card.php:120 pluginwww/card.php:275
msgid "noWriteAuth"
msgstr ""

#: pluginwww/admin/index.php:82 pluginwww/card.php:262
msgid "update_successful"
msgstr ""

#: pluginwww/admin/index.php:99
msgid "create_successful"
msgstr ""

#: pluginwww/admin/index.php:102 pluginwww/card.php:378
#: pluginwww/chronos.php:91
msgid "deleted"
msgstr ""

#: pluginwww/admin/index.php:115 pluginwww/card.php:358
msgid "noDeleteAuth"
msgstr ""

#: pluginwww/admin/index.php:139
msgid "section_admin_groups"
msgstr ""

#: pluginwww/admin/index.php:155
msgid "group_id"
msgstr ""

#: pluginwww/admin/index.php:156 pluginwww/admin/index.php:223
msgid "group_name"
msgstr ""

#: pluginwww/admin/index.php:166
msgid "error_no_groups_defined"
msgstr ""

#: pluginwww/admin/index.php:169
msgid "add_group"
msgstr ""

#: pluginwww/admin/index.php:173
msgid "new_group_name"
msgstr ""

#: pluginwww/admin/index.php:178
msgid "new_group_parent"
msgstr ""

#: pluginwww/admin/index.php:183
msgid "add"
msgstr ""

#: pluginwww/admin/index.php:187 pluginwww/admin/index.php:238
msgid "editgroups_description"
msgstr ""

#: pluginwww/admin/index.php:218
msgid "edit_group"
msgstr ""

#: pluginwww/admin/index.php:228
msgid "group_parent"
msgstr ""

#: pluginwww/admin/index.php:234
msgid "edit"
msgstr ""

#: pluginwww/admin/index.php:243
msgid "delete_group"
msgstr ""

#: pluginwww/admin/index.php:261
msgid "really_sure"
msgstr ""

#: pluginwww/admin/index.php:278
msgid "delete_warning_group"
msgstr ""

#: pluginwww/admin/index.php:312
#, php-format
msgid "section_admin_main"
msgstr ""

#: pluginwww/admin/index.php:314
msgid "add_edit_frgroups"
msgstr ""

#: pluginwww/admin/index.php:319
msgid "error_no_frs"
msgstr ""

#: pluginwww/card.php:128
#, php-format
msgid "error_invalid_file_attack"
msgstr ""

#: pluginwww/card.php:134
msgid "bad_filemane"
msgstr ""

#: pluginwww/card.php:153
msgid "error_on_update"
msgstr ""

#: pluginwww/card.php:271
msgid "no_valid_group"
msgstr ""

#: pluginwww/card.php:289
msgid "error_blank_file"
msgstr ""

#: pluginwww/card.php:298
msgid "invalid_filename"
msgstr ""

#: pluginwww/card.php:336
msgid "submitted_successfully"
msgstr ""

#: pluginwww/card.php:342 pluginwww/card.php:384 pluginwww/card.php:409
#: pluginwww/chronos.php:44 pluginwww/index.php:88
msgid "title_display"
msgstr ""

#: pluginwww/chronos.php:107
msgid "chrono"
msgstr ""

#: pluginwww/chronos.php:122
msgid "updateDate"
msgstr ""

#: pluginwww/view.php:83
msgid "no_file_data_title"
msgstr ""

#: pluginwww/view.php:83
msgid "no_file_data_text"
msgstr ""
