DROP TABLE plugin_report_javancss_package;

DROP TABLE plugin_report_javancss_object;

DROP TABLE plugin_report_javancss_function;

DROP TABLE plugin_report_javancss;

DROP SEQUENCE plugin_report_javancss_package_pk_seq;

DROP SEQUENCE plugin_report_javancss_object_pk_seq;

DROP SEQUENCE plugin_report_javancss_function_pk_seq;

DROP SEQUENCE plugin_report_javancss_pk_seq;

DROP TABLE plugin_report_checkstyle;

DROP SEQUENCE plugin_report_checkstyle_pk_seq;

DROP TABLE plugin_report_checker_checkstyle;

DROP TABLE plugin_report_maven_info;

DROP SEQUENCE plugin_report_maven_info_pk_seq;