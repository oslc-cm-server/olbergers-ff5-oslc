#!/bin/sh

#
# This script builds a component
#
# Parameters:
# $1: target directory
# $2: SVN location (trunk, tags, branches)
# $3: component name
# $4: source version
# $5: package release
# $6: branch identifier

#
# Environment variable that MUST be defined
# by the calling script
#
# SVN_URL SVN_USER SVN_PASS

#
# Binaries
#
BIN_CP="/bin/cp"
BIN_CUT="/bin/cut"
BIN_ECHO="/bin/echo"
BIN_GETDIST="/usr/bin/getdist"
BIN_LS="/bin/ls"
BIN_MKDIR="/bin/mkdir"
BIN_MV="/bin/mv"
BIN_RM="/bin/rm"
BIN_RPM="/bin/rpm"
BIN_RPMBUILD="/usr/bin/rpmbuild"
BIN_SVN="/usr/bin/svn"
BIN_TAR="/bin/tar"
BIN_MSGFMT="/usr/bin/msgfmt"

#
# Parameters
#
TARGET_DIR=""
SVN_LOCATION=""
SVN_COMPONENT=""
SVN_VERSION=""
SVN_RELEASE=""
SVN_BRANCH=""
TANSLATION_DIR_SOURCE="translations"

#
# Internal variables
#
EXIT=0
BUILD=1
SVN_PATH_PACKAGE=""
SVN_PATH_SOURCE=""
SVN_LOG=""
RPM_LOG=""
RPM_TOP_DIR=""
BUILD_DIR=""
RPMS_DIR=""
SOURCES_DIR=""
SPECS_DIR=""
SRPMS_DIR=""

#
# Config variables
#
TARGET_OS=""
USE_EXTRA_VERSION_LEVEL=0
CREATE_TARBALL=0
TARBALL_COMPONENT=""
TARBALL_VERSION=""
EXCLUDE_FROM_TARBALL=""
USE_EXTRA_SOURCES=0
EXTRA_SOURCES_DIRS=""
RPM_COMPONENT=""
RPM_VERSION=""
RPM_RELEASE=""
RPM_ARCH=""
RPM_SUBCOMPONENTS=""
POST_RPM_SCRIPT=""

#
# Functions
#

pause(){
	echo -e "$1"
  read -p "Press any key to continue ..."
}

invert_string()
# $1: string to invert
{
	STRING=$1
	GNIRTS=""
	while [ -n "$STRING" ] ; do
		GNIRTS="`$BIN_ECHO $STRING | $BIN_CUT -c1`$GNIRTS"
		STRING=`$BIN_ECHO $STRING | $BIN_CUT -c2-`
	done
}

delete_dir()
# $1: dir to delete
{
	DIR=$1
	if [ -n "$DIR" ] ; then
		if [ "$DIR" != "/" ] ; then
			$BIN_RM -rf $DIR
		else
			$BIN_ECHO "-> The name of the directory to delete is '/' !!!"
			EXIT=1
		fi
	else
		$BIN_ECHO "-> The name of the directory to delete is empty"
		EXIT=1
	fi
}

readconfig()
# $1: target directory
# $2: SVN location (trunk, tags, branches)
# $3: component name
# $4: source version
# $5: package release
# $6: branch identifier
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Reading configuration"
		if [ -z "$1" ] ; then
			$BIN_ECHO "-> Target directory parameter is missing"
			EXIT=1
		else
			if [ -d $1 ] ; then
				TARGET_DIR=$1
			else
				$BIN_ECHO "-> Target directory '$1' does not exist"
				EXIT=1
			fi
		fi
		if [ -z "$2" ] ; then
			$BIN_ECHO "-> SVN location parameter is missing"
			EXIT=1
		else
			if [ "$2" != "trunk" -a "$2" != "tags" -a "$2" != "branches" ] ; then
				$BIN_ECHO "-> SVN location '$2' is not supported"
				EXIT=1
			else
				SVN_LOCATION=$2
			fi
		fi
		if [ -z "$3" ] ; then
			$BIN_ECHO "-> SVN component parameter is missing"
			EXIT=1
		else
			SVN_COMPONENT=$3
		fi
		if [ -z "$4" ] ; then
			if [ "$SVN_LOCATION" = "tags" -o "$SVN_LOCATION" = "branches" ] ; then
				$BIN_ECHO "-> SVN version parameter is missing"
				EXIT=1
			fi
		else
			SVN_VERSION=$4
		fi
		if [ -z "$5" ] ; then
			if [ "$SVN_LOCATION" = "tags" -o "$SVN_LOCATION" = "branches" ] ; then
				$BIN_ECHO "-> SVN release parameter is missing"
				EXIT=1
			fi
		else
			SVN_RELEASE=$5
		fi
		if [ -z "$6" ] ; then
			if [ "$SVN_LOCATION" = "branches" ] ; then
				$BIN_ECHO "-> SVN branch parameter is missing"
				EXIT=1
			fi
		else
			SVN_BRANCH=$6
		fi
		if [ -x $BIN_GETDIST ] ; then
			HOST_OS=`$BIN_GETDIST`
		else
			$BIN_ECHO "-> Binary '$BIN_GETDIST' is missing"
			EXIT=1
		fi
		if [ ! -x $BIN_SVN ] ; then
			$BIN_ECHO "-> Binary '$BIN_SVN' is missing"
			EXIT=1
		fi
		if [ ! -x $BIN_MSGFMT ] ; then
			$BIN_ECHO "-> Binary '$BIN_MSGFMT' is missing"
			EXIT=1
		fi
	fi
	if [ $EXIT -eq 0 ] ; then
		case "$SVN_LOCATION" in
			trunk)
				SVN_VERSION=""
				SVN_RELEASE=""
				SVN_BRANCH=""
				SVN_PATH_PACKAGE="trunk/component/$SVN_COMPONENT/package"
				SVN_PATH_SOURCE="trunk/component/$SVN_COMPONENT/source"
				SVN_LOG="$TARGET_DIR/$SVN_COMPONENT-trunk.svnlog"
				RPM_LOG="$TARGET_DIR/$SVN_COMPONENT-trunk.rpmlog"
				;;
			tags)
				SVN_BRANCH=""
				SVN_PATH_PACKAGE="tags/component/$SVN_COMPONENT/package/$SVN_VERSION-$SVN_RELEASE"
				SVN_PATH_SOURCE="tags/component/$SVN_COMPONENT/source/$SVN_VERSION"
				SVN_LOG="$TARGET_DIR/$SVN_COMPONENT-$SVN_VERSION-$SVN_RELEASE.svnlog"
				RPM_LOG="$TARGET_DIR/$SVN_COMPONENT-$SVN_VERSION-$SVN_RELEASE.rpmlog"
				;;
			branches)
				SVN_PATH_PACKAGE="branches/component/$SVN_COMPONENT/package/$SVN_VERSION-$SVN_RELEASE-$SVN_BRANCH"
				SVN_PATH_SOURCE="branches/component/$SVN_COMPONENT/source/$SVN_VERSION-$SVN_BRANCH"
				SVN_LOG="$TARGET_DIR/$SVN_COMPONENT-$SVN_VERSION-$SVN_RELEASE-$SVN_BRANCH.svnlog"
				RPM_LOG="$TARGET_DIR/$SVN_COMPONENT-$SVN_VERSION-$SVN_RELEASE-$SVN_BRANCH.rpmlog"
				;;
			*)
				$BIN_ECHO "-> SVN location '$SVN_LOCATION' is not supported"
				EXIT=1
		esac
	fi
	if [ $EXIT -eq 0 ] ; then
		$BIN_RM -f $SVN_LOG
		$BIN_RM -f $RPM_LOG
	fi
	if [ $EXIT -eq 0 ] ; then
		TMP_DIR="$TARGET_DIR/readconfig"
		if [ -e $TMP_DIR ] ; then
			$BIN_ECHO "-> Directory '$TMP_DIR' already exists"
			EXIT=1
		else
                	$BIN_MKDIR -p $TMP_DIR
	                if [ $? -ne 0 ] ; then
        	                $BIN_ECHO "-> Error while creating directory '$TMP_DIR'"
                	        EXIT=1
			fi
                fi
        fi
	if [ $EXIT -eq 0 ] ; then
		RPM_TOP_DIR="$TARGET_DIR/rpmtopdir"
		if [ -e $RPM_TOP_DIR ] ; then
			$BIN_ECHO "-> Directory '$RPM_TOP_DIR' already exist"
			EXIT=1
		else
			BUILD_DIR="$RPM_TOP_DIR/BUILD"
			RPMS_DIR="$RPM_TOP_DIR/RPMS"
			SOURCES_DIR="$RPM_TOP_DIR/SOURCES"
			SPECS_DIR="$RPM_TOP_DIR/SPECS"
			SRPMS_DIR="$RPM_TOP_DIR/SRPMS"
		fi
	fi
	if [ $EXIT -eq 0 ] ; then
		pushd $TMP_DIR >> /dev/null 2>&1
                $BIN_ECHO -e "\n\n\n$BIN_SVN checkout --username $SVN_USER --password $SVN_PASS $SVN_URL/$SVN_PATH_PACKAGE .\n\n\n" >> $SVN_LOG
               	$BIN_SVN checkout --username $SVN_USER --password $SVN_PASS $SVN_URL/$SVN_PATH_PACKAGE . >> $SVN_LOG 2>&1
		if [ $? -ne 0 ] ; then
			$BIN_ECHO "-> Error while checkouting URL '$SVN_URL/$SVN_PATH_PACKAGE'"
			EXIT=1
		fi
                popd >> /dev/null 2>&1
	fi
	if [ $EXIT -eq 0 ] ; then
		if [ -e $TMP_DIR/config ] ; then
			TARGET_OS=""
			USE_EXTRA_VERSION_LEVEL=0
			CREATE_TARBALL=0
			TARBALL_COMPONENT=""
			TARBALL_VERSION=""
			EXCLUDE_FROM_TARBALL=""
			USE_EXTRA_SOURCES=0
			EXTRA_SOURCES_DIRS=""
			RPM_COMPONENT=""
			RPM_VERSION=""
			RPM_RELEASE=""
			RPM_ARCH=""
			RPM_SUBCOMPONENTS=""
			POST_RPM_SCRIPT=""
			. $TMP_DIR/config
			if [ -z "$TARGET_OS" ] ; then
				TARGET_OS=$HOST_OS
			fi
			if [ "$USE_EXTRA_VERSION_LEVEL" != "1" ] ; then
				USE_EXTRA_VERSION_LEVEL=0
			fi
			if [ "$CREATE_TARBALL" != "1" ] ; then
				CREATE_TARBALL=0
			fi
			if [ -z "$TARBALL_COMPONENT" ] ; then
				TARBALL_COMPONENT=$SVN_COMPONENT
			fi
			if [ -z "$TARBALL_VERSION" ] ; then
				TARBALL_VERSION=$SVN_VERSION
			fi
			if [ "$CREATE_TARBALL" = "1" -a -z "$TARBALL_VERSION" ] ; then
				$BIN_ECHO "-> Tarball version is empty"
				EXIT=1
			fi
			if [ "$USE_EXTRA_SOURCES" != "1" ] ; then
				USE_EXTRA_SOURCES=0
			fi
			if [ -z "$RPM_COMPONENT" ] ; then
				RPM_COMPONENT=$SVN_COMPONENT
			fi
			if [ -z "$RPM_VERSION" -a -n "$SVN_VERSION" ] ; then
				if [ $USE_EXTRA_VERSION_LEVEL -eq 1 ] ; then
					invert_string $SVN_VERSION
					RPM_VERSION=`$BIN_ECHO $GNIRTS | $BIN_CUT -f2- -d.`
					invert_string $RPM_VERSION
					RPM_VERSION=$GNIRTS
				else
					RPM_VERSION=$SVN_VERSION
				fi
			fi
			if [ -z "$RPM_RELEASE" -a -n "$SVN_RELEASE" ] ; then
				if [ $USE_EXTRA_VERSION_LEVEL -eq 1 -a -n "$SVN_VERSION" ] ; then
					invert_string $SVN_VERSION
					RPM_RELEASE=`$BIN_ECHO $GNIRTS | $BIN_CUT -f1 -d.`
					invert_string $RPM_RELEASE
					RPM_RELEASE="$GNIRTS.$SVN_RELEASE"
				else
					RPM_RELEASE=$SVN_RELEASE
				fi
			fi
			if [ -n "$RPM_RELEASE" -a "$SVN_COMPONENT" != "getdist" ] ; then
				RPM_RELEASE="$RPM_RELEASE.$HOST_OS"
			fi
			if [ -z "$RPM_ARCH" ] ; then
				if [ -n "$DEFAULT_RPM_ARCH" ] ; then
					RPM_ARCH=$DEFAULT_RPM_ARCH
				else
					RPM_ARCH=`$BIN_RPM --eval %_host_cpu`
					if [ $? -ne 0 ] ; then
						$BIN_ECHO "-> Error while retrieving architecture"
						EXIT=1
					fi
				fi
			fi
			if [ -n "$POST_RPM_SCRIPT" ] ; then
				if [ -e $TMP_DIR/$POST_RPM_SCRIPT ] ; then
					. $TMP_DIR/$POST_RPM_SCRIPT
				else
					$BIN_ECHO "-> Post RPM script '$TMP_DIR/$POST_RPM_SCRIPT' is missing"
					EXIT=1
				fi
			fi
			if [ -z "$TANSLATION_DIR_SOURCE" ] ; then
				$BIN_ECHO "-> Missing parameter : TANSLATION_DIR_SOURCE"
			fi
		else
			$BIN_ECHO "-> File '$TMP_DIR/config' is missing"
			EXIT=1
		fi
		if [ $EXIT -eq 0 ] ; then
			if [ -e ~/.rpmmacros ] ; then
				if [ ! -e ~/.rpmmacros.readconfig ] ; then
					$BIN_MV -f ~/.rpmmacros ~/.rpmmacros.readconfig
					if [ $? -ne 0 ] ; then
						$BIN_ECHO "-> Error while moving '~/.rpmmacros' file to '~/.rpmmacros.readconfig'"
						EXIT=1
					fi
				fi
			fi
		fi
		if [ $EXIT -eq 0 ] ; then
			$BIN_ECHO "%_topdir $RPM_TOP_DIR" > ~/.rpmmacros
			$BIN_MKDIR -p $BUILD_DIR
			$BIN_MKDIR -p $RPMS_DIR/$RPM_ARCH
			$BIN_MKDIR -p $RPMS_DIR/noarch
			$BIN_MKDIR -p $SOURCES_DIR
			$BIN_MKDIR -p $SPECS_DIR
			$BIN_MKDIR -p $SRPMS_DIR
		fi
	fi
	delete_dir $TMP_DIR
	if [ $EXIT -eq 0 ] ; then
		echo "-> OK"
	fi
}

checkbuild()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Checking component '$SVN_COMPONENT'"
		BUILD=0
		if [ -z "$TARGET_OS" ] ; then
			BUILD=1
		else
			for OS in $TARGET_OS ; do
				if [ "$OS" = "$HOST_OS" ] ; then
					BUILD=1
				fi
			done
		fi
		if [ $BUILD -eq 1 ] ; then
			MESSAGE="RPMs need to be built for this OS"
		else
			MESSAGE="No need to build for this OS"
		fi
	fi
	if [ $EXIT -eq 0 -a $BUILD -eq 1 ] ; then
		BUILD=0
		if [ "$SVN_LOCATION" = "trunk" ] ; then
			RPM_VERSION_RELEASE="*-*"
		else
			RPM_VERSION_RELEASE="$RPM_VERSION-$RPM_RELEASE"
		fi
		LIST="`$BIN_LS -1 $TARGET_DIR/$RPM_COMPONENT-$RPM_VERSION_RELEASE.$RPM_ARCH.rpm 2>/dev/null`"
		if [ -z "$LIST" ] ; then
			BUILD=1
		fi
		LIST="`$BIN_LS -1 $TARGET_DIR/$RPM_COMPONENT-$RPM_VERSION_RELEASE.src.rpm 2>/dev/null`"
		if [ -z "$LIST" ] ; then
			BUILD=1
		fi
		if [ -n "$RPM_SUBCOMPONENTS" ] ; then
			for COMPONENT in $RPM_SUBCOMPONENTS ; do
				LIST="`$BIN_LS -1 $TARGET_DIR/$COMPONENT-$RPM_VERSION_RELEASE.$RPM_ARCH.rpm 2>/dev/null`"
				if [ -z "$LIST" ] ; then
					BUILD=1
					break
				fi
			done
		fi
		if [ $BUILD -eq 0 ] ; then
			MESSAGE="RPMs already exist in target directory"
		fi
	fi
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "-> $MESSAGE"
	fi
}

getfiles()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Getting files to build '$SVN_COMPONENT'"
	fi
	if [ $EXIT -eq 0 ] ; then
		TMP_DIR="$TARGET_DIR/getfiles"
		if [ -e $TMP_DIR ] ; then
			$BIN_ECHO "-> Directory '$TMP_DIR' already exists"
			EXIT=1
		else
			$BIN_MKDIR -p $TMP_DIR/package
			if [ $? -ne 0 ] ; then
				$BIN_ECHO "-> Error while creating directory '$TMP_DIR/package'"
				EXIT=1
			fi
		fi
	fi
	if [ $EXIT -eq 0 ] ; then
		pushd $TMP_DIR/package >> /dev/null 2>&1
		$BIN_ECHO -e "\n\n\n$BIN_SVN checkout --username $SVN_USER --password $SVN_PASS $SVN_URL/$SVN_PATH_PACKAGE .\n\n\n" >> $SVN_LOG
		$BIN_SVN checkout --username $SVN_USER --password $SVN_PASS $SVN_URL/$SVN_PATH_PACKAGE . >> $SVN_LOG 2>&1
		if [ $? -ne 0 ] ; then
			$BIN_ECHO "-> Error while checkouting URL '$SVN_URL/SVN_PATH_PACKAGE'"
			EXIT=1
		fi
		popd >> /dev/null 2>&1
	fi
	if [ $EXIT -eq 0 ] ; then
		if [ ! -e $TMP_DIR/package/$RPM_COMPONENT.spec ] ; then
			$BIN_ECHO "-> File '$TMP_DIR/package/$RPM_COMPONENT.spec' is missing"
			EXIT=1
		fi
	fi
	if [ $CREATE_TARBALL -eq 1 -o $USE_EXTRA_SOURCES -eq 1 ] ; then
		if [ $EXIT -eq 0 ] ; then
			$BIN_MKDIR -p $TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION
			if [ $? -ne 0 ] ; then
				$BIN_ECHO "-> Error while creating directory '$TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION'"
				EXIT=1
			fi
		fi
		if [ $EXIT -eq 0 ] ; then
			pushd $TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION >> /dev/null 2>&1
			$BIN_ECHO -e "\n\n\n$BIN_SVN checkout --username $SVN_USER --password $SVN_PASS $SVN_URL/$SVN_PATH_SOURCE .\n\n\n" >> $SVN_LOG
			$BIN_SVN checkout --username $SVN_USER --password $SVN_PASS $SVN_URL/$SVN_PATH_SOURCE . >> $SVN_LOG 2>&1
			if [ $? -ne 0 ] ; then
				$BIN_ECHO "-> Error while checkouting URL '$SVN_URL/$SVN_PATH_SOURCE'"
				EXIT=1
			fi
			if [ $EXIT -eq 0 ] ; then
				build_mo $SVN_COMPONENT $TANSLATION_DIR_SOURCE
			fi
			popd >> /dev/null 2>&1
		fi
	fi
	if [ $EXIT -eq 0 -a $CREATE_TARBALL -eq 1 ] ; then
		EXCLUDES="--exclude=.svn"
		if [ -n "$EXCLUDE_FROM_TARBALL" ] ; then
			for EXCLUDE in $EXCLUDE_FROM_TARBALL ; do
				EXCLUDES="$EXCLUDES --exclude=$EXCLUDE"
			done
		fi
		pushd $TMP_DIR >> /dev/null 2>&1
		$BIN_TAR -czf $SOURCES_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION.tar.gz $EXCLUDES $TARBALL_COMPONENT-$TARBALL_VERSION
		if [ $? -ne 0 ] ; then
			$BIN_ECHO "-> Error while creating tarball '$SOURCES_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION.tar.gz'"
			EXIT=1
		fi
		popd >> /dev/null 2>&1
	fi
	if [ $EXIT -eq 0 ] ; then
		$BIN_CP -af $TMP_DIR/package/$RPM_COMPONENT.spec $SPECS_DIR/
		if [ $? -ne 0 ] ; then
			$BIN_ECHO "-> Error while copying file '$TMP_DIR/package/$RPM_COMPONENT.spec' to directory '$SPECS_DIR'"
			EXIT=1
		fi
	fi
	if [ $EXIT -eq 0 -a $USE_EXTRA_SOURCES -eq 1 ] ; then
		for DIR in $EXTRA_SOURCES_DIRS ; do
			if [ -d $TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION/$DIR ] ; then
				$BIN_CP -a $TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION/$DIR/* $SOURCES_DIR/
				if [ $? -ne 0 ] ; then
					$BIN_ECHO "-> Error while copying files '$TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION/$DIR/*' to directory '$SOURCES_DIR'"
					EXIT=1
					break
				fi
			else
				$BIN_ECHO "-> Extra source directory '$TMP_DIR/$TARBALL_COMPONENT-$TARBALL_VERSION/$DIR' is missing"
				EXIT=1
				break
			fi
		done
	fi
	delete_dir $TMP_DIR
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "-> OK"
	fi
}

uninstallrpms()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Uninstalling '$SVN_COMPONENT'"
		LIST=""
		$BIN_ECHO -e "\n\n\n$BIN_RPM -q $RPM_COMPONENT\n\n\n" >> $RPM_LOG
		$BIN_RPM -q $RPM_COMPONENT >> $RPM_LOG 2>&1
		if [ $? -eq 0 ] ; then
			LIST="$LIST $RPM_COMPONENT"
		fi
		if [ -n "$RPM_SUBCOMPONENTS" ] ; then
			for COMPONENT in $RPM_SUBCOMPONENTS ; do
				$BIN_RPM -q $COMPONENT >> $RPM_LOG 2>&1
				if [ $? -eq 0 ] ; then
					LIST="$LIST $COMPONENT"
				fi
			done
		fi
		if [ -n "$LIST" ] ; then
			$BIN_ECHO -e "\n\n\n$BIN_RPM -e$LIST\n\n\n" >> $RPM_LOG
			$BIN_RPM -e$LIST >> $RPM_LOG 2>&1
			if [ $? -eq 0 ] ; then
				$BIN_ECHO "-> OK"
			else
				$BIN_ECHO "-> Error while uninstalling"
				EXIT=1
			fi
		else
			$BIN_ECHO "-> Nothing to uninstall"
		fi
	fi
}

buildrpms()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Compiling '$SVN_COMPONENT'"
	fi
	if [ $EXIT -eq 0 ] ; then
		BUILD=1
		if [ -e $SPECS_DIR/$RPM_COMPONENT.spec ] ; then
			$BIN_ECHO -e "\n\n\n$BIN_RPMBUILD -ba --target=$RPM_ARCH-redhat-linux $SPECS_DIR/$RPM_COMPONENT.spec\n\n\n" >> $RPM_LOG
			$BIN_RPMBUILD -ba --target=$RPM_ARCH-redhat-linux $SPECS_DIR/$RPM_COMPONENT.spec >> $RPM_LOG 2>&1
			if [ $? -ne 0 ] ; then
				$BIN_ECHO "-> Error while compiling"
				EXIT=1
			else
				$BIN_ECHO "-> OK"
			fi
		else
			$BIN_ECHO "-> File '$SPECS_DIR/$RPM_COMPONENT.spec' is missing"
			EXIT=1
		fi
	fi
}

installrpms()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Installing '$SVN_COMPONENT'"
		$BIN_ECHO -e "\n\n\n$BIN_RPM -i $RPMS_DIR/$RPM_ARCH/$RPM_COMPONENT-*-*.$RPM_ARCH.rpm\n\n\n" >> $RPM_LOG
		$BIN_RPM -i $RPMS_DIR/$RPM_ARCH/$RPM_COMPONENT-*-*.$RPM_ARCH.rpm >> $RPM_LOG 2>&1
		if [ $? -eq 0 ] ; then
			$BIN_ECHO "-> OK"
		else
			$BIN_ECHO "-> Error while installing"
			EXIT=1
		fi
	fi
}

postrpmscript()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Executing post RPM script for '$SVN_COMPONENT'"
		if [ -n "$POST_RPM_SCRIPT" ] ; then
			postrpmbuild
		else
			$BIN_ECHO "-> No script to execute"
		fi
	fi
}

copyrpms()
{
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Copying '$SVN_COMPONENT' files to target directory"
	fi
	if [ $EXIT -eq 0 ] ; then
		$BIN_CP -af $RPMS_DIR/$RPM_ARCH/* $TARGET_DIR/
		if [ $? -ne 0 ] ; then
			$BIN_ECHO "-> Error while copying file(s) '$RPMS_DIR/$RPM_ARCH/*' to directory '$TARGET_DIR'"
			EXIT=1
		fi
	fi
	if [ $EXIT -eq 0 ] ; then
		$BIN_CP -af $SRPMS_DIR/* $TARGET_DIR/
		if [ $? -ne 0 ] ; then
			$BIN_ECHO "-> Error while copying file '$SRPMS_DIR/*' to directory '$TARGET_DIR'"
			EXIT=1
		fi
	fi
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "-> OK"
	fi
}

build_mo()
# $1: Domain Name (plugin name)
# $2: *.po translation files directory
{
	DOMAINE_NAME=$1
	SOURCE_PO=$2
	TARGET_MO=locale
	
	if [ $EXIT -eq 0 ] ; then
		$BIN_ECHO "Generating *.mo translation Files for $DOMAINE_NAME (in `pwd`)" 
		
		if [[ ( -e "$SOURCE_PO" ) &&  ! ( -d "$SOURCE_PO" ) ]] ; then
			$BIN_ECHO "-> Source '$SOURCE_PO' must be a directory"
			EXIT=1
		fi
		
		
		if [ -e "$TARGET_MO" ] ; then
			$BIN_ECHO "-> Target '$TARGET_MO' must not exist"
			EXIT=1
		fi
	fi
		
	if [ $EXIT -eq 0 ] ; then
		if [ ! -e "$SOURCE_PO" ] ; then
			$BIN_ECHO "No translations provided"
		else
			pushd $SOURCE_PO >> /dev/null 2>&1
			languages=$(ls *.po|cut -f1 -d.)
			popd >> /dev/null 2>&1
			
			for lang in $languages; do
				poSourceFile=$SOURCE_PO/$lang.po
				moTargetDir=$TARGET_MO/$lang/LC_MESSAGES
				moTargetFile=$moTargetDir/$DOMAINE_NAME.mo
				
				$BIN_MKDIR -p $moTargetDir
				$BIN_MSGFMT $poSourceFile -o $moTargetFile
				$BIN_ECHO "translations  $lang ($poSourceFile) installed: to $moTargetFile"
			done
		fi
	fi
}

#
# Main script
#
readconfig $1 $2 $3 $4 $5 $6
checkbuild
if [ $BUILD -eq 1 ] ; then
	getfiles
	if [ "$SVN_COMPONENT" = "subversion" ] ; then
		uninstallrpms
	fi
	buildrpms
	if [ "$SVN_COMPONENT" = "subversion" ] ; then
		installrpms
	fi
	copyrpms
	postrpmscript
fi
$BIN_RM -f /etc/rpm/macros.gforge
if [ -e ~/.rpmmacros.readconfig ] ; then
	$BIN_MV -f ~/.rpmmacros.readconfig ~/.rpmmacros
	if [ $? -ne 0 ] ; then
		$BIN_ECHO "-> Error while restoring file '~/.rpmmacros' from '~/.rpmmacros.readconfig'"
	fi
fi
delete_dir $RPM_TOP_DIR
if [ $EXIT -ne 0 ] ; then
	$BIN_ECHO ""
	$BIN_ECHO "****************************************"
	$BIN_ECHO "Build of component '$SVN_COMPONENT' failed !"
	$BIN_ECHO "SVN log: $SVN_LOG"
	$BIN_ECHO "RPM log: $RPM_LOG"
	$BIN_ECHO "****************************************"
fi
exit $EXIT
