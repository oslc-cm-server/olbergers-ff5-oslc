#DSFHELPER:common-variables#
#DSFHELPER:shellhost-variables#
#DSFHELPER:users-variables#
#DSFHELPER:lists-variables#
#DSFHELPER:downloadhost-variables#
#DSFHELPER:host-variables#
#DSFHELPER:web-variables#

Template: gforge/shared/db_password
Type: password
_Description: Password used for the database
 The connection to the DB system requires a password.  Please choose a
 password here.

Template: gforge/shared/db_password_confirm
Type: password
_Description: Password used for the database - again
 Please re-type the password for confirmation.

Template: gforge/shared/admin_login
Type: string
_Default: admin
_Description: The Gforge administrator login
 This Gforge account will have all privileges on the Gforge
 system.  It is needed to approve the creation of the projects.

Template: gforge/shared/admin_password
Type: password
_Description: The Gforge administrator password
 This Gforge account will have all privileges on the Gforge
 system.  It is needed to approve the creation of the projects.  Please
 type a password here.

Template: gforge/shared/admin_password_confirm
Type: password
_Description: The Gforge administrator password - again
 Please re-type the password for confirmation.

Template: gforge/shared/skill_list
Type: string
Default: Ada;C;C++;HTML;LISP;Perl;PHP;Python;SQL
_Description: The initial list of skills
 Gforge allows users to define a list of their skills, to be chosen
 amongst those present in the database.  This list is the initial list of
 skills that will enter the database.  Please enter the skill names
 separated by semi-colons `;'.

Template: gforge/shared/ldap_base_dn
Type: string
_Description: The LDAP base DN
 The DN is used to refer to the LDAP directory unambiguously.  You could use,
 for instance, "dc=gforge,dc=example,dc=com"

Template: gforge/shared/ldap_host
Type: string
_Description: The LDAP host
 The hostname of the LDAP server.

Template: gforge/shared/noreply_to_bitbucket
Type: boolean
Default: true
_Description: Do you want mail to ${noreply} to be deleted?
 Gforge sends plenty of e-mail from the "${noreply}" address,
 and maybe even some e-mail to that address too.
 .
 It is advised that you let the package direct e-mail to that address to a
 black hole (/dev/null), unless you have another use for that address.
 .
 Accepting this option will perform that redirection.

Template: gforge/shared/ldap_web_add_password
Type: password
_Description: LDAP password used to add users from the web
 In order to add users into the LDAP directory from the web, you need to
 provide a password.

Template: gforge/shared/ldap_web_add_password_confirm
Type: password
_Description: LDAP password - again
 Please re-type the password for confirmation.

Template: gforge/shared/mod_ssl_cert
Type: note
_Description: Generate an SSL certificate
 You need a valid SSL/TLS certificate to run Gforge.
 If you don't already have one, you'll have to run the
 "mod-ssl-makecert" command to generate one, and restart Apache
 afterwards.
 .
 Please note you will not be able to start Apache if you have no
 such certificate.

Template: gforge/shared/pam_ldap_config
Type: note
_Description: Gforge requires appropriate PAM-LDAP configuration
 Gforge requires the libpam-ldap package to be configured
 appropriately.  You might need to "dpkg-reconfigure libpam-ldap" in
 order to fix incorrect parameters.
 .
 Known good values are: LDAP protocol version=3, make root database
 admin=yes, root login account="cn=admin,dc=<your-dc-here>", crypt to
 use for passwords=crypt.  Make sure that you type the same password
 for the libpam-ldap root password and the Gforge LDAP password.

Template: gforge/shared/simple_dns
Type: boolean
Default: false
_Description: Do you want a simple DNS setup?
 You can have a simple DNS setup wich uses wildcards to map all
 project web-hosts to a single IP, and direct all the scm-hosts
 to a single SCM server, or a complex setup wich allows you to
 have many servers as project web servers or SCM servers.
 .
 Even though you have a simple DNS setup, you can still have
 separate boxes for your project servers, it just assumes you
 have all the project web dirs on the same server and a single
 server for SCM.

Template: gforge/shared/replace_file_remove
Type: boolean
Default: false
_Description: Do you want ${file} to be updated?
 In order for Gforge to be fully deinstalled, some changes must
 be made to the ${file} file.
 .
 I can either do them for you (and backup the current version in
 ${file}.gforge-old), or write the changes to another file
 (${file}.gforge-new) and let you propagate the changes to
 ${file} yourself.
 .
 Shall I modify the ${file} automatically?

Template: gforge/shared/file_changed
Type: note
_Description: Please check ${file}
 You have chosen not to let the Gforge package update the
 ${file} file.  Very well, I have not changed it.  Instead,
 I have written the suggested changes into ${file}.gforge-new.
 Please check this file and apply the appropriate changes
 to ${file}.
