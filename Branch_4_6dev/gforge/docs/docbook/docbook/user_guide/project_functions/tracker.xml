<section id="ug_project_tracker">
	<title>Tracker</title>
	<section>
		<title>What is the Tracker?</title>
		<para>
			The Tracker is a generic system where you can store items like bugs, feature requests, patch submissions, etc.
		</para>
		<para>
			In previous versions of the software, these items were handled in separate software modules. Bugs, Enhancement Requests, Support Requests and Patches handle the same type of data, so it was logical to create an unique software module that can handle these types of data. New types of trackers can be created when needed, e.g. Test Results, meeting minutes, etc.
		</para>
		<para>
			You can use this system to track virtually any kind of data, with each tracker having separate user, group, category, and permission lists. You can also easily move items between trackers when needed.
		</para>
		<para>
			Trackers are referred to as <quote>Artifact Types</quote> and individual pieces of data are <quote>Artifacts</quote>. <quote>Bugs</quote> might be an Artifact Type, while a bug report would be an Artifact. You can create as many Artifact Types as you want, but remember you need to set up categories, groups, and permission for each type, which can get time-consuming.
		</para>
		<para>
			When a project is created, GForge creates automatically 4 trackers:
		</para>
		<variablelist>
			<varlistentry>
				<term>Bugs</term>
				<listitem>
					<para>Used for Bug tracking</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Support Requests</term>
				<listitem>
					<para>Users can insert here support requests and receive support</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Patches</term>
				<listitem>
					<para>Developers can upload here patches to the software</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Feature Requests</term>
				<listitem>
					<para>Requests for enhancements of the software should be posted here</para>
				</listitem>
			</varlistentry>
		</variablelist>
	</section>
	<section>
		<title>Using a Tracker</title>
		<para>
			The following descriptions can be applied to any of the trackers. The functionalities between the different trackers are the same, we'll use the Bugs Tracker as example to describe the functionality of all trackers.
		</para>
		<para>
			The Tracker provides the following functions:
		</para>
		<orderedlist>
			<listitem><para>Submitting a new item</para></listitem>
			<listitem><para>Browsing of Items</para></listitem>
			<listitem><para>Reporting</para></listitem>
			<listitem><para>Administration</para></listitem>
		</orderedlist>
	</section>
	<section>
		<title>Submitting a new Bug</title>
		<para>
			To submit a new bug, click on the <guimenuitem>Submit New</guimenuitem> link. A form will be displayed, where you can insert/select the following data:
		</para>
		<variablelist>
			<varlistentry>
				<term>Category</term>
				<listitem>
					<para>The Category is generally used to describe the function/module in which the bug appears. E.g for GForge, this might be the items <quote>User Login</quote>, <quote>File releases</quote>, <quote>Forums</quote>, <quote>Tracker</quote>, etc.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Group</term>
				<listitem>
					<para>The Category can be used to describe the version of the software or the gravity of the bug. E.g <quote>3.0pre7</quote>, <quote>3.0pre8</quote> in case of version or <quote>Fatal error</quote>, <quote>Non-fatal error</quote> in case of gravity.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Assigned To</term>
				<listitem>
					<para>You can assign the item to a user. Only users which are <quote>Technicians</quote> are listed here.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Priority</term>
				<listitem>
					<para>You can select the Priority of the item. In the Browse list, and the homepage of the users, priorities are displayed in different colors, and can be ordered by priority.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Summary</term>
				<listitem>
					<para>Give a short description of the bug, e.g. Logout function gives an SQL Error</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Detailed Description</term>
				<listitem>
					<para>Insert the most detailed description possible.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>File upload</term>
				<listitem>
					<para>You can also upload a file as an attachment to the bug. This can be used to attach a screenshot with the error and the log file of the application.</para>
					<para>To upload the file, Check the checkbox, select a file using the Browse button and insert a file description.</para>
					<note>
						<para>Attachments to tracker items can be no larger than 256KB.</para>
					</note>
				</listitem>
			</varlistentry>
		</variablelist>
	</section>
	<section>
		<title>Browse Bugs</title>
		<para>
			The Browse page shows the list of bugs. You can select to filter the bugs by Assignee, Status, Category or Group.
		</para>
		<para>
			You can sort the items by ID, Priority, Summary, Open Date, Close Date, Submittere, Assignee and the Ordering (Ascending, descending).
		</para>
		<para>
			The different colors indicate the different priorities of the bug; a * near the open date indicates that the request is more than 30 days old. The overdue time (default 30 days) is configurable for each tracker.
		</para>
		<para>
			When you click on the summary, you go to the detail/modify Bug page.
		</para>
	</section>
	<section>
		<title>Modify Bugs</title>
		<para>
			In the modify Bug page, you can modify the data you inserted, and also add the following information:
		</para>
		<variablelist>
			<varlistentry>
				<term>Data Type</term>
				<listitem>
					<para>This combo box lists the trackers of the project. If you select a different tracker and submit the changes, the item will be reassigned to the selected tracker.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Status</term>
				<listitem>
					<para>The status indicates the status of the item. When an item is inserted, it is created in the <quote>Open</quote> state. When you fix a bug, you should change the state to <quote>Closed</quote>. When a bug is duplicated or not valid, change it to <quote>Deleted</quote>.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Resolution</term>
				<listitem>
					<para>This indicates the resolution of the item.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Canned Responses</term>
				<listitem>
					<para>Canned responses are prefixed responses. You can create canned responses for your project in the admin section and select the responses in the combo box.</para>
				</listitem>
			</varlistentry>
		</variablelist>
		<para>
			The Changelog on the bottom of the page shows in chronological order the changes applied to the item. Also all followups can be viewed.
		</para>
	</section>
	<section>
		<title>Monitor Bugs</title>
		<para>
			If you select the <guibutton>Monitor</guibutton> button on the top left of the Bug detail page, bug monitoring will be enabled.
		</para>
		<para>
			When you are monitoring a bug, every change to the bug will be sent to you by email.
		</para>
		<para>
			To disable bug monitoring, simply reselect the <guibutton>Monitor</guibutton> button.
		</para>
	</section>
	<section>
		<title>Admin Tracker</title>
		<para>
			If you are an Administrator of the tracker, you can add or change bug groups, categories, canned responses:
		</para>
		<variablelist>
			<varlistentry>
				<term>Add/Update Categories</term>
				<listitem>
					<para>You can add new categories or change the name of existing categories.</para>
					<para>You can also select a user in the Auto-Assign To combo box; every bug with this category will be auto-assigned to the selected user. This feature can save you lots of time when administering the tracker.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Add/Update Groups</term>
				<listitem>
					<para>You can add new groups or change the name of existing groups.It is not recommended that you change the  group name because other things are dependent upon it. When you change the group name, all related items will be changed to the new name.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Add Update Canned Responses</term>
				<listitem>
					<para>Canned responses are predefined responses. Creating useful generic messages can save you a lot of time when handling common requests.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Add Update Users and Permissions</term>
				<listitem>
					<para>You can add new users to the tracker or delete users from the tracker.</para>
					<variablelist>
						<varlistentry>
							<term>-</term>
							<listitem>
								<para>The user has no specific permission on the tracker; he cannot administer the tracker, no items can be assigned to the user.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Technician</term>
							<listitem>
								<para>Items can be assigned to the user.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Administrator and Technician</term>
							<listitem>
								<para>The user is both an Administrator and also a Technician.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Administrator</term>
							<listitem>
								<para>User can administer the tracker (add user, set permissions, create/update groups, categories, canned responses).</para>
							</listitem>
						</varlistentry>
					</variablelist>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Update preferences</term>
				<listitem>
					<para>Here you can update the following information on the tracker:</para>
					<variablelist>
						<varlistentry>
							<term>Name</term>
							<listitem>
								<para>The name of the Tracker. This is the name displayed in the tracker list, e.g. Bug Submissions.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Description</term>
							<listitem>
								<para>The description of the Tracker. E.g. This is the tracker dedicated to the Bugs of the project</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Publicly Available</term>
							<listitem>
								<para>By default, this checkbox is not enabled.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Allow non-logged-in postings</term>
							<listitem>
								<para>If this checkbox is enabled, also non logged-in users can post items to the tracker. If this checkbox is not enabled, only logged in users can post items.</para>
								<para>By default, this checkbox is not enabled.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Display the <quote>Resolution</quote> box</term>
							<listitem>
								<para>By default, this checkbox is not enabled.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Send email on new submission to address</term>
							<listitem>
								<para>All new items will be sent to the address inserted in the text box.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Send email on all changes</term>
							<listitem>
								<para>If this checkbox is enabled, all changes on the items will be sent out via email. It is useful to check this radiobutton only if in the Send email address is inserted an email address.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Days still considered overdue</term>
							<listitem>
								<para></para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Days till pending tracker items time out</term>
							<listitem>
								<para></para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Free form text for the <guimenuitem>Submit new item</guimenuitem> page</term>
							<listitem>
								<para>This allows you to put a specific introduction on the <guimenuitem>submit new item</guimenuitem> page.</para>
							</listitem>
						</varlistentry>
						<varlistentry>
							<term>Free form text for the <guimenuitem>Browse items</guimenuitem> page</term>
							<listitem>
								<para>This allows you to put a specific introduction on the <guimenuitem>Browse items</guimenuitem> page.</para>
							</listitem>
						</varlistentry>
					</variablelist>
				</listitem>
			</varlistentry>
		</variablelist>
	</section>
	<section>
		<title>Mass Update</title>
		<para>
			If you are an Administrator of the tracker, you are also enabled for the Mass Update function.
		</para>
		<para>
			This function is visible in the browse bug page and allows you to update the following information:
		</para>
		<orderedlist>
			<listitem><para>Category</para></listitem>
			<listitem><para>Group</para></listitem>
			<listitem><para>Priority</para></listitem>
			<listitem><para>Resolution</para></listitem>
			<listitem><para>Assignee</para></listitem>
			<listitem><para>Status</para></listitem>
			<listitem><para>Canned Response</para></listitem>
		</orderedlist>
		<para>
			When this function is enabled, a checkbox will appear at the left side of each bug id. You can check one or more of the ids, select one or more of the values in the Mass Update combo boxes and click Mass Update.
		</para>
		<para>
			All selected bugs will be modified with these new value(s). This function is very useful if you need to change the same information for more bugs; e.g. assigning 5 bugs to one developer or closing 10 bugs.
		</para>
	</section>
	<section>
		<title>Reporting</title>
		<para> 
			The reporting functions allows to check the life-span of the Bug. The lifespan is the duration of the bug; it starts when the bug is inserted (opened) in the tracker and ends when the bug is closed.
		</para>
		<variablelist>
			<varlistentry>
				<term>Aging Report</term>
				<listitem>
					<para>The Aging report shows the turnaround time for closed bugs, the number of bugs inserted and the number of bugs still open.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Bugs by Technician</term>
				<listitem>
					<para>The Bugs by Technician report shows for every member of the project: the number of bugs assigned to the user, the number of closed bugs and the number of bugs still open.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Bugs by Category</term>
				<listitem>
					<para>The Bugs by Category report shows for every Category: the number of bugs inserted, the number of closed and the number of open bugs</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Bugs by Group</term>
				<listitem>
					<para>The Bugs by Group report shows for every Group: the number of bugs inserted, the number of closed and the number of open bugs.</para>
				</listitem>
			</varlistentry>
			<varlistentry>
				<term>Bugs by Resolution</term>
				<listitem>
					<para>The Bugs by Resolution report shows for every type of Resolution (Fixed, invalid, later, etc): the number of bugs inserted, the number of closed and the number of open bugs.</para>
				</listitem>
			</varlistentry>
		</variablelist>
	</section>
	<section>
		<title>Searching for bugs</title>
		<para>
			When using a tracker, a voice with the name of the tracker will appear in the search combo box. The search will be done on the description, the summary, the username of the submitter and the username of the assignee.
		</para>
	</section>
</section>
