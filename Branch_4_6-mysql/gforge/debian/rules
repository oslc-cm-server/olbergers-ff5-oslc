#!/usr/bin/make -f
# debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess (sample file)
# Copyright 2000 to 2002 by Roland Mas and Christian Bayle for the Sourceforge package
# Copyright 2002 to 2003 by Roland Mas and Christian Bayle for the Gforge package

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This is the debhelper compatability version to use.
export DH_COMPAT=4

PACKAGE=gforge
include /usr/share/dpatch/dpatch.make

remove-binary-files:
	# Remove files in contrib, especially binaries that break dpkg-source
#	find contrib/ -name CVS -prune -not -name CVS -or -type f | xargs rm -f
	# Also a few images
#	rm -f www/themes/gforge/images/es_*
	sh $(CURDIR)/deb-specific/manage-uufiles.sh clean

configure: configure-stamp debian/po/templates.pot
configure-stamp: remove-binary-files
	dh_testdir

	touch configure-stamp

debian/po/templates.pot: $(wildcard debian/*.templates) $(wildcard debian/*.templates.dsfh-in) $(wildcard debian/dsf-helper/*.templates)
	@debconf-updatepo

build: configure-stamp build-stamp
build-stamp: patch
	dh_testdir

	# Build man pages from Docbook sources
	/usr/bin/docbook-to-man debian/cvssh.sgml > cvssh.1
	/usr/bin/docbook-to-man debian/gforge-config.sgml > gforge-config.1

	# Uudecode binary files
	sh $(CURDIR)/deb-specific/manage-uufiles.sh decode

	touch build-stamp

clean: remove-binary-files unpatch
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp
	rm -f cvssh.1 gforge-config.1

	perl $(CURDIR)/deb-specific/dsf-helper.pl --clean
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	# Preprocess files with DSF-Helper
	perl $(CURDIR)/deb-specific/dsf-helper.pl

	# gforge
	# (gforge is a meta-package and needs no files)
	
	# gforge-plugins
	cp -r plugins $(CURDIR)/debian/gforge-plugins/usr/share/gforge/
	cp -r $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/*/etc/* $(CURDIR)/debian/gforge-plugins/usr/share/gforge/etc/
	rm -rf $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/*/etc/*
	rm -rf $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/*/debian
	ln -s /var/lib/gforge/www/plugins $(CURDIR)/debian/gforge-plugins/usr/share/gforge/www
	chown www-data:www-data $(CURDIR)/debian/gforge-plugins/var/lib/gforge/www/plugins
	chmod 755 $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/*/sbin/*
	chmod 755 $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/*/bin/*
	# mediawiki plugin
	cp -r $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/mediawiki/usr $(CURDIR)/debian/gforge-plugins/
	rm -rf $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/mediawiki/usr
	mv $(CURDIR)/debian/gforge-plugins/usr/share/mediawiki1.7/LocalLdapSettings.php \
	$(CURDIR)/debian/gforge-plugins/usr/share/mediawiki1.7/LocalLdapSettings.php.example
	mv $(CURDIR)/debian/gforge-plugins/usr/share/mediawiki1.7/LocalSettings.php \
	$(CURDIR)/debian/gforge-plugins/usr/share/mediawiki1.7/LocalSettings.php.gforge_example
	mv $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/mediawiki/README $(CURDIR)/debian/gforge-plugins/usr/share/doc/gforge-plugins/README.mediawiki
	# scmcvs plugin
	install -m 755 deb-specific/install-cvs.sh     $(CURDIR)/debian/gforge-plugins/usr/lib/gforge/bin/
	cp -r deb-specific/scmcvs/cronjobs/* $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/scmcvs/cronjobs
	# scmsvn plugin
	install -m 755 deb-specific/install-svn.sh     $(CURDIR)/debian/gforge-plugins/usr/lib/gforge/bin/
	cp -r deb-specific/scmsvn/cronjobs/* $(CURDIR)/debian/gforge-plugins/usr/share/gforge/plugins/scmsvn/cronjobs

	# gforge-common
	install -m 644 etc/httpd.d/[0-9][0-9]*            $(CURDIR)/debian/gforge-common/usr/share/gforge/etc/httpd.d/
	install -m 644 etc/httpd.d/httpd*                 $(CURDIR)/debian/gforge-common/usr/share/gforge/etc/httpd.d/
	install -m 644 etc/local.d/[0-9][0-9]*            $(CURDIR)/debian/gforge-common/usr/share/gforge/etc/local.d/
	install -m 644 etc/templates/*\.*                 $(CURDIR)/debian/gforge-common/usr/share/gforge/etc/templates/
	install -m 755 setup                               $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	install -m 755 deb-specific/cvssh.pl               $(CURDIR)/debian/gforge-common/bin/cvssh
	#
	install -m 755 deb-specific/install-chroot.sh     $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	install -m 755 deb-specific/install-ssh.sh        $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	install -m 755 utils/fill-in-the-blanks.pl        $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	install -m 755 deb-specific/gforge-config         $(CURDIR)/debian/gforge-common/usr/sbin/
	install -m 644 utils/include.pl                   $(CURDIR)/debian/gforge-common/usr/lib/gforge/lib/
	#
	install -m 755 deb-specific/user_dump_update.pl   $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	install -m 755 deb-specific/group_dump_update.pl  $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
#	install -m 644 cronjobs/cvs-cron/default_page.php $(CURDIR)/debian/gforge-common/usr/lib/gforge/lib/
	install -m 755 deb-specific/ssh_dump_update.pl    $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	#
	install -m 755 deb-specific/update-user-group-cvs.sh $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	#
	cp -r common $(CURDIR)/debian/gforge-common/usr/share/gforge/
	find $(CURDIR)/debian/gforge-common/usr/share/gforge/ -name CVS -type d | xargs rm -rf
	cp -r common $(CURDIR)/debian/gforge-common/usr/share/gforge/
	find $(CURDIR)/debian/gforge-common/usr/share/gforge/common -type d -exec chmod 0755 {} \;
	find $(CURDIR)/debian/gforge-common/usr/share/gforge/common -type f -exec chmod 0644 {} \;

	# gforge-web-apache
	install -m 644 $(CURDIR)/debian/gforge-web-apache.overrides $(CURDIR)/debian/gforge-web-apache/usr/share/lintian/overrides/gforge-web-apache
	cp -r www $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/
	install -m 644 deb-specific/index_std.php $(CURDIR)/debian/gforge-web-apache/etc/gforge/custom/
	rm $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/index_std.php
	find $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/ -name CVS -type d | xargs rm -rf
	find $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/ -name .cvsignore | xargs rm -f
	find $(CURDIR)/debian/gforge-web-apache -name \*.uu -type f | xargs rm -f
	find $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www -type d -exec chmod 0755 {} \;
	find $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www -type f -exec chmod 0644 {} \;
	find $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www -name '*.py' -exec chmod 0755 {} \;
	# Remove empty theme directories
	for i in $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/themes/* ; do [ -d $$i ] && [ $$(find $$i -type f | wc -l) = 0 ] && rm -rf $$i || true ; done

	install -o www-data -m 755 -d $(CURDIR)/debian/gforge-web-apache/var/cache/gforge
	install -m 755 utils/install-apache.sh $(CURDIR)/debian/gforge-web-apache/usr/lib/gforge/bin/
	install -m 755 deb-specific/create-vhosts.sh $(CURDIR)/debian/gforge-web-apache/usr/lib/gforge/bin/
	install -m 755 deb-specific/prepare-vhosts-file.pl $(CURDIR)/debian/gforge-web-apache/usr/lib/gforge/bin/

	

	# viewvc rights
	find $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/scm/viewvc -type f -exec chmod 0644 {} \;
	grep -ri '^#!' $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/scm/viewvc | grep -v '\.svn' | cut -d: -f1 | xargs chmod 0755

	# dojo toolkit
	#chmod 0755 $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/dojo/src/storage/storage.sh

	# move some files from gforge-web-apache to gforge-common as there are needed by postgress upgrade
	# these files should be moved one day in common/

	mv $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/include/languages \
		$(CURDIR)/debian/gforge-common/usr/share/gforge/www/include
	mv $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/include/squal_pre.php \
		$(CURDIR)/debian/gforge-common/usr/share/gforge/www/include
	mv $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/include/squal_exit.php \
		$(CURDIR)/debian/gforge-common/usr/share/gforge/www/include
	mv $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/include/pre.php \
		$(CURDIR)/debian/gforge-common/usr/share/gforge/www/include
	mv $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/include/BaseLanguage.class \
		$(CURDIR)/debian/gforge-common/usr/share/gforge/www/include
	mv $(CURDIR)/debian/gforge-web-apache/usr/share/gforge/www/include/vote_function.php \
		$(CURDIR)/debian/gforge-common/usr/share/gforge/www/include

	# gforge-web-apache2
	cp -r $(CURDIR)/debian/gforge-web-apache/* $(CURDIR)/debian/gforge-web-apache2
	install -m 644 deb-specific/index_std.php $(CURDIR)/debian/gforge-web-apache2/etc/gforge/custom/

	# gforge-db-postgresql
	install -m 644 $(CURDIR)/debian/gforge-db-postgresql.overrides $(CURDIR)/debian/gforge-db-postgresql/usr/share/lintian/overrides/gforge-db-postgresql
	install -m 644 deb-specific/sf-2.6-complete.sql $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 deb-specific/sf2.5-to-sf2.6.sql $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	#
	install -m 755 cronjobs/calculate_user_metric.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/db_trove_maint.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/project_cleanup.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/rating_stats.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/db_project_sums.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/check_stale_tracker_items.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/project_weekly_metric.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/rotate_activity.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	#
	install -m 755 cronjobs/site_stats.php        $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 644 cronjobs/stats_site.inc        $(CURDIR)/debian/gforge-db-postgresql/usr/share/gforge/cronjobs/
	install -m 644 cronjobs/stats_projects.inc    $(CURDIR)/debian/gforge-db-postgresql/usr/share/gforge/cronjobs/
	#
	install -m 755 cronjobs/db_stats_agg.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/massmail.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 cronjobs/vacuum.php $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 deb-specific/stats_projects_logparse.pl $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 deb-specific/get_news_notapproved.pl $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 deb-specific/install-db.sh $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 deb-specific/db-upgrade.pl $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 644 deb-specific/sqlparser.pm $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/lib/
	install -m 644 deb-specific/sqlhelper.pm $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/lib/
	install -m 755 deb-specific/sf-add-skill $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 deb-specific/register-theme $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/
	install -m 755 deb-specific/unregister-theme $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/bin/

	install -m 755 deb-specific/register-plugin $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	install -m 755 deb-specific/unregister-plugin $(CURDIR)/debian/gforge-common/usr/lib/gforge/bin/
	#
	install -m 644 deb-specific/view_bug.sql      $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 deb-specific/view_patch.sql    $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	#
	install -m 644 db/20021125.sql $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 db/200212*.sql  $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 db/2003*.sql    $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 755 db/2004*php     $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 db/2004*.sql    $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 755 db/2005*php     $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 db/2005*.sql    $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
	install -m 644 db/2006*.sql    $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/
#	install -m 644 db/timetracking-init.sql    $(CURDIR)/debian/gforge-db-postgresql/usr/lib/gforge/db/

	# gforge-mta-exim
	install -m 755 deb-specific/install-exim.sh $(CURDIR)/debian/gforge-mta-exim/usr/lib/gforge/bin/

	# gforge-mta-exim4
	install -m 755 deb-specific/install-exim4.sh $(CURDIR)/debian/gforge-mta-exim4/usr/lib/gforge/bin/

	# gforge-mta-postfix
	install -m 755 deb-specific/install-postfix.sh $(CURDIR)/debian/gforge-mta-postfix/usr/lib/gforge/bin/

	# gforge-shell-ldap
	# not needed chroot is a symlink -> ../../../
	#cd $(CURDIR)/debian/gforge-shell-ldap/var/lib/gforge/chroot/var/lib/gforge/chroot && ln -s ../../../../home .

	# gforge-shell-postgresql
	install -m 755 deb-specific/install-nsspgsql.sh $(CURDIR)/debian/gforge-shell-postgresql/usr/lib/gforge/bin/

	# gforge-ftp-proftpd
	install -m 755 deb-specific/install-ftp.sh $(CURDIR)/debian/gforge-ftp-proftpd/usr/lib/gforge/bin/
	install -m 644 deb-specific/sf-proftpd.conf $(CURDIR)/debian/gforge-ftp-proftpd/etc/gforge/

	# gforge-ldap-openldap
	install -m 755 utils/ldap/sql2ldif.pl $(CURDIR)/debian/gforge-ldap-openldap/usr/lib/gforge/bin/
	install -m 755 deb-specific/install-ldap.sh $(CURDIR)/debian/gforge-ldap-openldap/usr/lib/gforge/bin/
	install -m 644 deb-specific/gforge.schema $(CURDIR)/debian/gforge-ldap-openldap/etc/gforge/

	# gforge-dns-bind9
	install -m 644 $(CURDIR)/debian/gforge-dns-bind9.overrides $(CURDIR)/debian/gforge-dns-bind9/usr/share/lintian/overrides/gforge-dns-bind9
	install -m 755 deb-specific/install-dns.sh $(CURDIR)/debian/gforge-dns-bind9/usr/lib/gforge/bin/
	install -m 644 deb-specific/dns.head.template $(CURDIR)/debian/gforge-dns-bind9/var/lib/gforge/bind/
	install -m 644 deb-specific/dns.simple.template $(CURDIR)/debian/gforge-dns-bind9/var/lib/gforge/bind/
	install -m 755 deb-specific/dns_conf.pl $(CURDIR)/debian/gforge-dns-bind9/usr/lib/gforge/bin/

	# gforge-lists-mailman
	install -m 755 deb-specific/create-mailing-lists.pl $(CURDIR)/debian/gforge-lists-mailman/usr/lib/gforge/bin/
	install -m 755 deb-specific/fix-mailing-lists.pl $(CURDIR)/debian/gforge-lists-mailman/usr/lib/gforge/bin/

binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installdebconf	
	dh_installdocs
	#dh_installexamples
	#dh_installmenu
	#dh_installemacsen
	#dh_installpam
	#dh_installinit
	dh_installcron
	dh_installman
	#dh_installinfo
	#dh_undocumented
	dh_installchangelogs ChangeLog
	#dh_link
	dh_strip
	dh_compress
	#dh_fixperms
	#dh_makeshlibs
	dh_installdeb
	#dh_perl
	#dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary-arch: build install
	# (No architecture-dependent files for Gforge, doing nothing here)

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure remove-binary-files patch unpatch
