# Galician translation of gforge's debconf templates
# This file is distributed under the same license as the gforge package.
# Jacobo Tarrio <jtarrio@debian.org>, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gforge\n"
"Report-Msgid-Bugs-To: gforge@packages.debian.org\n"
"POT-Creation-Date: 2008-02-26 22:50+0100\n"
"PO-Revision-Date: 2008-03-25 03:47+0000\n"
"Last-Translator: Jacobo Tarrio <jtarrio@debian.org>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:3001
#: ../dsf-helper/shellhost-variables.templates:2001
msgid "Shell server:"
msgstr "Servidor de shell:"

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:3001
msgid ""
"Please enter the hostname of the server that will host the GForge shell "
"accounts."
msgstr ""
"Introduza o nome do servidor que ha hospedar as contas shell de GForge."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:6001
#: ../dsf-helper/downloadhost-variables.templates:2001
msgid "Download server:"
msgstr "Servidor de descargas:"

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:6001
msgid ""
"Please enter the hostname of the server that will host the GForge packages."
msgstr "Introduza o nome do servidor que ha hospedar os paquetes de GForge."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:6001
#: ../dsf-helper/downloadhost-variables.templates:2001
#: ../dsf-helper/lists-variables.templates:2001
#: ../dsf-helper/users-variables.templates:2001
msgid "It should not be the same as the main GForge host."
msgstr "Non debería ser o mesmo que o servidor principal de GForge."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:8001
msgid "GForge administrator login:"
msgstr "Nome do usuario administrador de GForge:"

#. Type: string
#. Description
#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:8001
#: ../gforge-db-postgresql.templates.dsfh-in:10001
msgid ""
"The GForge administrator account will have full privileges on the system. It "
"will be used to approve the creation of new projects."
msgstr ""
"A conta de administrador de GForge ha ter tódolos privilexios no sistema. "
"Hase empregar para aprobar a creación de novos proxectos."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:8001
msgid "Please choose the username for this account."
msgstr "Escolla o nome de usuario para esta conta."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:9001
#: ../dsf-helper/host-variables.templates:2001
msgid "IP address:"
msgstr "Enderezo IP:"

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:9001
#: ../dsf-helper/host-variables.templates:2001
msgid ""
"Please enter the IP address of the server that will host the GForge "
"installation."
msgstr ""
"Introduza o enderezo IP do servidor que ha hospedar a instalación de GForge."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:9001
#: ../dsf-helper/host-variables.templates:2001
msgid "This is needed for the configuration of Apache virtual hosting."
msgstr "Isto é preciso para configurar a hospedaxe virtual de Apache."

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:10001
msgid "GForge administrator password:"
msgstr "Contrasinal de administrador de GForge:"

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:10001
msgid "Please choose the password for this account."
msgstr "Escolla un contrasinal para esta conta."

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:11001
#: ../dsf-helper/dbpasswd-variables.templates:3001
msgid "Password confirmation:"
msgstr "Confirmación do contrasinal:"

#. Type: password
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:11001
#: ../dsf-helper/dbpasswd-variables.templates:3001
msgid "Please re-type the password for confirmation."
msgstr "Volva introducir o contrasinal para confirmalo."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:12001
msgid "Initial list of skills:"
msgstr "Lista inicial de coñecementos:"

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:12001
msgid ""
"GForge allows users to define a list of their skills, to be chosen from "
"those present in the database. This list is the initial list of skills that "
"will enter the database."
msgstr ""
"GForge permítelle aos usuarios facer unha lista cos seus coñecementos, que "
"poden escoller entre os que figuran na base de datos. Esta lista é a lista "
"inicial de coñecementos que han entrar na base de datos."

#. Type: string
#. Description
#: ../gforge-db-postgresql.templates.dsfh-in:12001
msgid "Please enter a semicolon-separated list of skill names."
msgstr ""
"Introduza unha lista de nomes de coñecementos separados por signos de punto "
"e coma."

#. Type: boolean
#. Description
#. Translators: SCM here means "Source Control Management"
#. (cvs, svn, etc.)
#: ../gforge-dns-bind9.templates.dsfh-in:3001
msgid "Do you want a simple DNS setup for GForge?"
msgstr "¿Quere unha configuración de DNS simple para GForge?"

#. Type: boolean
#. Description
#. Translators: SCM here means "Source Control Management"
#. (cvs, svn, etc.)
#: ../gforge-dns-bind9.templates.dsfh-in:3001
msgid ""
"You can use a simple DNS setup with wildcards to map all project web-hosts "
"to a single IP address, and direct all the scm-hosts to a single SCM server, "
"or a complex setup which allows many servers as project web servers or SCM "
"servers."
msgstr ""
"Pode empregar unha configuración de DNS simple con comodíns para mapear "
"tódalas webs dos proxectos a un só enderezo IP e dirixir tódolos nomes dos "
"SCM a un só servidor de SCM, ou unha configuración complexa que lle permite "
"ter moitos servidores coma servidores web ou servidores SCM."

#. Type: boolean
#. Description
#. Translators: SCM here means "Source Control Management"
#. (cvs, svn, etc.)
#: ../gforge-dns-bind9.templates.dsfh-in:3001
msgid ""
"Even if you use a simple DNS setup, you can still use separate machines as "
"project servers; it just assumes that all the project web directories are on "
"the same server with a single SCM server."
msgstr ""
"Aínda se ten unha configuración simple de DNS, pode ter varias máquinas para "
"os servidores dos proxectos; só supón que tódolos directorios web dos "
"proxectos están no mesmo servidor cun só servidor para SCM."

#. Type: boolean
#. Description
#: ../gforge-mta-postfix.templates.dsfh-in:3001
msgid "Do you want mail to ${noreply} to be discarded?"
msgstr "¿Quere que se descarten as mensaxes enviadas a ${noreply}?"

#. Type: boolean
#. Description
#: ../gforge-mta-postfix.templates.dsfh-in:3001
msgid ""
"GForge sends and receives plenty of e-mail to and from the \"${noreply}\" "
"address."
msgstr ""
"GForge envía e recibe moito email desde e para o enderezo \"${noreply}\"."

#. Type: boolean
#. Description
#: ../gforge-mta-postfix.templates.dsfh-in:3001
msgid ""
"E-mail to that address should be directed to a black hole (/dev/null), "
"unless you have another use for that address."
msgstr ""
"O email dirixido a ese enderezo debería ser enviado a un burato negro (/dev/"
"null), a menos que empregue ese enderezo para outra cousa."

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:2001
msgid "GForge domain or subdomain name:"
msgstr "Nome de dominio ou subdominio de GForge:"

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:2001
msgid ""
"Please enter the domain that will host the GForge installation. Some "
"services (scm, lists, etc.) will be given their own subdomain in that domain."
msgstr ""
"Introduza o dominio que ha hospedar a instalación de GForge. Algúns servizos "
"(scm, listas, etc.) han ter o seu propio subdominio dentro dese dominio."

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:3001
msgid "GForge administrator e-mail address:"
msgstr "Enderezo de email do administrador de GForge:"

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:3001
msgid ""
"Please enter the e-mail address of the GForge administrator of this site. It "
"will be used when problems occur."
msgstr ""
"Introduza o enderezo de email do administrador de GForge deste sitio. Hase "
"empregar se aparecen problemas."

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:4001
msgid "GForge system name:"
msgstr "Nome do sistema GForge:"

#. Type: string
#. Description
#: ../dsf-helper/common-variables.templates:4001
msgid ""
"Please enter the name of the GForge system. It is used in various places "
"throughout the system."
msgstr ""
"Introduza o nome do sistema GForge. Emprégase en varios lugares do sistema."

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:2001
msgid "Database server:"
msgstr "Servidor de bases de datos:"

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:2001
msgid ""
"Please enter the IP address (or hostname) of the server that will host the "
"GForge database."
msgstr ""
"Introduza o enderezo IP (ou nome) do servidor que ha hospedar a base de "
"datos de GForge."

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:3001
msgid "Database name:"
msgstr "Nome da base de datos:"

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:3001
msgid ""
"Please enter the name of the database that will host the GForge database."
msgstr ""
"Introduza o nome da base de datos que ha hospedar as contas shell de Gforge"

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:4001
msgid "Database administrator username:"
msgstr "Nome de usuario do administrador da base de datos:"

#. Type: string
#. Description
#: ../dsf-helper/dbhost-variables.templates:4001
msgid ""
"Please enter the username of the database administrator for the server that "
"will host the GForge database."
msgstr ""
"Introduza o nome de usuario do administrador de bases de datos do servidor "
"que ha hospedar a base de datos de GForge."

#. Type: password
#. Description
#: ../dsf-helper/dbpasswd-variables.templates:2001
msgid "Password used for the database:"
msgstr "Contrasinal para a base de datos:"

#. Type: password
#. Description
#: ../dsf-helper/dbpasswd-variables.templates:2001
msgid "Connections to the database system are authenticated by a password."
msgstr ""
"As conexións ao sistema de bases de datos van autenticadas cun contrasinal."

#. Type: password
#. Description
#: ../dsf-helper/dbpasswd-variables.templates:2001
msgid "Please choose the connection password."
msgstr "Escolla o contrasinal para a conexión."

#. Type: string
#. Description
#: ../dsf-helper/downloadhost-variables.templates:2001
msgid ""
"Please enter the host name of the server that will host the GForge packages."
msgstr "Introduza o nome do servidor que ha hospedar os paquetes de GForge."

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:2001
msgid "News administrative group ID:"
msgstr "ID do grupo de administradores de novas:"

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:2001
msgid ""
"The members of the news admin group can approve news for the GForge main "
"page. This group's ID must not be 1. This should be changed only if you "
"upgrade from a previous version and want to keep the data."
msgstr ""
"Os membros do grupo de administradores de novas poden aprobar as novas da "
"páxina principal de Gforge. Este ID de grupo non debe ser 1. Só se debería "
"cambiar se está a se actualizar dunha versión anterior e quere conservar os "
"datos."

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:3001
msgid "Statistics administrative group ID:"
msgstr "ID do grupo de administradores de estatísticas:"

#. Type: string
#. Description
#: ../dsf-helper/groupid-variables.templates:4001
msgid "Peer rating administrative group ID:"
msgstr "ID do grupo de administradores de calificacións de iguais:"

#. Type: string
#. Description
#: ../dsf-helper/lists-variables.templates:2001
msgid "Mailing lists server:"
msgstr "Servidor de listas de correo:"

#. Type: string
#. Description
#: ../dsf-helper/lists-variables.templates:2001
msgid ""
"Please enter the host name of the server that will host the GForge mailing "
"lists."
msgstr ""
"Introduza o nome do servidor que ha hospedar as listas de correo de GForge."

#. Type: string
#. Description
#: ../dsf-helper/shellhost-variables.templates:2001
msgid ""
"Please enter the host name of the server that will host the GForge shell "
"accounts."
msgstr ""
"Introduza o nome do servidor que ha hospedar as contas shell de GForge."

#. Type: string
#. Description
#: ../dsf-helper/users-variables.templates:2001
msgid "User mail redirector server:"
msgstr "Redirector de correo dos usuarios:"

#. Type: string
#. Description
#: ../dsf-helper/users-variables.templates:2001
msgid ""
"Please enter the host name of the server that will host the GForge user mail "
"redirector."
msgstr ""
"Introduza o nome do servidor que ha hospedar o redirector do correo dos "
"usuarios de GForge."

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "English"
msgstr "Inglés"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Bulgarian"
msgstr "Búlgaro"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Catalan"
msgstr "Catalán"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Chinese (Traditional)"
msgstr "Chinés (Tradicional)"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Dutch"
msgstr "Holandés"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Esperanto"
msgstr "Esperanto"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "French"
msgstr "Francés"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "German"
msgstr "Alemán"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Greek"
msgstr "Grego"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Hebrew"
msgstr "Hebreo"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Indonesian"
msgstr "Indonesio"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Italian"
msgstr "Italiano"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Japanese"
msgstr "Xaponés"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Korean"
msgstr "Coreano"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Latin"
msgstr "Latín"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Norwegian"
msgstr "Noruegués"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Polish"
msgstr "Polaco"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Portuguese (Brazilian)"
msgstr "Portugués (Brasil)"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Portuguese"
msgstr "Portugués"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Russian"
msgstr "Ruso"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Chinese (Simplified)"
msgstr "Chinés (Simplificado)"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Spanish"
msgstr "Español"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Swedish"
msgstr "Sueco"

#. Type: select
#. Choices
#: ../dsf-helper/web-variables.templates:2001
msgid "Thai"
msgstr "Thai"

#. Type: select
#. DefaultChoice
#: ../dsf-helper/web-variables.templates:2002
#| msgid "Default language:"
msgid "English[ Default language]"
msgstr "Inglés"

#. Type: select
#. Description
#: ../dsf-helper/web-variables.templates:2003
msgid "Default language:"
msgstr "Idioma por defecto:"

#. Type: select
#. Description
#: ../dsf-helper/web-variables.templates:2003
msgid "Please choose the default language for web pages."
msgstr "Escolla o idioma por defecto para as páxinas web."

#. Type: string
#. Description
#: ../dsf-helper/web-variables.templates:3001
msgid "Default theme:"
msgstr "Tema por defecto:"

#. Type: string
#. Description
#: ../dsf-helper/web-variables.templates:3001
msgid ""
"Please choose the default theme for web pages. This must be a valid name."
msgstr ""
"Escolla o tema por defecto para as páxinas web. Debe ser un nome válido."

#~ msgid "admin"
#~ msgstr "admin"
