<?

/**
 * This file maps symbolic values to localised texts for the group subsections
 */

$group_subsection_names = array (
	'short_forum' => _('Forums'),
	'short_tracker' => _('Trackers'),
	'short_pm' => _('Tasks'),
	'short_docman' => _('Documentations'),
	'short_files' => _('Files'),
	'short_news' => _('News'),
	) ;

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
