#! /usr/bin/make -f

list:
	@echo ======================================================================================
	@echo '=                    Available target are listed below                               ='
	@echo '=                    Today only to build debian pakages and orig tarballs            ='
	@echo ======================================================================================
	@cat Makefile.debian | grep '^.*:.*#$$' | sed 's/^\(.*:\).*#\(.*\)#$$/\1		\2/'

all: allgf                         # Build gforge #
clean: cleangf                     # Clean gforge #
allor: orig origcvs origsvn        # Build gforge orig tarball #
cleanor:                           # Clean gforge orig tarball #
	rm -f gforge*orig.tar.gz
allgf: orig cleangf build          # Build gforge and orig tarballs #
allup: upload                      # Upload all using dput and optional where=<server> #

dchcmd=dch -i
dchcmd=dch
dchcmd=""
dchcmd=dch -v 4.5.6-1 -D unstable "New Upstream version"
debuildopts=-sa
debuildopts=-sa -us -uc
debuildopts=-us -uc
gfversion=$(shell head -1 gforge/debian/changelog | sed 's/.*(\(.*\)-.*).*/\1/'| sed 's/+something/+z/')
gfminor=$(shell head -1 gforge/debian/changelog | sed 's/.*(.*-\(.*\)).*/\1/')
where=g-rouille
where=mercure
where=local
documentor_path=/tmp
documentor_vers=phpdocumentor-1.3.0rc3

#
# GFORGE
#
cleangf:		# Clean debian files of gforge build                         #
	@rm -f gforge*.deb sourceforge*.deb gforge*.changes gforge*.upload gforge*.build gforge*.dsc gforge*[^g].tar.gz gforge*.diff.gz gforge*.asc
	@echo cleangf Done
build:			# Build debian gforge packages                               #
	cd gforge ; debclean; $(dchcmd) ; find . -type f -or -type l | grep -v '/CVS/' | grep -v '/.svn/' | grep -v rpm-specific | grep -v docs/phpdoc/docs | cpio -pdumvB ../gforge-$(gfversion)
	cd gforge-$(gfversion); debuild $(debuildopts); fakeroot debian/rules clean
	rm -rf gforge-$(gfversion)
upload:			# Upload gforge packages on where=<server> using dput        #
	dput $(where) gforge*.changes

orig: gforge_$(gfversion).orig.tar.gz                                 # Make gforge orig file                                      #
gforge_$(gfversion).orig.tar.gz:
	cd gforge ; debclean; find . -type f -or -type l | grep -v '/CVS/' | grep -v '/.svn/' | grep -v rpm-specific | grep -v docs/phpdoc/docs | grep -v ^./debian/ | cpio -pdumvB ../gforge-$(gfversion)
	tar cvzf gforge_$(gfversion).orig.tar.gz gforge-$(gfversion)
	rm -rf gforge-$(gfversion)
#
# PHPDOCUMENTOR
#
phpdoc: phpdocumentor_get phpdocumentor_unpack $(documentor_path)/$(documentor_vers)/patched gforge/docs/phpdoc/docs # Get phpdocumentor, install phpdocumentor, build gforge phpdoc     #
	
phpdocumentor_get:
	[ ! -f $(documentor_path)/$(documentor_vers).tar.gz ] && cd $(documentor_path) && wget http://heanet.dl.sourceforge.net/sourceforge/phpdocu/$(documentor_vers).tar.gz || true
phpdocumentor_unpack:
	[ ! -d $(documentor_path)/$(documentor_vers) ] && cd $(documentor_path) && tar xvzf $(documentor_vers).tar.gz || true
$(documentor_path)/$(documentor_vers)/patched:
	cd $(documentor_path)/ && patch -p2 < $(CURDIR)/gforge/docs/phpdoc/manageclass.patch && touch $(documentor_path)/$(documentor_vers)/patched 
gforge/docs/phpdoc/docs:
	cd gforge/docs/phpdoc/ && ./makedoc.sh

#
# Build with pbuilder
#
DISTRIB=etch
MINOR=1
LOCALREPO=/var/www/debian-gforge
DISTRIBLIST=etch lenny sid

svnbuildtest:
	@echo "Will build gforge_$(gfversion)-$(MINOR)$(DISTRIB)"

svnbuild:		# This is the one you should use #
	for dist in $(DISTRIBLIST); do \
	make -f Makefile.debian svnbuilddist DISTRIB=$$dist ; \
	done

svnbuilddist: pbuilderenv localrepo result/gforge_$(gfversion)-$(MINOR)$(DISTRIB)_i386.changes

result/gforge_$(gfversion)-$(MINOR)$(DISTRIB)_i386.changes: tarballs/gforge_$(gfversion).orig.tar.gz 
	# Save changelog
	cp gforge/debian/changelog . 
	# Set version for given distrib
	cd gforge; dch -v $(gfversion)-$(MINOR)$(DISTRIB) -D UNRELEASED "This is $(DISTRIB) autobuild"
	perl -pi -e "s/UNRELEASED/$(DISTRIB)/" gforge/debian/changelog
	# Build the package
	cd gforge; svn-buildpackage --svn-ignore --svn-builder="pdebuild --debbuildopts -sa --buildresult $(CURDIR)/result -- --basetgz $(CURDIR)/pbuilder/base-$(DISTRIB).tgz"
	# Restore changelog
	mv changelog gforge/debian/changelog
	# Install in repository
	cd result; reprepro -Vb $(LOCALREPO) include $(DISTRIB) gforge_$(gfversion)-$(MINOR)$(DISTRIB)_i386.changes

tarballs/gforge_$(gfversion).orig.tar.gz: tarballs
	debclean; find gforge -type f -or -type l | grep -v '/CVS/' | grep -v '/.svn/' | grep -v rpm-specific | grep -v docs/phpdoc/docs | grep -v ^./debian/ | cpio -o -H ustar | gzip > tarballs/gforge_$(gfversion).orig.tar.gz
	
tarballs:
	mkdir tarballs

pbuilderenv: pbuilder pbuilder/base-$(DISTRIB).tgz
	@echo "Ready for $(DISTRIB)"

pbuilder/base-$(DISTRIB).tgz: 
	sudo /usr/sbin/pbuilder create --debug --distribution $(DISTRIB) --basetgz $(CURDIR)/pbuilder/base-$(DISTRIB).tgz

pbuilder:
	mkdir pbuilder

localrepo: $(LOCALREPO) $(LOCALREPO)/conf $(LOCALREPO)/conf/distributions
	
$(LOCALREPO)/conf/distributions:
	for dist in $(DISTRIBLIST); do \
	echo "Codename: $$dist" ; \
	echo "Suite: $$dist" ; \
	echo "Components: main" ; \
	echo "UDebComponents: main" ; \
	echo "Architectures: i386 source" ; \
	echo "Origin: gforge.eu" ; \
	echo "Version: 4.7" ; \
	echo "Description: My GForge $$dist repository" ; \
	echo "SignWith: yes" ; \
	echo "" ; done >> $(LOCALREPO)/conf/distributions

$(LOCALREPO)/conf:
	mkdir $(LOCALREPO)/conf

$(LOCALREPO):
	sudo mkdir /var/www/debian-gforge
	sudo chown `id -u`.`id -g` /var/www/debian-gforge
